﻿directory.setLoginListener("login", "Admin");

addHttpRequestHandler(
      '/readDoc',             
      'handlers/requestHandlers.js',  
      'readDocument'         
);

addHttpRequestHandler(
      '/loadDoc',             
      'handlers/requestHandlers.js',  
      'loadDocument'         
);
addHttpRequestHandler(
      '/readVersion',             
      'handlers/requestHandlers.js',  
      'readVersion'         
);

addHttpRequestHandler(
      '/loadVersion',             
      'handlers/requestHandlers.js',  
      'loadVersion'         
);

// lancement de Apache Solr

var pathToSolrFolder = ds.getModelFolder().path + "solr-4.6.0/example";
var pathToSolrFolderString = "\"" +pathToSolrFolder+"\"";

// detection de l'OS
if(os.isWindows){
	var BatFilePath = ds.getModelFolder().path + "bootStrap/solr.bat";
	var BatFilePathString ="\"" +BatFilePath+"\"";
	var inPut = BatFilePathString + " " + pathToSolrFolderString;
	var inPutString = "\"" +inPut+"\"";
	var worker = new SystemWorker('cmd /u /c' + inPutString);
	var startOk = false;	
    while(startOk == false){
    	var strURL = "http://localhost:8983/solr";
		var xhr = new XMLHttpRequest();
   	 	xhr.open('POST', strURL, true);
  		try{
    		xhr.send(null);
    		startOk = true;
    	}
   		catch(e){
//    		startOk = false;
    	}
    }
}else{
	if(os.isLinux){
		var ShellFilePath = ds.getModelFolder().path + "bootStrap/solr.sh";
		var ShellFilePathString ="\"" +ShellFilePath+"\"";
		var inPut = ShellFilePathString + " " + pathToSolrFolderString;
		var inPutString = "\"" +inPut+"\"";
		var worker = new SystemWorker('bash -c' + "./" + inPutString);
		var startOk = false;	
    	while(startOk == false){
    		var strURL = "http://localhost:8983/solr";
			var xhr = new XMLHttpRequest();
   	 		xhr.open('POST', strURL, true);
  			try{
    			xhr.send(null);
    			startOk = true;
    		}
   			catch(e){
//    		startOk = false;
    		}
   		}
	}
}
