

model.UserGroup.events.onValidate = function() {
var result = {error: 0};
	var theErrors = [];
	var names = ds.UserGroup.query("group.ID = :1 and user.ID= :2", this.group.ID,this.user.ID);
	names = names.first();
	var us = ds.UserGroup.query("user.ID = :1 and group.ID = :2",this.user.ID,this.group.ID);
	us = us.first();
	if(names){
		theErrors[0] = 'The user \n'+ this.user.firstName +' is already in the group : '+this.group.name;
	}
	if(us){
		theErrors[1] = 'The user \n'+ this.user.firstName +' is already in the group : '+this.group.name;
	}
	if(theErrors.length > 0){
		result = {error: 1, errorMessage: theErrors};
	}
	return result;
};
