

model.User.methods.addUser = function(user) {
	var newUser = ds.User.createEntity();
	
		newUser.firstName = user.firstName;
		newUser.lastName = user.lastName;
		newUser.email = user.email;
		newUser.telNumber = user.telNumber;
		newUser.address = user.address;
		newUser.quotaUser = user.quotaUser;
		newUser.login = user.login;
		newUser.password = user.password;
		
		try {
          				newUser.save();
          				if(user.checked == true){
          				ds.Email.sendEmail1(user);
          				}
          				return true;
          				
          					
							}
							catch(e) {
								return {message: e.messages[1]};
							}
		
};

model.User.methods.addUser.scope = "public";


model.User.methods.getCurrentUser = function() {
	var session = currentSession();
	var test = session.storage;
//	var usr = ds.User(test.myID);
//	return usr.userGroupCollection.group;
	return test;
};
model.User.methods.getCurrentUser.scope = "public";







model.User.entityMethods.EditAccount = function(userAccount) {
	this.firstName = userAccount.firstName;     
    this.lastName = userAccount.lastName;     
    this.email = userAccount.email; 
    this.telNumber = userAccount.telNumber;
    this.address = userAccount.address;
    if(userAccount.password != '' && userAccount.password != null){
    	this.password = userAccount.password;
	}	
	try {
		this.save();
		return true;
	}
	catch(e) {
		return {message: e.messages[1]};
	}
};

model.User.entityMethods.EditAccount.scope = "public";


model.User.methods.checkPassword = function(pass) {
	
	var user = ds.User.query("login=:1", currentUser().name);
	
	if(user.length ==1){
		user = user.first();
	}else{
		return false;	
	}
	if(user.password === pass){
		return true;
	}
	
	return false;
};
model.User.methods.checkPassword.scope = "public";


model.User.entityMethods.EditAccount2 = function(userAccount) {
	this.firstName = userAccount.firstName;     
    this.lastName = userAccount.lastName;     
    this.email = userAccount.email; 
    this.telNumber = userAccount.telNumber;
    this.address = userAccount.address;
    this.quotaUser = userAccount.quota;
    this.login = userAccount.login;
    if(userAccount.password != '' && userAccount.password != null){
    	this.password = userAccount.password;
	}	
	try {
		this.save();
		return true;
	}
	catch(e) {
		return {message: e.messages[1]};
	}
};

model.User.entityMethods.EditAccount2.scope = "public";


model.User.entityMethods.validatePassword = function(password) {
	
	var ha1 = directory.computeHA1(this.ID, password);
                return (ha1 === this.HA1Key); 
};
model.User.entityMethods.validatePassword.scope = "publicOnServer";
