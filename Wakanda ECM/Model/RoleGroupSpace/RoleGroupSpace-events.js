


model.RoleGroupSpace.events.onValidate = function() {
	
	var result = {error: 0};
	var theErrors = [];
	var names = ds.RoleGroupSpace.query("group.ID = :1 and space.ID= :2", this.group.ID,this.space.ID);
	names = names.first();
	var id = ds.RoleGroupSpace.query('ID = :1', this.ID);
	if(names && id.length == 0 ){
		theErrors[0] = 'The group \n'+ this.group.name +' is already in the space : '+this.space.name;
	}
	if(theErrors.length > 0){
		result = {error: 1, errorMessage: theErrors};
	}
	return result;
};

model.RoleGroupSpace.events.onInit = function() {
	
};


model.RoleGroupSpace.events.onSave = function() {
	//init values
	if(this.contribution){
		this.reading = true;
	}
	if(this.collaboration){
		this.contribution = true;
		this.reading = true;
	}
	// gestion heritage permissions
	var libraries = this.space.libraries;
	var group = this.group;
	if(this.isNew()){
		for(var i=0;i<libraries.length;i++){
		// on rajoute ces permissions pour tous les libraries
			var oneRoleGroupLibrary = new ds.RoleGroupLibrary({
           		group:group,
            	library: libraries[i],
            	reading: this.reading,
            	contribution: this.contribution,
            	collaboration: this.collaboration
        	});
        	oneRoleGroupLibrary.save(); 
		}
	}else{
		for (var j=0; j<libraries.length; j++){
			if(libraries[j].permissionInheritance == true){
				var roleGroupLibrary = ds.RoleGroupLibrary.find("library.ID = :1 AND group.ID = :2",libraries[j].ID, this.group.ID);
				if(roleGroupLibrary != null){
					roleGroupLibrary.group = group;
					roleGroupLibrary.library = libraries[j];
					roleGroupLibrary.reading = this.reading;
					roleGroupLibrary.contribution = this.contribution;
					roleGroupLibrary.collaboration = this.collaboration;
					roleGroupLibrary.save();
				}		
			}
		}
	}
};



model.RoleGroupSpace.events.onRemove = function() {
	// on supprime l'heritage dans les libraries filles 
	var libraries = this.space.libraries;
	for (var i=0 ;i<libraries.length;i++){
		if(libraries[i].permissionInheritance == true){
			var roleGroupLibraries = ds.RoleGroupLibrary.query("library.ID = :1 AND group.ID = :2",libraries[i].ID, this.group.ID);
			if(roleGroupLibraries != null)
				roleGroupLibraries.remove();	
		}
	}
};
