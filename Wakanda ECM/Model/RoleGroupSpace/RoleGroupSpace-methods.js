

model.RoleGroupSpace.methods.addRoleGroupSpace = function(space,group,role) {
	
	var sp = ds.Space.find("ID=:1",space);
	var gr = ds.Group.find("ID=:1",group);
			var entity = ds.RoleGroupSpace.createEntity();
			entity.reading = role.reading;
			entity.contribution = role.contribution;
			entity.collaboration = role.collaboration
			entity.space = sp;
			entity.group = gr;
			
			try {
          						entity.save();
          						return true;
							}
							catch(e) {
								return {message: e.messages[1]};
							}
};

model.RoleGroupSpace.methods.addRoleGroupSpace.scope = "public";



model.RoleGroupSpace.entityMethods.updateRoleGroupSpace = function(permissions) {
	this.reading = permissions.reading;     
    this.contribution = permissions.contribution;     
    this.collaboration = permissions.collaboration; 
    
    	
	try {
		this.save();
		return true;
	}
	catch(e) {
		return {message: e.messages[1]};
	}
};

model.RoleGroupSpace.entityMethods.updateRoleGroupSpace.scope = "public";
