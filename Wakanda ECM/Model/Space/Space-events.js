
model.Space.events.onSave = function() {
		var date = new Date();
		if(!this.updatingDate && !this.creationDate){
			this.creationDate = this.updatingDate = date;
		}
		else {
			this.updatingDate= date;	
		}
};
model.Space.events.onInit = function() {
	var session = currentSession();
	var userID = session.storage.myID;
	var theUser = ds.Admin(userID);
	if(theUser !== null){
		this.creator = theUser;
	}
	this.creationDate = new Date();
	this.updatingDate = new Date();
};


model.Space.events.onValidate = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	if(this.isNew()){
		if(permissions.isAdmin() == false){
			return { error: 100, errorMessage:"You don't have permissions to create new space" };
		}
	}else{
		if((permissions.isAdmin() == false) && (permissions.isSpaceManager(this.ID) == false)){
			return { error: 100, errorMessage:"You don't have permissions to edit this space" };
		}
	}
	// champs vides 
	var result = {error: 0};
	// errors remplissage
	var theErrors = [];
	if((this.name == null)||(this.name.length == 0)){
		theErrors[0] = 'The Name field is requiered!';
	}
	if((this.description == null)||(this.description.length == 0)){
		theErrors[1] = 'The Description field is requiered!';
	}
	if((this.manager == null)||(this.manager.length == 0)){
		theErrors[2] = 'The Manager field is requiered!';
	}
	if((this.quotaSpace == null)||(this.quotaSpace.length == 0)){
		theErrors[3] = 'The Space Quota field is requiered!';
	}
	//return
	if(theErrors.length > 0){
		result = {error: 1, errorMessage: theErrors};
	}
	return result;

};

model.Space.events.onRestrictingQuery = function() {
	var result = ds.Space.createEntityCollection();
	var col = ds.Space.all();
	var permissions = require('permissions/permissions');
	for(var i=0;i<col.length;i++){	
		if(permissions.checkSpacePermissions(col[i].ID).reading == true){
			result.add(col[i]);
		}
	}
	return result;
};



model.Space.events.onRemove = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	if((permissions.isAdmin() == false) && (permissions.isSpaceManager(this.ID) == false)){
		return {error: 100, errorMessage: "You don't have permissions to delete this space" };
	}
		var lib = this.libraries;
		for(var i=0;i<lib.length;i++){
			var libID = ds.Library.find("ID = :1", lib[i].ID);
			libID.remove();
		}
		//suppression des autorisations associ�es
		var roleGroupsSpace = ds.RoleGroupSpace.query("space.ID = :1", this.ID);
		if(roleGroupsSpace != null){
			roleGroupsSpace.remove();
		}
};


model.Space.path.events.onSave = function() {
	var libraries = this.libraries;
	for(var i=0;i<libraries.length;i++){
		libraries[i].setPath(this.path);
	}
};
