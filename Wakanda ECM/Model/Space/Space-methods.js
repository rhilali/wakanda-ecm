

model.Space.methods.addSpace = function(spaceData) {
					var basePath = ds.getModelFolder().path+"Base/";
					var spacePath = basePath + spaceData.name;
					var newSpace = Folder(spacePath);
					
						if (newSpace.exists && spaceData.name.length != 0) {
							return {message: " This Space exists "};
						}else {
							var newSpaceEntity =  ds.Space.createEntity();     
          					newSpaceEntity.name = spaceData.name;     
          					newSpaceEntity.description = spaceData.description;     
          					newSpaceEntity.quotaSpace = spaceData.quota; 
          					newSpaceEntity.manager = spaceData.manager;
          					newSpaceEntity.path = spacePath;
          					try {
          						newSpaceEntity.save();
          						newSpace.create();
							}
							catch(e) {
								return {message: e.messages[1]};
							}
						
						}
};
model.Space.methods.addSpace.scope = "public";


model.Space.entityMethods.deleteSpace = function() {
	var theSpaceToDelete = Folder(this.path);
	try {
		this.remove();
		theSpaceToDelete.remove();
	}
	catch(e) {
		return {message: e.messages[0]};
	}
};
model.Space.entityMethods.deleteSpace.scope = "public";


model.Space.entityMethods.updateSpace = function(spaceData) { 
	var basePath = ds.getModelFolder().path+"Base/";
	var spacePath = basePath + spaceData.name;
	
	if (Folder(spacePath).exists && spaceData.name.length != 0 && this.name != spaceData.name) {
		return {message: " This name of space exists. Choose an other name"};
	}else {
		var oldPath = this.path;
		this.name = spaceData.name;  
		this.path = spacePath;  
   		this.description = spaceData.description;     
    	this.quotaSpace = spaceData.quota; 
   		this.manager = spaceData.manager;
    	this.updatingDate = new Date();
		try {
			this.save();
			Folder(oldPath).setName(spaceData.name);
		}
		catch(e) {
			return {message: e.messages[1]};
		}
	}
	
};
model.Space.entityMethods.updateSpace.scope = "public";


model.Space.entityMethods.getLibraries = function() {
	return this.libraries;
};
model.Space.entityMethods.getLibraries.scope = "public";
