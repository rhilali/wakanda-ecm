

model.Group.events.onSave = function() {
	if(this.directoryID == null) {
		
	var newGroup = directory.addGroup(this.name , this.name);
	directory.save();
	
		var id = newGroup.ID;
		this.directoryID = id;
		this.save();
	}

};


model.Group.events.onValidate = function() {

   var result = {error: 0};
	var theErrors = [];
	if((this.name == null)||(this.name.length == 0)){
		theErrors[0] = 'The name must have a value!';
	}
	var names = ds.Group.query("name = :1", this.name);
	if((names.length !=0 )&& (this.directoryID == null)){
		theErrors[1] = 'The name must be unique!';
	}
	
	if(theErrors.length > 0){
		result = {error: 1, errorMessage: theErrors};
	}
	return result;
    
    
};


model.Group.events.onRemove = function() {
	var myGroup = directory.group(this.name);
	myGroup.remove();
	directory.save();
};
