

model.Group.methods.addGroup = function(group) {
	
	var newGroup = ds.Group.createEntity();
	
		newGroup.name = group.name;
		newGroup.description = group.description;
		newGroup.nbrUsers = group.nbrUsers;
		newGroup.dateCreation = new Date();
		try {
          				newGroup.save();
          				return true;
          					
							}
							catch(e) {
								return {message: e.messages[1]};
							}
};


model.Group.methods.addGroup.scope = "public";



model.Group.entityMethods.UpdateGroup = function(upGroup) {
	this.name = upGroup.name;     
    this.description = upGroup.description;     
    this.nbrUsers = upGroup.nbrUsers; 
    
    	
	try {
		this.save();
		return true;
	}
	catch(e) {
		return {message: e.messages[1]};
	}
};

model.Group.entityMethods.UpdateGroup.scope = "public";


model.Group.entityMethods.deleteGroupe = function() {
	// Add your code here;
};
