
function upgradeMajorVersion(version){
		var oldVersion = version.split('.');
		var first = oldVersion[0];
		var i = parseInt(first);
		i +=1;
		return i +'.'+0;
}
function upgradeMinorVersion(version){
		var oldVersion = version.split('.');
		var first = oldVersion[0];
		var second = oldVersion[1];
		if(parseInt(second) == 8){
			var i = parseInt(first);
			i +=1;
			return i +'.'+0;
		}else{
			var j = parseInt(second);
			j +=2;
			return first +'.'+j;
		}
}

model.Versions.methods.saveVersion = function(versionData) {
	var errors = [];
	var result;
	var metaDataArray = [];
	var document = ds.Document.find("ID = :1", versionData.document.ID);
	// sauvegarder le document physique avec (nom + version) 
	if(versionData.versionType == "minor"){
		var newVersion = upgradeMinorVersion(document.version);
	}else{
		if(versionData.versionType == "major")
			var newVersion = upgradeMajorVersion(document.version);
	}
	var nameNoExt = document.name.split('.');
	nameNoExt.pop();
	nameNoExt.join(".");
	var ext = File(versionData.versionToSave.path).extension;
	var newName = nameNoExt + " " + "(" + newVersion + ")" + "." +ext;
	File(versionData.versionToSave.path).setName(newName);
	var newPath = File(versionData.versionToSave.path).parent.path + newName;
		
	var indexationCall = require('metaData/indexDocument').indexDocument({id:document.ID, path:newPath, version:newVersion});
	var rsp = eval("("+indexationCall.response+")");
	if(rsp.error != null){
		errors.push("* The document (" + File(versionData.versionToSave.path).name + ") has not been saved : We can't index this Document");
		File(newPath).remove();
	}else{
		if(rsp.responseHeader.status != 0){
			errors.push("* The document (" + File(versionData.versionToSave.path).name + ") has not been saved : Error when indexing this Document");
			File(newPath).remove();
		}else{
			var metaData = require('metaData/getMetaData').getMetaData({ID:document.ID, version:newVersion});
			var rsp = eval("("+metaData+")");
			// set current version on dataClass (Document) 
			document.version = newVersion;
			document.path = newPath;
			document.save();
			// save new version on dataclass (Versions)	
			var newDocumentEntity =  ds.Versions.createEntity();
          	newDocumentEntity.document = document;
			newDocumentEntity.path = document.path;
			newDocumentEntity.version = document.version;
			newDocumentEntity.comment = versionData.comment;
			newDocumentEntity.size = rsp.response.docs[0].stream_size[0];
			newDocumentEntity.save();
         	try {
         		newDocumentEntity.save();
			}
			catch(e) {
				errors.push(e.messages[1]);
			}
			metaDataArray.push({
				idDoc:document.ID,
				path:newDocumentEntity.path,
				name:newDocumentEntity.document.name,
				version:newDocumentEntity.version,
				title:(rsp.response.docs[0].title == undefined)? "" : rsp.response.docs[0].title[0],
				author:(rsp.response.docs[0].author == undefined)? "" : rsp.response.docs[0].author[0],
				type:(rsp.response.docs[0].content_type == undefined)? "" : rsp.response.docs[0].content_type[0],
				description:(rsp.response.docs[0].description == undefined)? "" : rsp.response.docs[0].description[0],
				publisher:(rsp.response.docs[0].publisher == undefined)? "" : rsp.response.docs[0].publisher[0],
				nbPages:(rsp.response.docs[0].xmptpg_npages == undefined)? ((rsp.response.docs[0].page_count == undefined)? "" : 
						rsp.response.docs[0].page_count[0]) :
						rsp.response.docs[0].xmptpg_npages[0],
				size:(rsp.response.docs[0].stream_size == undefined)? " " : rsp.response.docs[0].stream_size[0]
			});
		}
	}
	result = {error:errors,metaData:metaDataArray};
	return result;	
};
model.Versions.methods.saveVersion.scope ="public";

model.Versions.methods.uploadVersion = function(versionData) {
	var library = null; 
	var parentDirectory = null;
	if(versionData.theParentDirectory == null){
		library = ds.Library.find("ID= :1",versionData.theLibrary);
	}else{
		parentDirectory = ds.Directory.find("ID= :1",versionData.theParentDirectory);
	}
	var tmpPath = ds.getModelFolder().path+"DataFolder/tmp/" + versionData.name;
	var path = (parentDirectory == null)? library.path + '/' + versionData.name : parentDirectory.path + '/' + versionData.name;
	File(tmpPath).moveTo(path);
	return {path: path};
};
model.Versions.methods.uploadVersion.scope ="public";


model.Versions.methods.deleteDocumentVersions = function(documentID) {
	var versions = ds.Versions.query("document.ID = :1",documentID);
	for(var i=0;i<versions.length;i++){
		var file = File(versions[i].path)
		file.remove();
	}
	versions.remove();
};
model.Versions.methods.deleteDocumentVersions.scope ="public";


model.Versions.methods.getAllVersions = function(documentID) {
	var versions = ds.Versions.query("document.ID = :1",documentID);
	var document = ds.Document.find("ID = :1", documentID);
	var lastVersion = versions.query("version = :1", document.version);
	var olderVersions = versions.minus(lastVersion);
	var lastVersionObject = {
		ID: lastVersion.ID,
		version: lastVersion.version,
		documentName: lastVersion.document.name,
		creator: lastVersion.creator.lastName,
		comment: lastVersion.comment,
		creationDate: lastVersion.creationDate.toLocaleString()
	};
	var olderVersionsArray = [];
	for(var i=0;i<olderVersions.length;i++){
		olderVersionsArray.push({
			ID: olderVersions[i].ID,
			version: olderVersions[i].version,
			documentName: olderVersions[i].document.name,
			creator: olderVersions[i].creator.lastName,
			comment: olderVersions[i].comment,
			creationDate: olderVersions[i].creationDate.toLocaleString()
		})
	}
	return {lastVersion: lastVersionObject, olderVersions: olderVersionsArray}
};
model.Versions.methods.getAllVersions.scope ="public";


model.Versions.methods.deleteVersion = function(versionID) {
	var version = ds.Versions.find("ID = :1", versionID);
	var file = File(version.path)
	file.remove();
	version.remove();
};
model.Versions.methods.deleteVersion.scope ="public";


model.Versions.methods.revertToOlderVersion = function(versionData) {
	var version = ds.Versions.find("ID = :1", versionData.versionID);
	var document = version.document;
	var oldNumVersion = version.version;
	var nameNoExt = version.document.name.split('.');
	nameNoExt.pop();
	nameNoExt.join(".");
	var ext = File(version.path).extension;
	if(versionData.versionType == "minor"){
		var newVersion = upgradeMinorVersion(document.version);
	}else{
		if(versionData.versionType == "major")
		var newVersion = upgradeMajorVersion(document.version);
	}
	// creer le nouveau document physique avec (nom + version)
	var newName = nameNoExt + " " + "(" + newVersion + ")" + "." +ext;
	var newPath = File(version.path).parent.path + newName;
	File(version.path).copyTo(newPath);
	// set current version on dataClass (Document) 
	document.version = newVersion;
	document.path = newPath;
	document.save();
	// save new version on dataclass (Versions)	
	var newDocumentEntity =  ds.Versions.createEntity();
	newDocumentEntity.document = document;
	newDocumentEntity.path = document.path;
	newDocumentEntity.version = document.version;
	newDocumentEntity.comment = versionData.comment;
	newDocumentEntity.save();
	//indexation du document
	var metaData = require('metaData/getMetaData').getMetaData({ID:document.ID, version:oldNumVersion});
	var rsp = eval("("+metaData+")");
	var metaDataObject = {
				idDoc:document.ID,
				path:version.path,
				name:document.name,
				version:document.version,
				title:(rsp.response.docs[0].title == undefined)? "" : rsp.response.docs[0].title[0],
				author:(rsp.response.docs[0].author == undefined)? "" : rsp.response.docs[0].author[0],
				type:(rsp.response.docs[0].content_type == undefined)? "" : rsp.response.docs[0].content_type[0],
				description:(rsp.response.docs[0].description == undefined)? "" : rsp.response.docs[0].description[0],
				publisher:(rsp.response.docs[0].publisher == undefined)? "" : rsp.response.docs[0].publisher[0],
				nbPages:(rsp.response.docs[0].xmptpg_npages == undefined)? ((rsp.response.docs[0].page_count == undefined)? "" : 
						rsp.response.docs[0].page_count[0]) :
						rsp.response.docs[0].xmptpg_npages[0],
				size:(rsp.response.docs[0].stream_size == undefined)? " " : rsp.response.docs[0].stream_size[0]
	};
	require('metaData/updateMetaData').indexRevertVersion(metaDataObject);
};
model.Versions.methods.revertToOlderVersion.scope ="public";
