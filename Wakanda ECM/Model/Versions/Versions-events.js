

model.Versions.events.onInit = function() {
	var session = currentSession();
	var userID = session.storage.myID;
	var theUser = ds.User(userID);
	if(theUser !== null){
		this.creator = theUser;
	}
	this.creationDate = new Date();
};


model.Versions.events.onRemove = function() {
	require('metaData/updateMetaData').deleteVersionMetaData({idDoc: this.document.ID, version: this.version});
};
