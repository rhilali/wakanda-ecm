

model.Directory.methods.addDirectory = function(directoryData) {
							
							var library = null; 
							var parentDirectory = null;
//							var basePath = ds.getModelFolder().path+"Base/";
//							var libraryPath = library.path;
							var directoryPath;
							if(directoryData.theParentDirectory == null){
								library = ds.Library.find("ID= :1",directoryData.theLibrary);
								directoryPath = library.path +'/' + directoryData.name;
								var relativePath = library.path +'/' + directoryData.name;
							}else{
								parentDirectory = ds.Directory.find("ID= :1",directoryData.theParentDirectory);
								directoryPath = parentDirectory.path + '/' + directoryData.name;
							}
							var newDirectory = Folder(directoryPath);
							
							if (newDirectory.exists && directoryData.name.length != 0) {
								return {message: " This Directory exists "};
							}else{
								var newDirectoryEntity =  ds.Directory.createEntity();     
        	  					newDirectoryEntity.name = directoryData.name;     
         	 					newDirectoryEntity.description = directoryData.description; 
          						newDirectoryEntity.path = directoryPath;     
          						newDirectoryEntity.library = library;
          						newDirectoryEntity.directory = parentDirectory;		
          						try {
          							newDirectoryEntity.save();
          							newDirectory.create();
								}
								catch(e) {
									return {message: e.messages[1]};
								}
							}
};
model.Directory.methods.addDirectory.scope = "public";


model.Directory.methods.getContents = function(id) {
	var directory = ds.Directory.find("ID = :1",id);
	var directories = directory.directoryCollection;
	var documents = directory.documents;
	var arrayDirectories = directories.toArray("ID, name, path");
	for(var i=0 ; i < arrayDirectories.length ; i++){
	 arrayDirectories[i].type="directory";
	}
	var arrayDocuments = documents.toArray("ID, name, path, version");
	for(var i=0 ; i < arrayDocuments.length ; i++){
	 arrayDocuments[i].type="document";
	}
	var result = arrayDirectories.concat(arrayDocuments);
	return result;
};
model.Directory.methods.getContents.scope = "public";



model.Directory.methods.getAllDocuments = function(id) {
	// functions 
	function getDocuments(idDirectory){
		var theDirectory = ds.Directory.find("ID = :1",idDirectory);
		var docs = theDirectory.documents;
		var arrayDocs = docs.toArray("ID, name, path,version, relativePath");
		return arrayDocs;
	}
	
	function getAllDirectories(id){
		var parentDirectory = ds.Directory.find("ID = :1",id);
		var directories = [];
		for(var j=0;j<parentDirectory.directoryCollection.length;j++){
			directories.push(parentDirectory.directoryCollection[j].ID);
		}
		if(directories.length == 0){
			return directories;
		}
		else{
			var tmp = [];
			for(var i=0;i<directories.length;i++){
				var tmpDirectories = getAllDirectories(directories[i]);
				for(var k=0;k<tmpDirectories.length;k++)
 				tmp.push(tmpDirectories[k]);
 			}
 		}
 		for(var m=0;m<tmp.length;m++)
 			directories.push(tmp[m]);
 		return directories;
	}
	
	var allDocuments = [];
	
	var firstLeveldocs = getDocuments(id);
	for(var i=0;i<firstLeveldocs.length;i++){
		allDocuments.push(firstLeveldocs[i]);
	}
	
	var allDirectories = getAllDirectories(id);
	for(var i=0;i<allDirectories.length;i++){
		var docs = getDocuments(allDirectories[i]);
		for(var j=0;j<docs.length;j++){
			allDocuments.push(docs[j]);
		}
	}
	
	return allDocuments;
	
};
model.Directory.methods.getAllDocuments.scope = "public";


model.Directory.methods.getAllDirectories = function(idDirectory) {
	function getAllDirectories(id){
		var parentDirectory = ds.Directory.find("ID = :1",id);
		var directories = [];
		for(var j=0;j<parentDirectory.directoryCollection.length;j++){
			directories.push(parentDirectory.directoryCollection[j].ID);
		}
		if(directories.length == 0){
			return directories;
		}
		else{
			var tmp = [];
			for(var i=0;i<directories.length;i++){
				var tmpDirectories = getAllDirectories(directories[i]);
				for(var k=0;k<tmpDirectories.length;k++)
 				tmp.push(tmpDirectories[k]);
 			}
 		}
 		for(var m=0;m<tmp.length;m++)
 			directories.push(tmp[m]);
 		return directories;
	}
	return getAllDirectories(idDirectory);
};
model.Directory.methods.getAllDirectories.scope = "public";


model.Directory.methods.deleteDirectory = function(idDirectory) {
	var directory = ds.Directory.find("ID = :1",idDirectory);
	if(directory != null){
		var directoryFolder = Folder(directory.path);
		try{
			directory.remove();
			directoryFolder.remove();
		}
		catch(e) {
			return {message: e.messages[0]};
		}
		
	}
	
};
model.Directory.methods.deleteDirectory.scope = "public";

model.Directory.methods.updateDirectory = function(directoryData) {
	var directory = ds.Directory.find("ID = :1",directoryData.ID);
	var parentPath = (directory.directory != null)? directory.directory.path: directory.library.path;
	if(directory != null){
		var oldPath = directory.path;
		directory.name = directoryData.name;
		directory.description = directoryData.description;
		directory.save();
	}
	
};
model.Directory.methods.updateDirectory.scope = "public";

model.Directory.entityMethods.setPath = function(parent) {
	this.path = parent + '/' + this.name;
	this.save();
};
model.Directory.entityMethods.setPath.scope = "public";
