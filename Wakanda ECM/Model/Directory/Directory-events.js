
model.Directory.events.onSave = function() {
	var date = new Date();
	this.parentSpace = getParentSpace(this);
	function getParentSpace(dir){
		if(dir.library != null){
			return dir.library.space.ID;
		}else{
			return dir.directory.parentSpace;
		}
	}
	if(!this.updatingDate && !this.creationDate){
		this.creationDate = this.updatingDate = date;
	}
	else {
		this.updatingDate= date;
	}
	if(this.isNew()){
	//gestion heritage des permissions
		if(this.library != null){
			var roleParentGroups = ds.RoleGroupLibrary.query("library.ID = :1",this.library.ID);
		}else{
			var roleParentGroups = ds.RoleGroupDirectory.query("directory.ID = :1",this.directory.ID);
		}
		for(var i=0; i<roleParentGroups.length; i++){
			var oneRoleGroupDirectory = new ds.RoleGroupDirectory({
           		group:roleParentGroups[i].group,
           		directory: this,
            	reading: roleParentGroups[i].reading,
           		contribution: roleParentGroups[i].contribution,
            	collaboration: roleParentGroups[i].collaboration
       		});
        	oneRoleGroupDirectory.save(); 
		}
	}
	this.save();
};
model.Directory.events.onInit = function() {
	var myCurrentUser = currentUser(),
	myUser = ds.User.find('ID = :1',myCurrentUser.ID);
	if((myCurrentUser !== null) && (myUser !== null)){
		this.creator = myUser;
	}
	this.creationDate = new Date();
	this.updatingDate = new Date();
};


model.Directory.events.onValidate = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	var isLibraryChild = (this.library != null)? true : false;
	var parent = (this.library != null)? this.library : this.directory;
	if(isLibraryChild){
		var parentPermissions = permissions.checkLibraryPermissions(this.library.ID);
	}else{
		var parentPermissions = permissions.checkDirectoryPermissions(this.directory.ID);
	}
	if(this.isNew()){
		if(parentPermissions.contribution == false){
			return { error: 100, errorMessage:"You don't have permissions to create new directory" };
		}
	}else{
		if(parentPermissions.collaboration == false){
			return { error: 100, errorMessage:"You don't have permissions to edit this directory" };
		}
	}
	// champs vides 
	var result = {error: 0};
	var theErrors = [];
	if((this.name == null)||(this.name.length == 0)){
		theErrors[0] = 'The Name field is requiered!';
	}
	if((this.description == null)||(this.description.length == 0)){
		theErrors[1] = 'The Description field is requiered!';
	}
	if(theErrors.length > 0){
		result = {error: 1, errorMessage: theErrors};
	}
	return result;
};


model.Directory.events.onRemove = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	var isLibraryChild = (this.library != null)? true : false;
	var parent = (this.library != null)? this.library : this.directory;
	if(isLibraryChild){
		var parentPermissions = permissions.checkLibraryPermissions(this.library.ID);
	}else{
		var parentPermissions = permissions.checkDirectoryPermissions(this.directory.ID);
	}
	if(parentPermissions.collaboration == false){
		return { error: 100, errorMessage:"You don't have permissions to delete this directory" };
	}
	
	var directories = this.directoryCollection;
	var documents = this.documents;
	for(var i=0;i<directories.length;i++){
		var directoryID = ds.Directory.find("ID = :1", directories[i].ID);
		directoryID.remove();
	}
	for(var i=0;i<documents.length;i++){
		var documentID = ds.Document.find("ID = :1", documents[i].ID);
		documentID.remove();
	}
// suppression des autorisations associ�s
	var roleGroupsDirectory = ds.RoleGroupDirectory.query("directory.ID = :1", this.ID);
	if(roleGroupsDirectory != null){
		roleGroupsDirectory.remove();
	}
};


model.Directory.path.events.onSave = function() {
	var directories = this.directoryCollection;
	var documents = this.documents;
	for(var i=0;i<directories.length;i++){
		directories[i].setPath(this.path);
	}
	for(var j=0;j<documents.length;j++){
		documents[j].setPath(this.path);
	}
};


model.Directory.permissionInheritance.events.onInit = function() {
	this.permissionInheritance = true;
};


model.Directory.events.onRestrictingQuery = function() {
	var result = ds.Directory.createEntityCollection();
	var col = ds.Directory.all();
	var permissions = require('permissions/permissions');
	for(var i=0;i<col.length;i++){
		var isLibraryChild = (col[i].library != null)? true : false;
		var parent = (col[i].library != null)? col[i].library : col[i].directory;
		if(isLibraryChild){
			if((permissions.checkDirectoryPermissions(col[i].ID).reading == true) && (permissions.checkLibraryPermissions(col[i].library.ID).reading == true)){
				result.add(col[i]);
			}
		}else{
			if((permissions.checkDirectoryPermissions(col[i].ID).reading == true) && (permissions.checkDirectoryPermissions(col[i].directory.ID).reading == true)){
				result.add(col[i]);
			}
		}
	}
	return result;
};
