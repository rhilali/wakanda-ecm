

model.Document.methods.addDocument = function(documentData) {
							var library = null; 
							var parentDirectory = null;
							var documentPath;
							var errors = [];
							var result;
							var metaDataArray = [];
							if(documentData.theParentDirectory == null){
								library = ds.Library.find("ID= :1",documentData.theLibrary);
							}else{
								parentDirectory = ds.Directory.find("ID= :1",documentData.theParentDirectory);
							}
							for(var i=0 ; i< documentData.docs.length; i++){	
								documentPath = documentData.docs[i].path;	
								var newDocumentEntity =  ds.Document.createEntity();
								var indexationCall = require('metaData/indexDocument').indexDocument({id:newDocumentEntity.ID, path:documentPath, version: "1.0"});
								var rsp = eval("("+indexationCall.response+")");
								if(rsp.error != null){
									errors.push("* The document (" + documentData.docs[i].name + ") has not been saved : We can't index this Document");
									File(documentPath).remove();
								}else{
									if(rsp.responseHeader.status != 0){
										errors.push("* The document (" + documentData.docs[i].name + ") has not been saved : Error when indexing this Document");
										File(documentPath).remove();
									}else{
										var metaData = require('metaData/getMetaData').getMetaData({ID:newDocumentEntity.ID, version: "1.0"});
										var rsp = eval("("+metaData+")");
          								newDocumentEntity.name = documentData.docs[i].name;     
         								newDocumentEntity.path = documentPath;     
         								newDocumentEntity.library = library;
         								newDocumentEntity.directory = parentDirectory;
										newDocumentEntity.type = rsp.response.docs[0].content_type[0];
										newDocumentEntity.size = rsp.response.docs[0].stream_size[0];
         								try {
         									newDocumentEntity.save();
										}
										catch(e) {
											errors.push(e.messages[1]);
										}
										metaDataArray.push({
											idDoc:newDocumentEntity.ID,
											path:newDocumentEntity.path,
											name:newDocumentEntity.name,
											version:newDocumentEntity.version,
											title:(rsp.response.docs[0].title == undefined)? "" : rsp.response.docs[0].title[0],
											author:(rsp.response.docs[0].author == undefined)? "" : rsp.response.docs[0].author[0],
											type:(rsp.response.docs[0].content_type == undefined)? "" : rsp.response.docs[0].content_type[0],
											description:(rsp.response.docs[0].description == undefined)? "" : rsp.response.docs[0].description[0],
											publisher:(rsp.response.docs[0].publisher == undefined)? "" : rsp.response.docs[0].publisher[0],
											nbPages:(rsp.response.docs[0].xmptpg_npages == undefined)? ((rsp.response.docs[0].page_count == undefined)? "" : 
													rsp.response.docs[0].page_count[0]) :
									 				rsp.response.docs[0].xmptpg_npages[0],
											size:(rsp.response.docs[0].stream_size == undefined)? " " : rsp.response.docs[0].stream_size[0]
										});
									}
								}	
							}
							result = {error:errors,metaData:metaDataArray};
							return result;
};
model.Document.methods.addDocument.scope ="public";


model.Document.entityMethods.displayVersions = function() {
	// Add your code here;
};


model.Document.methods.verifyExistence = function(documentData) {
	var library = null; 
	var parentDirectory = null;
	var exists;	
	var documentsExisted = [];
	var documentsToSave = [];
	if(documentData.theParentDirectory == null){
		library = ds.Library.find("ID= :1",documentData.theLibrary);
		var documents = library.documents;
		for(var i=0 ; i< documentData.names.length; i++){
			exists = false;
			var tmpPath = ds.getModelFolder().path+"DataFolder/tmp/" + documentData.names[i];
			documents.forEach(
    			function(doc) {
      			  if(doc.name == documentData.names[i]){
      			  	exists = true;
      			  }
    			});
			if (exists) {
				File(tmpPath).remove();
				documentsExisted.push(documentData.names[i]);
			}else{
				var nameNoExt = File(tmpPath).nameNoExt;
				var ext = File(tmpPath).extension;
				var path = library.path + "/" + nameNoExt + " " + "(1.0)" + "." +ext;
				File(tmpPath).moveTo(path);
				documentsToSave.push({name: documentData.names[i], path: path});
			}
		}
	}else{
		parentDirectory = ds.Directory.find("ID= :1",documentData.theParentDirectory);
		var documents = parentDirectory.documents;
		for(var i=0 ; i< documentData.names.length; i++){
			exists = false;
			var tmpPath = ds.getModelFolder().path+"DataFolder/tmp/" + documentData.names[i];
			documents.forEach(
    			function(doc) {
      			  if(doc.name == documentData.names[i]){
      			  	exists = true;
      			  }
    			});	
			if (exists) {
				File(tmpPath).remove();
				documentsExisted.push(documentData.names[i]);
			}else{
				var nameNoExt = File(tmpPath).nameNoExt;
				var ext = File(tmpPath).extension;
				var path = parentDirectory.path + "/" + nameNoExt + " " + "(1.0)" + "." +ext;
				File(tmpPath).moveTo(path);
				documentsToSave.push({name: documentData.names[i], path: path});
			}
		}
	}
	return {existedFiles: documentsExisted, filesToSave: documentsToSave};
};
model.Document.methods.verifyExistence.scope ="public";


model.Document.methods.updateDocument = function(documentData) {
	var document = ds.Document.find("ID = :1",documentData.ID);
	if(document != null){
		document.save();
	}
};
model.Document.methods.updateDocument.scope ="public";


model.Document.methods.deleteDocument = function(idDocument) {
	var document = ds.Document.find("ID = :1",idDocument);
	if(document != null){
		var documentFile = File(document.path);
		try{
			document.remove();
			documentFile.remove();
		}
		catch(e) {
			return {message: e.messages[1]};
		}
		
	}
};
model.Document.methods.deleteDocument.scope ="public";


model.Document.entityMethods.setPath = function(parent) {
	this.path = parent +'/' + this.name;
	this.save();
};
model.Document.entityMethods.setPath.scope ="public";