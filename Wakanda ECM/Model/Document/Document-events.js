

model.Document.events.onSave = function() {
	var date = new Date();
	this.parentSpace = getParentSpace(this);
	function getParentSpace(doc){
		if(doc.library != null){
			return doc.library.space.ID;
		}else{
			return doc.directory.parentSpace;
		}
	}
	if(!this.updatingDate && !this.creationDate){
		this.creationDate = this.updatingDate = date;
	}
	else {	
		this.updatingDate= date;
	}
	if(this.isNew() == true){
		var newVersionEntity = ds.Versions.createEntity();
		newVersionEntity.document = this;
		newVersionEntity.path = this.path;
		newVersionEntity.version = this.version;
		newVersionEntity.comment = "First version";
		newVersionEntity.save();	
	}
//gestion heritage des permissions
	if(this.library != null){
		var roleParentGroups = ds.RoleGroupLibrary.query("library.ID = :1",this.library.ID);
	}else{
		var roleParentGroups = ds.RoleGroupDirectory.query("directory.ID = :1",this.directory.ID);
	}
	for(var i=0; i<roleParentGroups.length; i++){
		var oneAccessRuleGroupDocument = new ds.AccessRuleGroupDocument({
            group:roleParentGroups[i].group,
            document: this,
            reading: roleParentGroups[i].reading,
            writing: roleParentGroups[i].contribution,
            deleting: roleParentGroups[i].collaboration
        });
        oneAccessRuleGroupDocument.save(); 
	}
	this.save();
};


model.Document.events.onInit = function() {
	var session = currentSession();
	var userID = session.storage.myID;
	var theUser = ds.Admin(userID);
	if(theUser !== null){
		this.creator = theUser;
	}
	this.version = "1.0";
};


model.Document.events.onRemove = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	var isLibraryChild = (this.library != null)? true : false;
	var parent = (this.library != null)? this.library : this.directory;
	if(isLibraryChild){
		var parentPermissions = permissions.checkLibraryPermissions(this.library.ID);
	}else{
		var parentPermissions = permissions.checkDirectoryPermissions(this.directory.ID);
	}
	if(parentPermissions.collaboration == false){
		return { error: 100, errorMessage:"You don't have permissions to delete this document" };
	}
	
	var deleteMetaData = require('metaData/updateMetaData').deleteMetaData(this.ID);
	ds.Versions.deleteDocumentVersions(this.ID);
// suppression des autorisations associ�s
	var roleGroupsDocument = ds.AccessRuleGroupDocument.query("document.ID = :1", this.ID);
	if(roleGroupsDocument != null){
		roleGroupsDocument.remove();
	}
};


model.Document.permissionInheritance.events.onInit = function() {
	this.permissionInheritance = true;
};


model.Document.events.onRestrictingQuery = function() {
	var result = ds.Document.createEntityCollection();
	var col = ds.Document.all();
	var permissions = require('permissions/permissions');
	for(var i=0;i<col.length;i++){
		var isLibraryChild = (col[i].library != null)? true : false;
		var parent = (col[i].library != null)? col[i].library : col[i].directory;
		if(isLibraryChild){
			if((permissions.checkDocumentPermissions(col[i].ID).reading == true) && (permissions.checkLibraryPermissions(col[i].library.ID).reading == true)){
				result.add(col[i]);
			}
		}else{
			if((permissions.checkDocumentPermissions(col[i].ID).reading == true) && (permissions.checkDirectoryPermissions(col[i].directory.ID).reading == true)){
				result.add(col[i]);
			}
		}
	}
	return result;
	
};


model.Document.events.onValidate = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	var isLibraryChild = (this.library != null)? true : false;
	var parent = (this.library != null)? this.library : this.directory;
	if(isLibraryChild){
		var parentPermissions = permissions.checkLibraryPermissions(this.library.ID);
	}else{
		var parentPermissions = permissions.checkDirectoryPermissions(this.directory.ID);
	}
	if(!this.isNew()){
		
		if(parentPermissions.collaboration == false){
			return { error: 100, errorMessage:"You don't have permissions to edit this document" };
		}
	}
};
