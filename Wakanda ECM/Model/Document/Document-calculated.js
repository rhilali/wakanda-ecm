

model.Document.relativePath.onGet = function() {
	var basePath = ds.getModelFolder().path+"Base/";
	var path = File(this.path).parent.path + this.name;
	var relativePath = path.split(basePath);
	return relativePath[1];
};
