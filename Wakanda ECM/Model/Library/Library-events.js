

model.Library.events.onInit = function() {
	var myCurrentUser = currentUser(),
	myUser = ds.User.find('ID = :1',myCurrentUser.ID);
	if((myCurrentUser !== null) && (myUser !== null)){
		this.creator = myUser;
	}
//	this.creationDate = new Date();
//	this.updatingDate = new Date();
};


model.Library.events.onValidate = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	var parentPermissions = permissions.checkSpacePermissions(this.space.ID);
	if(this.isNew()){
		if(parentPermissions.contribution == false){
			return { error: 100, errorMessage:"You don't have permissions to create new library" };
		}
	}else{
		if(parentPermissions.collaboration == false){
			return { error: 100, errorMessage:"You don't have permissions to edit this library" };
		}
	}
	// champs vides 
	var result = {error: 0};
	var theErrors = [];
	if((this.name == null)||(this.name.length == 0)){
		theErrors[0] = 'The Name field is requiered!';
	}
	if((this.description == null)||(this.description.length == 0)){
		theErrors[1] = 'The Description field is requiered!';
	}
	if(theErrors.length > 0){
		result = {error: 1, errorMessage: theErrors};
	}
	return result;
};


model.Library.events.onRemove = function() {
	//permissions 
	var permissions = require('permissions/permissions');
	var parentPermissions = permissions.checkSpacePermissions(this.space.ID);
	if(parentPermissions.collaboration == false){
		return { error: 100, errorMessage:"You don't have permissions to delete this library" };
	}
		var directories = this.directories;
		var documents = this.documents;
		for(var i=0;i<directories.length;i++){
			var directoryID = ds.Directory.find("ID = :1", directories[i].ID);
			directoryID.remove();
		}
		for(var i=0;i<documents.length;i++){
			var documentID = ds.Document.find("ID = :1", documents[i].ID);
			documentID.remove();
		}
		// suppression des autorisations associ�s
		var roleGroupsLibrary = ds.RoleGroupLibrary.query("library.ID = :1", this.ID);
		if(roleGroupsLibrary != null){
			roleGroupsLibrary.remove();
		}
};



model.Library.events.onSave = function() {
	var date = new Date();
	if(!this.updatingDate && !this.creationDate){
		this.creationDate = this.updatingDate = date;
	}
	else {
		this.updatingDate= date;	
	}
	if(this.isNew()){
		//gestion heritage des permissions
		var roleParentGroups = ds.RoleGroupSpace.query("space.ID = :1",this.space.ID);
		for(var i=0; i<roleParentGroups.length; i++){
			var oneRoleGroupLibrary = new ds.RoleGroupLibrary({
           		group:roleParentGroups[i].group,
            	library: this,
            	reading: roleParentGroups[i].reading,
            	contribution: roleParentGroups[i].contribution,
           		collaboration: roleParentGroups[i].collaboration
        	});
        	oneRoleGroupLibrary.save(); 
		}
	}
};


model.Library.path.events.onSave = function() {
	var directories = this.directories;
	var documents = this.documents;
	for(var i=0;i<directories.length;i++){
		directories[i].setPath(this.path);
	}
	for(var j=0;j<documents.length;j++){
		documents[j].setPath(this.path);
	}
};


model.Library.permissionInheritance.events.onInit = function() {
	this.permissionInheritance = true;
};


model.Library.events.onRestrictingQuery = function() {
	var result = ds.Library.createEntityCollection();
	var col = ds.Library.all();
	var permissions = require('permissions/permissions');
	for(var i=0;i<col.length;i++){
		if((permissions.checkLibraryPermissions(col[i].ID).reading == true) && (permissions.checkSpacePermissions(col[i].space.ID).reading == true)){
			result.add(col[i]);
		}
	}
	return result;
};
