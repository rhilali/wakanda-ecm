

model.Library.methods.addLibrary = function(libraryData) {
							var space = ds.Space.find("ID= :1",libraryData.theSpace); 
							var basePath = ds.getModelFolder().path+"Base/";
							var spacePath = basePath + space.name;
							var libraryPath = spacePath +'/' + libraryData.name;
							var newLibrary = Folder(libraryPath);
							if (newLibrary.exists && libraryData.name.length != 0) {
								return {message: " This Library exists "};
							}else {
								var newLibraryEntity =  ds.Library.createEntity();     
          						newLibraryEntity.name = libraryData.name;     
          						newLibraryEntity.description = libraryData.description; 
          						newLibraryEntity.path = libraryPath;
          						newLibraryEntity.relativePath = space.name +'/' + libraryData.name;      
          						newLibraryEntity.space = space;
          					
          						try {
          							newLibraryEntity.save();
          							newLibrary.create();
								}
								catch(e) {
									return {message: e.messages[1]};
								}
							}
	
};
model.Library.methods.addLibrary.scope = "public";


model.Library.entityMethods.deleteLibrary = function() {
	var theLibraryToDelete = Folder(this.path);
	try {
		this.remove();
		theLibraryToDelete.remove();
	}
	catch(e) {
		return {message: e.messages[0]};
	}
};
model.Library.entityMethods.deleteLibrary.scope = "public";


model.Library.entityMethods.attributeStyle = function() {
	// Add your code here;
};


model.Library.entityMethods.displayContents = function() {
	var directories = this.directories;
	var documents = this.documents;
	var arrayDocuments = [];
	var arrayDirectories = directories.toArray("ID, name, path");
	for(var i=0 ; i < arrayDirectories.length ; i++){
	 arrayDirectories[i].type="directory";
	}
	arrayDocuments = documents.toArray("ID, name, path, version");
	for(var i=0 ; i < arrayDocuments.length ; i++){
	 arrayDocuments[i].type="document";
	}
	var result = arrayDirectories.concat(arrayDocuments);
	return result;
};
model.Library.entityMethods.displayContents.scope = "public";


model.Library.entityMethods.getAllDocuments = function() {
	var directories = this.directories;
	var documents = this.documents;
	var firstLevelDocs = documents.toArray("ID, name, path, version, relativePath");
	var allDocuments = [];
	for(var i =0; i<directories.length;i++){
		var docs = ds.Directory.getAllDocuments(directories[i].ID);
		for(var j= 0;j<docs.length;j++)
			allDocuments.push(docs[j]);
	}
	for(var m =0; m<firstLevelDocs.length;m++)
	allDocuments.push(firstLevelDocs[m]);
	return allDocuments;
	
};
model.Library.entityMethods.getAllDocuments.scope = "public";


model.Library.entityMethods.updateLibrary = function(libraryData) {
	var newPath = this.space.path + '/' +libraryData.name; ;
	if (Folder(newPath).exists && libraryData.name.length != 0 && this.name != libraryData.name) {
		return {message: " This name of library exists. Choose an other name"};
	}else {
		var oldPath = this.path;
		this.name = libraryData.name;  
		this.path = newPath;  
   		this.description = libraryData.description;     
    	this.updatingDate = new Date();
		try {
			this.save();
			Folder(oldPath).setName(libraryData.name);
		}
		catch(e) {
			return {message: e.messages[1]};
		}
	}
	
};
model.Library.entityMethods.updateLibrary.scope = "public";


model.Library.entityMethods.setPath = function(parent) {
	this.path = parent + '/' + this.name;
	this.save();
};
model.Library.entityMethods.setPath.scope = "public";


model.Library.entityMethods.getAllDirectories = function() {
	var directories = [];
	var firstLevelDirectories = this.directories;
	for(var i=0;i<firstLevelDirectories.length;i++){
		directories.push(firstLevelDirectories[i].ID);
		var tmpDirectories = ds.Directory.getAllDirectories(firstLevelDirectories[i].ID);
		for(var j=0;j<tmpDirectories.length;j++){
			directories.push(tmpDirectories[j]);
		}
	}
	// retun que les ID
	return directories;
};
model.Library.entityMethods.getAllDirectories.scope = "public";
