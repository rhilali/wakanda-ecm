

model.RoleGroupDirectory.methods.addRoleGroupDirectory = function(directory,group,role) {

	var dir = ds.Directory.find("ID=:1",directory);
	var gr = ds.Group.find("ID=:1",group);
			var entity = ds.RoleGroupDirectory.createEntity();
			entity.reading = role.reading;
			entity.contribution = role.contribution;
			entity.collaboration = role.collaboration
			entity.directory = dir;
			entity.group = gr;
			
			try {
          						entity.save();
          						return true;
							}
							catch(e) {
								return {message: e.messages[1]};
							}
};

model.RoleGroupDirectory.methods.addRoleGroupDirectory.scope = "public"


model.RoleGroupDirectory.entityMethods.updateRoleGroupDirectory = function(permissions) {
	this.reading = permissions.reading;     
    this.contribution = permissions.contribution;     
    this.collaboration = permissions.collaboration; 
    
    	
	try {
		this.save();
		return true;
	}
	catch(e) {
		return {message: e.messages[1]};
	}
};

model.RoleGroupDirectory.entityMethods.updateRoleGroupDirectory.scope = "public";