

model.RoleGroupLibrary.methods.addRoleGroupLibrary = function(library,group,role) {
	var lib = ds.Library.find("ID=:1",library);
	var gr = ds.Group.find("ID=:1",group);
			var entity = ds.RoleGroupLibrary.createEntity();
			entity.reading = role.reading;
			entity.contribution = role.contribution;
			entity.collaboration = role.collaboration
			entity.library = lib;
			entity.group = gr;
			
			try {
          						entity.save();
          						return true;
							}
							catch(e) {
								return {message: e.messages[1]};
							}
};

model.RoleGroupLibrary.methods.addRoleGroupLibrary.scope = "public";

model.RoleGroupLibrary.entityMethods.updateRoleGroupLibrary = function(permissions) {
	this.reading = permissions.reading;     
    this.contribution = permissions.contribution;     
    this.collaboration = permissions.collaboration; 
    
    	
	try {
		this.save();
		return true;
	}
	catch(e) {
		return {message: e.messages[1]};
	}
};

model.RoleGroupLibrary.entityMethods.updateRoleGroupLibrary.scope = "public";