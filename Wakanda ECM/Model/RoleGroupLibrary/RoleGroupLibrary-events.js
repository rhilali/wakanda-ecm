

model.RoleGroupLibrary.events.onValidate = function() {
	
		var result = {error: 0};
	var theErrors = [];
	var names = ds.RoleGroupLibrary.query("group.ID = :1 and library.ID= :2", this.group.ID,this.library.ID);
	names = names.first();
	var id = ds.RoleGroupLibrary.query('ID = :1', this.ID);
	if(names && id.length == 0){
		theErrors[0] = 'The group \n'+ this.group.name +' is already in the library: '+this.library.name;
	}
	if(theErrors.length > 0){
		result = {error: 1, errorMessage: theErrors};
	}
	return result;
};


model.RoleGroupLibrary.events.onSave = function() {
	//init values
	if(this.contribution){
		this.reading = true;
	}
	if(this.collaboration){
		this.contribution = true;
		this.reading = true;
	}
if(this.isNew()){
	// gestion heritage
	// on rajoute ces permissions pour first level directories
    var directories = this.library.directories;
    	for(var j=0;j<directories.length;j++){
			var oneRoleGroupDirectory = new ds.RoleGroupDirectory({
       			group:this.group,
           	 	directory: directories[j],
            	reading: this.reading,
            	contribution: this.contribution,
           		collaboration: this.collaboration
       		});
       		oneRoleGroupDirectory.save();
    	}
    // on rajoute ces permissions pour les firstLevel documents
    var documents = this.library.documents;
    for(var i=0;i<documents.length;i++){
		var oneAccessRuleGroupDocument = new ds.AccessRuleGroupDocument({
            group:this.group,
            document: documents[i],
            reading: this.reading,
            writing: this.contribution,
            deleting: this.collaboration
        });
        oneAccessRuleGroupDocument.save(); 
    }
}else{
	// on modifie l'heritage dans les directories enfants 
	var directories = this.library.directories;
	for (var m=0 ;m<directories.length;m++){
		if(directories[m].permissionInheritance == true){
			var roleGroupDirectory = ds.RoleGroupDirectory.find("directory.ID = :1 AND group.ID = :2",directories[m].ID, this.group.ID);
			if(roleGroupDirectory != null){
					roleGroupDirectory.group = this.group;
					roleGroupDirectory.directory = directories[m];
					roleGroupDirectory.reading = this.reading;
					roleGroupDirectory.contribution = this.contribution;
					roleGroupDirectory.collaboration = this.collaboration;
					roleGroupDirectory.save();
			}
		}
	}
	// on modifie l'heritage dans les documents enfants 
	var documents = this.library.documents;
	for (var n=0 ;n<documents.length;n++){
		if(documents[n].permissionInheritance == true){
			var accessRuleGroupDocument = ds.AccessRuleGroupDocument.find("document.ID = :1 AND group.ID = :2",documents[n].ID, this.group.ID);
			if(accessRuleGroupDocument != null){
					accessRuleGroupDocument.group = this.group;
					accessRuleGroupDocument.document = documents[n];
					accessRuleGroupDocument.reading = this.reading;
					accessRuleGroupDocument.writing = this.contribution;
					accessRuleGroupDocument.deleting = this.collaboration;
					accessRuleGroupDocument.save();
			}
		}
	}
}
};


model.RoleGroupLibrary.events.onRemove = function() {
	// on supprime l'heritage dans les directories enfants 
	var directories = this.library.directories;
	for (var i=0 ;i<directories.length;i++){
		if(directories[i].permissionInheritance == true){
			var roleGroupDirectories = ds.RoleGroupDirectory.query("directory.ID = :1 AND group.ID = :2",directories[i].ID, this.group.ID);
			if(roleGroupDirectories != null)
				roleGroupDirectories.remove();
		}
	}
	// on supprime l'heritage dans les documents enfants 
	var documents = this.library.documents;
	for (var j=0 ;j<documents.length;j++){
		if(documents[j].permissionInheritance == true){
			var accessRuleGroupDocuments = ds.AccessRuleGroupDocument.query("document.ID = :1 AND group.ID = :2",documents[j].ID, this.group.ID);
			if(accessRuleGroupDocuments != null)
				accessRuleGroupDocuments.remove();
		}
	}
};
