include("./Model/Admin/Admin-methods.js");
include("./Model/Manager/Manager-methods.js");
include("./Model/Group/Group-methods.js");
include("./Model/Email/Email-methods.js");
include("./Model/Session/Session-methods.js");
include("./Model/Library/Library-methods.js");
include("./Model/Space/Space-methods.js");
include("./Model/Directory/Directory-methods.js");
include("./Model/Task/Task-methods.js");
include("./Model/Document/Document-methods.js");
include("./Model/EmailModel/EmailModel-methods.js");
include("./Model/Group/Group-events.js");
include("./Model/Document/Document-events.js");
include("./Model/Space/Space-events.js");
include("./Model/Directory/Directory-events.js");
include("./Model/UserGroup/UserGroup-methods.js");
include("./Model/Library/Library-events.js");


include("./Model/UserGroup/UserGroup-events.js");

include("./Model/RoleGroupSpace/RoleGroupSpace-methods.js");
include("./Model/User/User-events.js");
include("./Model/Document/Document-calculated.js");

include("./Model/User/User-methods.js");
include("./Model/RoleGroupSpace/RoleGroupSpace-events.js");
include("./Model/RoleGroupLibrary/RoleGroupLibrary-methods.js");
include("./Model/RoleGroupLibrary/RoleGroupLibrary-events.js");

include("./Model/Versions/Versions-methods.js");
include("./Model/Versions/Versions-events.js");
include("./Model/RoleGroupDirectory/RoleGroupDirectory-events.js");
include("./Model/Directory/Directory-calculated.js");
include("./Model/RoleGroupDirectory/RoleGroupDirectory-methods.js");
include("./Model/RoleGroupDirectory/RoleGroupDirectory-events.js");
include("./Model/User/User-calculated.js");