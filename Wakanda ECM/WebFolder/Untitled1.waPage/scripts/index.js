﻿
WAF.onAfterInit = function onAfterInit() {// @lock

// @region namespaceDeclaration// @startlock
	var documentEvent = {};	// @document
	var button1 = {};	// @button
// @endregion// @endlock

// eventHandlers// @lock

 var author = function () { console.log("hhhhhhhhhhhhhhhh author"); },
          books = function () { console.log("books"); },
          viewBook = function(bookId) { console.log("viewBook: bookId is populated: " + bookId); };

      var routes = {
        '/author': author,
        '/books': [books, function() { console.log("An inline route handler."); }],
        '/books/view/:bookId': viewBook
      };

      var router = Router(routes);
      router.init();




	documentEvent.onLoad = function documentEvent_onLoad (event)// @startlock
	{// @endlock
		
     
	};// @lock

	button1.click = function button1_click (event)// @startlock
	{// @endlock
		window.location = '#/books/view/1';
	};// @lock

// @region eventManager// @startlock
	WAF.addListener("document", "onLoad", documentEvent.onLoad, "WAF");
	WAF.addListener("button1", "click", button1.click, "WAF");
// @endregion
};// @endlock
