﻿
WAF.onAfterInit = function onAfterInit() {// @lock

// @region namespaceDeclaration// @startlock
	var menuItem7 = {};	// @menuItem
	var login2 = {};	// @login
	var documentEvent = {};	// @document
	var menuItem3 = {};	// @menuItem
	var menuItem2 = {};	// @menuItem
	var menuItem1 = {};	// @menuItem
// @endregion// @endlock
	
// eventHandlers// @lock


	var loadOverview = function(){
		$$('bodyComponent').removeComponent();
		$$('menuItem1').addClass("isSelected");
		$$('menuItem2').removeClass("isSelected");
		$$('menuItem3').removeClass("isSelected");
		$$('menuItem7').removeClass("isSelected");
		$$('loaderComponent').loadComponent( '/Components/loader.waComponent');
		$$('bodyComponent').loadComponent({
			path: '/Components/overview.waComponent',
			onSuccess:function(){
				$$('loaderComponent').removeComponent();
			}
		});
	}
	var loadSpaces = function(){
		$$('bodyComponent').removeComponent();
		$$('menuItem2').addClass("isSelected");
		$$('menuItem1').removeClass("isSelected");
		$$('menuItem3').removeClass("isSelected");
		$$('menuItem7').removeClass("isSelected");
		$$('loaderComponent').loadComponent( '/Components/loader.waComponent');
		$$('bodyComponent').loadComponent({
			path: '/Components/spaces.waComponent',
			onSuccess:function(){
				$$('loaderComponent').removeComponent();
			}
		});
	}
	var loadSettings = function(tab){
		$$('bodyComponent').removeComponent();
		$$('menuItem3').addClass("isSelected");
		$$('menuItem1').removeClass("isSelected");
		$$('menuItem2').removeClass("isSelected");
		$$('menuItem7').removeClass("isSelected");
		$$('loaderComponent').loadComponent( '/Components/loader.waComponent');
		$$('bodyComponent').loadComponent({
			path: '/Components/settings.waComponent',
			onSuccess:function(){
				$$('loaderComponent').removeComponent();
				switch(tab){
					case "users":
						waf.widgets.bodyComponent_settingsTabView.selectTab(1);
						break;
					case "groups":
						waf.widgets.bodyComponent_settingsTabView.selectTab(2);
						break;
					case "permissions":
						waf.widgets.bodyComponent_settingsTabView.selectTab(3);
						break;
					default:
						window.location.hash = "settings/users";
				}
			}
		});
	}
	var loadMyfiles = function(){
		$$('bodyComponent').removeComponent();
		$$('menuItem7').addClass("isSelected");
		$$('menuItem1').removeClass("isSelected");
		$$('menuItem2').removeClass("isSelected");
		$$('menuItem3').removeClass("isSelected");
		$$('loaderComponent').loadComponent( '/Components/loader.waComponent');
		$$('bodyComponent').loadComponent({
			path: '/Components/myfiles.waComponent',
			onSuccess:function(){
				$$('loaderComponent').removeComponent();
			}
		});
	}
	menuItem7.click = function menuItem7_click (event)// @startlock
	{// @endlock
		window.location.hash = "myfiles";
	};// @lock

	login2.logout = function login2_logout (event)// @startlock
	{// @endlock
		window.location= "/index";
	};// @lock
	
	documentEvent.onLoad = function documentEvent_onLoad (event)// @startlock
	{// @endlock
		if (WAF.directory.currentUser()=== null) {
			window.location= "/index";
		}else{
			if(window.location.hash == ""){
				window.location.hash = "overview";
			}
		}
	};// @lock

	menuItem3.click = function menuItem3_click (event)// @startlock
	{// @endlock
		window.location.hash = "settings/users";
	};// @lock

	menuItem2.click = function menuItem2_click (event)// @startlock
	{// @endlock
		window.location.hash = "spaces";
	};// @lock

	menuItem1.click = function menuItem1_click (event)// @startlock
	{// @endlock
		window.location.hash = "overview";
	};// @lock
	var routes = {
  	   '/overview': loadOverview,
 	   '/spaces': loadSpaces,
 		'/settings/:tab': loadSettings,
 		'/myfiles': loadMyfiles,
 		'/*': function(){ window.location.hash = "overview";} 
	};
	var router = Router(routes);
	router.init();
	
// @region eventManager// @startlock
	WAF.addListener("menuItem7", "click", menuItem7.click, "WAF");
	WAF.addListener("login2", "logout", login2.logout, "WAF");
	WAF.addListener("document", "onLoad", documentEvent.onLoad, "WAF");
	WAF.addListener("menuItem3", "click", menuItem3.click, "WAF");
	WAF.addListener("menuItem2", "click", menuItem2.click, "WAF");
	WAF.addListener("menuItem1", "click", menuItem1.click, "WAF");
// @endregion
};// @endlock
