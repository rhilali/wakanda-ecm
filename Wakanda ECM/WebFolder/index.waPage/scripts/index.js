﻿
WAF.onAfterInit = function onAfterInit() {// @lock

// @region namespaceDeclaration// @startlock
	var documentEvent = {};	// @document
	var login = {};	// @login
// @endregion// @endlock

// eventHandlers// @lock

	documentEvent.onLoad = function documentEvent_onLoad (event)// @startlock
	{// @endlock
		if (WAF.directory.currentUser()!= null) {
			window.location= "/ECM";
		}
	};// @lock

	login.login = function login_login (event)// @startlock
	{// @endlock
		window.location= "/ECM";
	};// @lock

// @region eventManager// @startlock
	WAF.addListener("document", "onLoad", documentEvent.onLoad, "WAF");
	WAF.addListener("login", "login", login.login, "WAF");
// @endregion
};// @endlock
