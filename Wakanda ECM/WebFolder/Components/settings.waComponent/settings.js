﻿
(function Component (id) {// @lock

// Add the code that needs to be shared between components here

function constructor (id) {

	// @region beginComponentDeclaration// @startlock
	var $comp = this;
	this.name = 'settings';
	// @endregion// @endlock

	this.load = function (data) {// @lock

	// @region namespaceDeclaration// @startlock
	var menuItem3 = {};	// @menuItem
	var menuItem2 = {};	// @menuItem
	var menuItem1 = {};	// @menuItem
	var imageButtonCloseDlgEm = {};	// @buttonImage
	var buttonCancelEmail = {};	// @button
	var buttonSendEmail = {};	// @button
	var imageButtonSaveUpRolDir = {};	// @buttonImage
	var imageButtonCloserolDir = {};	// @buttonImage
	var buttonCancelRolDir = {};	// @button
	var buttonDeletRolDir = {};	// @button
	var imageButtonCloseDeletDir = {};	// @buttonImage
	var buttonClearaddRolDir = {};	// @button
	var buttonSaveRolDir = {};	// @button
	var imageButtonCloseAddRolDir = {};	// @buttonImage
	var imageButtonUpRolGroupDir = {};	// @buttonImage
	var imageButtonDelRolGrpDir = {};	// @buttonImage
	var imageButtonAddRolGrpDir = {};	// @buttonImage
	var imageButtonBackDirectory = {};	// @buttonImage
	var imageButtonNext = {};	// @buttonImage
	var dataGridLibrariesList = {};	// @dataGrid
	var imageButton9 = {};	// @buttonImage
	var button12 = {};	// @button
	var button11 = {};	// @button
	var imageButton8 = {};	// @buttonImage
	var button10 = {};	// @button
	var button9 = {};	// @button
	var imageButton7 = {};	// @buttonImage
	var button8 = {};	// @button
	var button7 = {};	// @button
	var button6 = {};	// @button
	var button5 = {};	// @button
	var imageButton6 = {};	// @buttonImage
	var button3 = {};	// @button
	var button4 = {};	// @button
	var imageButton5 = {};	// @buttonImage
	var buttonDelete = {};	// @button
	var buttonCancel = {};	// @button
	var imageButtonCloseDialog = {};	// @buttonImage
	var imageButton2 = {};	// @buttonImage
	var imageButtonCloseLibraryUpdate = {};	// @buttonImage
	var buttonClearRolLibrary = {};	// @button
	var buttonSaveLibraryRol = {};	// @button
	var imageButtonCloseAddLibrary = {};	// @buttonImage
	var imageButtonAddLibrary = {};	// @buttonImage
	var imageButtonDeletLibrary = {};	// @buttonImage
	var imageButtonEditLibrary = {};	// @buttonImage
	var imageButtonBack = {};	// @buttonImage
	var imageButtonSaveS = {};	// @buttonImage
	var imageButtonClosEdSp = {};	// @buttonImage
	var imageButtonSaveUpGrp = {};	// @buttonImage
	var imageButtonCloseUpGrp = {};	// @buttonImage
	var imageButtonClseAddGrp = {};	// @buttonImage
	var imageButtonSaveUser = {};	// @buttonImage
	var imageButtonCloseEdAcc = {};	// @buttonImage
	var imageButtonCloseUs = {};	// @buttonImage
	var imageButtonDelRol = {};	// @buttonImage
	var imageButtonDelete = {};	// @buttonImage
	var imageButtonDeletGr = {};	// @buttonImage
	var dataGridSpaces = {};	// @dataGrid
	var imageButtonSaveUpRol = {};	// @buttonImage
	var imageButtonCloseUp = {};	// @buttonImage
	var imageButtonUpRol = {};	// @buttonImage
	var buttonSaveGR = {};	// @button
	var buttonClearGR = {};	// @button
	var imageButtonCloseAdR = {};	// @buttonImage
	var imageButtonAddRol = {};	// @buttonImage
	var imageButtonDel = {};	// @buttonImage
	var imageButtonMinus = {};	// @buttonImage
	var dataGridListGroup = {};	// @dataGrid
	var imageButtonEnrg = {};	// @buttonImage
	var imageButtonPlus = {};	// @buttonImage
	var imageButton4 = {};	// @buttonImage
	var imageButtonUp = {};	// @buttonImage
	var imageButtonEnr = {};	// @buttonImage
	var imageButtonAd = {};	// @buttonImage
	var imageButtonSaveSelect = {};	// @buttonImage
	var icon8 = {};	// @icon
	var imageButtonAddGr = {};	// @buttonImage
	var imageButtonClose = {};	// @buttonImage
	var imageButtonAddUs = {};	// @buttonImage
	var imageButtonUpdate = {};	// @buttonImage
	var userDataGrid = {};	// @dataGrid
	var imageButtonAdd = {};	// @buttonImage
	// @endregion// @endlock

	// eventHandlers// @lock

	menuItem3.click = function menuItem3_click (event)// @startlock
	{// @endlock
		window.location.hash = "settings/permissions";
	};// @lock

	menuItem2.click = function menuItem2_click (event)// @startlock
	{// @endlock
		window.location.hash = "settings/groups";
	};// @lock

	menuItem1.click = function menuItem1_click (event)// @startlock
	{// @endlock
		window.location.hash = "settings/users";
	};// @lock

	imageButtonCloseDlgEm.click = function imageButtonCloseDlgEm_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("DialogSendEmail")).hide();
	};// @lock

	buttonCancelEmail.click = function buttonCancelEmail_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('DialogSendEmail')).hide();
	};// @lock

	buttonSendEmail.click = function buttonSendEmail_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("user")].remove();
		$$(getHtmlId('DialogDeleteUser')).hide();
		
	};// @lock

	imageButtonSaveUpRolDir.click = function imageButtonSaveUpRolDir_click (event)// @startlock
	{// @endlock

//      debugger;
		var currentRolGroupDir = waf.sources[getHtmlId('roleGroupDirectoryCollection1')];
		var roles = {
			reading:$$(getHtmlId("checkboxReadRolDir")).getValue() ,
			contribution: $$(getHtmlId("checkboxContRolDir")).getValue(),
			collaboration:$$(getHtmlId("checkboxCollGrRolDir")).getValue()
		};
		currentRolGroupDir.updateRoleGroupDirectory({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerUpRolDir')).hide();
	           			waf.sources[getHtmlId("directories")].serverRefresh();
	           			waf.sources[getHtmlId("roleGroupDirectoryCollection1")].serverRefresh();
	           			}
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	           	}
		,roles);
		
		
		
	};// @lock

	imageButtonCloserolDir.click = function imageButtonCloserolDir_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUpRolDir")).hide();
	};// @lock

	buttonCancelRolDir.click = function buttonCancelRolDir_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('DialogDeletRolDir')).hide();
	};// @lock

	buttonDeletRolDir.click = function buttonDeletRolDir_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("roleGroupDirectoryCollection1")].remove();
		$$(getHtmlId('DialogDeletRolDir')).hide();
		
	};// @lock

	imageButtonCloseDeletDir.click = function imageButtonCloseDeletDir_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("DialogDeletRolDir")).hide();
	};// @lock

	buttonClearaddRolDir.click = function buttonClearaddRolDir_click (event)// @startlock
	{// @endlock
			$$(getHtmlId("comboboxGroupDir")).setValue("");
			$$(getHtmlId("checkboxReadingRolDir")).uncheck();
			$$(getHtmlId("checkboxCollRolDir")).uncheck();
			$$(getHtmlId("checkboxContrRolDir")).uncheck();
			$(getHtmlObj('errorDivGroupDir')).html('');
	};// @lock

	buttonSaveRolDir.click = function buttonSaveRolDir_click (event)// @startlock
	{// @endlock
		
		var directoryCurrent = WAF.sources[getHtmlId("directories")].getCurrentElement().ID.value;
		var group = $$(getHtmlId("comboboxGroupDir")).getValue();
		var permissions = {
			reading:$$(getHtmlId("checkboxReadingRolDir")).getValue() ,
			contribution: $$(getHtmlId("checkboxContrRolDir")).getValue(),
			collaboration:$$(getHtmlId("checkboxCollRolDir")).getValue()
		};
		ds.RoleGroupDirectory.addRoleGroupDirectory({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerAddDirectoryRole')).hide();
	           			waf.sources[getHtmlId("directories")].serverRefresh();
	           			waf.sources[getHtmlId("roleGroupDirectoryCollection1")].serverRefresh();
	           			$$(getHtmlId("checkboxReadingRolDir")).uncheck();
						$$(getHtmlId("checkboxCollRolDir")).uncheck();
						$$(getHtmlId("checkboxContrRolDir")).uncheck();
	           			$(getHtmlObj('errorDivGroupDir')).html('');
	           			}
	           			
	           		else{
	           			
	           			var errors = event.result.message.split(',');
	           			$(getHtmlObj('errorDivGroupDir')).html(errors[0]);
	           			}
	           		
	           		
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	           	}
		
		,directoryCurrent,group,permissions);
		
	};// @lock

	imageButtonCloseAddRolDir.click = function imageButtonCloseAddRolDir_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddDirectoryRole")).hide();
		$$(getHtmlId("checkboxReadingRolDir")).uncheck();
		$$(getHtmlId("checkboxCollRolDir")).uncheck();
		$$(getHtmlId("checkboxContrRolDir")).uncheck();
	    $(getHtmlObj('errorDivGroupDir')).html('');
	};// @lock

	imageButtonUpRolGroupDir.click = function imageButtonUpRolGroupDir_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUpRolDir")).show();
		$$(getHtmlId("comboboxGrouprolDir")).disable();
	};// @lock

	imageButtonDelRolGrpDir.click = function imageButtonDelRolGrpDir_click (event)// @startlock
	{// @endlock
//		var isOk = confirm("Are you sure to delete this?");
//			if(isOk){
//				waf.sources[getHtmlId("roleGroupLibraryCollection")].remove();
//			}

			$$(getHtmlId('DialogDeletRolDir')).show();
	};// @lock

	imageButtonAddRolGrpDir.click = function imageButtonAddRolGrpDir_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddDirectoryRole")).show();
	};// @lock

	imageButtonBackDirectory.click = function imageButtonBackDirectory_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('containerDirectoriesList')).hide();
	};// @lock

	imageButtonNext.click = function imageButtonNext_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('containerDirectoriesList')).show();
	};// @lock

	dataGridLibrariesList.onRowDblClick = function dataGridLibrariesList_onRowDblClick (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerDirectoriesList")).show();
	};// @lock

	imageButton9.click = function imageButton9_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("DialogdeletGroupUser")).hide();
	};// @lock

	button12.click = function button12_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('DialogdeletGroupUser')).hide();
	};// @lock

	button11.click = function button11_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("userGroupCollection")].remove();
		$$(getHtmlId('DialogdeletGroupUser')).hide();
		
	};// @lock

	imageButton8.click = function imageButton8_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("DialogDeleteUser")).hide();
	};// @lock

	button10.click = function button10_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('DialogDeleteUser')).hide();
	};// @lock

	button9.click = function button9_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("user")].remove();
		$$(getHtmlId('DialogDeleteUser')).hide();
		
	};// @lock

	imageButton7.click = function imageButton7_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("DialogDeletUserGr")).hide();
	};// @lock

	button8.click = function button8_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('DialogDeletUserGr')).hide();
	};// @lock

	button7.click = function button7_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("userGroupCollection5")].remove();
		$$(getHtmlId('DialogDeletUserGr')).hide();
		
	};// @lock

	button6.click = function button6_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('DialogDeletGroup')).hide();
	};// @lock

	button5.click = function button5_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("group6")].remove();
		$$(getHtmlId('DialogDeletGroup')).hide();
		
	};// @lock

	imageButton6.click = function imageButton6_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("DialogDeletGroup")).hide();
	};// @lock

	button3.click = function button3_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("roleGroupSpaceCollection")].remove();
		$$(getHtmlId('DialogDeletRoleSpace')).hide();
		
	};// @lock

	button4.click = function button4_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('DialogDeletRoleSpace')).hide();
	};// @lock

	imageButton5.click = function imageButton5_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("DialogDeletRoleSpace")).hide();
	};// @lock

	buttonDelete.click = function buttonDelete_click (event)// @startlock
	{// @endlock
		waf.sources[getHtmlId("roleGroupLibraryCollection")].remove();
		$$(getHtmlId('containerDialogDeletion')).hide();
		
	};// @lock

	buttonCancel.click = function buttonCancel_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('containerDialogDeletion')).hide();
	};// @lock

	imageButtonCloseDialog.click = function imageButtonCloseDialog_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerDialogDeletion")).hide();
	};// @lock

	imageButton2.click = function imageButton2_click (event)// @startlock
	{// @endlock

//      debugger;
		var currentRolGroupLib = waf.sources[getHtmlId('roleGroupLibraryCollection')];
		var roles = {
			reading:$$(getHtmlId("checkboxReadingUpRol")).getValue() ,
			contribution: $$(getHtmlId("checkboxContUpRol")).getValue(),
			collaboration:$$(getHtmlId("checkboxCollUpRol")).getValue()
		};
		currentRolGroupLib.updateRoleGroupLibrary({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerUpdateRoleLibrary')).hide();
	           			waf.sources[getHtmlId("libraries")].serverRefresh();
	           			waf.sources[getHtmlId("roleGroupLibraryCollection")].serverRefresh();
	           			}
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	           	}
		,roles);
		
		
		
	};// @lock

	imageButtonCloseLibraryUpdate.click = function imageButtonCloseLibraryUpdate_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUpdateRoleLibrary")).hide();
	};// @lock

	buttonClearRolLibrary.click = function buttonClearRolLibrary_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("comboboxGroupLibrary")).setValue("");
		$$(getHtmlId("checkboxReadLibrary")).uncheck();
		$$(getHtmlId("checkboxCollLibrary")).uncheck();
		$$(getHtmlId("checkboxContrLibrary")).uncheck();
		$(getHtmlObj('errorDivGroupLibrary')).html('');
	};// @lock

	buttonSaveLibraryRol.click = function buttonSaveLibraryRol_click (event)// @startlock
	{// @endlock
		
		var libraryCurrent = WAF.sources[getHtmlId("libraries")].getCurrentElement().ID.value;
		var group = $$(getHtmlId("comboboxGroupLibrary")).getValue();
		var permissions = {
			reading:$$(getHtmlId("checkboxReadLibrary")).getValue() ,
			contribution: $$(getHtmlId("checkboxContrLibrary")).getValue(),
			collaboration:$$(getHtmlId("checkboxCollLibrary")).getValue()
		};
		
		ds.RoleGroupLibrary.addRoleGroupLibrary({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerAddLibraryRole')).hide();
	           			waf.sources[getHtmlId("libraries")].serverRefresh();
	           			waf.sources[getHtmlId("roleGroupLibraryCollection")].serverRefresh();
	           			$(getHtmlObj('errorDivGroupLibrary')).html('');
	           			}
	           			
	           		else{
	           			
	           			var errors = event.result.message.split(',');
	           			$(getHtmlObj('errorDivGroupLibrary')).html(errors[0]);
	           			}
	           		
	           		
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	           	}
		
		,libraryCurrent,group,permissions);
		
	};// @lock

	imageButtonCloseAddLibrary.click = function imageButtonCloseAddLibrary_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddLibraryRole")).hide();
	};// @lock

	imageButtonAddLibrary.click = function imageButtonAddLibrary_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddLibraryRole")).show();
	};// @lock

	imageButtonDeletLibrary.click = function imageButtonDeletLibrary_click (event)// @startlock
	{// @endlock
//		var isOk = confirm("Are you sure to delete this?");
//			if(isOk){
//				waf.sources[getHtmlId("roleGroupLibraryCollection")].remove();
//			}

			$$(getHtmlId('containerDialogDeletion')).show();
	};// @lock

	imageButtonEditLibrary.click = function imageButtonEditLibrary_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUpdateRoleLibrary")).show();
		$$(getHtmlId("comboboxGroupUpRol")).disable();
	};// @lock

	imageButtonBack.click = function imageButtonBack_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('containerLibrariesList')).hide();
	};// @lock

	imageButtonSaveS.click = function imageButtonSaveS_click (event)// @startlock
	{// @endlock
		var currentSpace = waf.sources[getHtmlId('space')];
		var spaceData = {
			name: $$(getHtmlId('textFieldNameS')).getValue(),
			description: $$(getHtmlId('textFieldDescriptionS')).getValue(),
			manager: $$(getHtmlId('comboboxManager')).getValue(),
			quota: $$(getHtmlId('textFieldQtS')).getValue()		
		};
		currentSpace.updateSpace({
			onSuccess: function(event){
				if(event.result == true){
					$$(getHtmlId('containerUpdateSpace')).hide();
					waf.sources[getHtmlId('space')].sererRefresh();
					$(getHtmlObj('errorDivNameSp')).html('');
					$$(getHtmlId('comboboxManager')).setValue("");
					
				} else {
					var errors = event.result.message.split(',');
					$(getHtmlObj('errorDivNameSp')).html(errors[0]);
					$(getHtmlObj('errorDivManagSp')).html(errors[2]);
				}
			},
			onError: function(error) {
				alert(error.result.message);
			}
		},spaceData);
	};// @lock

	imageButtonClosEdSp.click = function imageButtonClosEdSp_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('containerUpdateSpace')).hide();
	};// @lock

	imageButtonSaveUpGrp.click = function imageButtonSaveUpGrp_click (event)// @startlock
	{// @endlock

		//debugger;
		var currentGroup = waf.sources[getHtmlId('group6')];
//		var selectN = [];
//		selectN[0] = $$(getHtmlId('dataGridListGroup')).getSelectedRows();
//		var selectN = $$(getHtmlId('dataGridListGroup')).getSelectedRows(); 
		var upGroup = {
			name: $$(getHtmlId('textField12')).getValue(),
			description: $$(getHtmlId('textField13')).getValue(),
			nbrUsers: $$(getHtmlId('textField14')).getValue()
		};
//		var selectN = $$(getHtmlId('userDataGrid')).getSelectedRows(); 
		currentGroup.UpdateGroup({
								onSuccess: function(event){
									if(event.result == true){
										$$(getHtmlId('containerUpdateGrp')).hide();
										$(getHtmlObj('errorDivNameGrUp')).html('');
										waf.sources[getHtmlId("group6")].serverRefresh();
										//$$(getHtmlId('dataGridListGroup')).setSelectedRows(selectN);
														
									} else {
										var errors = event.result.message.split(',');
										$(getHtmlObj('errorDivNameGrUp')).html(errors[0]);
										$(getHtmlObj('errorDivNameGrUp')).html(errors[1]);
									}
								},
								onError: function(error) {
									//$$(getHtmlId('accountContainer')).hide();
									alert(error.result.message);
								}
							},upGroup);
			
	};// @lock

	imageButtonCloseUpGrp.click = function imageButtonCloseUpGrp_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('containerUpdateGrp')).hide();
	};// @lock

	imageButtonClseAddGrp.click = function imageButtonClseAddGrp_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('containerAddGroup')).hide();
	};// @lock

	imageButtonSaveUser.click = function imageButtonSaveUser_click (event)// @startlock
	{// @endlock
		var currentUser = waf.sources[getHtmlId('user')];
		var userAcc = {
			firstName: $$(getHtmlId('textField1')).getValue(),
			lastName: $$(getHtmlId('textField2')).getValue(),
    		email: $$(getHtmlId('textField3')).getValue(),
			telNumber: $$(getHtmlId('textField5')).getValue(),
			address: $$(getHtmlId('textFieldAddress')).getValue(),
			quota: $$(getHtmlId('textField6')).getValue(),
			login: $$(getHtmlId('textField7')).getValue()
		};
		var newPass =$$(getHtmlId('textFieldPassEdAcc')).getValue();
		var confirmPass = $$(getHtmlId('textFieldConfirmEA')).getValue();
		if(newPass != null && newPass != ""){
			if(newPass == confirmPass){
				
							userAcc.password = $$(getHtmlId('textFieldConfirmEA')).getValue();
							
							currentUser.EditAccount2({
								onSuccess: function(event){
									if(event.result == true){
										$$(getHtmlId('updateUserContainer')).hide();
										waf.sources[getHtmlId('user')].serverRefresh();
										$(getHtmlObj('errorDivFirstNameUp')).html('');
										$(getHtmlObj('errorDivLastNameUp')).html('');
										$(getHtmlObj('errorDivEmailUp')).html('');
										$(getHtmlObj('errorDivLoginUp')).html("");
										$(getHtmlObj('errorDivConfirmPss')).html("");
														
									} else {
										var errors = event.result.message.split(',');
										$(getHtmlObj('errorDivFirstNameUp')).html(errors[0]);
										$(getHtmlObj('errorDivLastNameUp')).html(errors[1]);
										$(getHtmlObj('errorDivEmailUp')).html(errors[2]);
										$(getHtmlObj('errorDivLoginUp')).html(errors[3]);
										$(getHtmlObj('errorDivLoginUp')).html(errors[4]);
									}
								},
								onError: function(error) {
									//$$(getHtmlId('accountContainer')).hide();
									alert(error.result.message);
								}
							},userAcc);
						}
							
					
					
		
		else {
			$(getHtmlObj('errorDivConfirmPss')).html("Pasword And Confirmation are different");
			return;
			
		}
			
		}
		else {
			
			currentUser.EditAccount2({
								onSuccess: function(event){
									if(event.result == true){
										$$(getHtmlId('updateUserContainer')).hide();
										waf.sources[getHtmlId('user')].serverRefresh();
										$(getHtmlObj('errorDivFirstNameUp')).html('');
										$(getHtmlObj('errorDivLastNameUp')).html('');
										$(getHtmlObj('errorDivEmailUp')).html('');
										$(getHtmlObj('errorDivLoginUp')).html("");
										$(getHtmlObj('errorDivConfirmPss')).html("");
														
									} else {
										var errors = event.result.message.split(',');
										$(getHtmlObj('errorDivFirstNameUp')).html(errors[0]);
										$(getHtmlObj('errorDivLastNameUp')).html(errors[1]);
										$(getHtmlObj('errorDivEmailUp')).html(errors[2]);
										$(getHtmlObj('errorDivLoginUp')).html(errors[3]);
									}
								},
								onError: function(error) {
									//$$(getHtmlId('accountContainer')).hide();
									alert(error.result.message);
								}
							},userAcc);
		}
		
	};// @lock

	imageButtonCloseEdAcc.click = function imageButtonCloseEdAcc_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("updateUserContainer")).hide();
	};// @lock

	imageButtonCloseUs.click = function imageButtonCloseUs_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("addUserContainer")).hide();
	};// @lock

	imageButtonDelRol.click = function imageButtonDelRol_click (event)// @startlock
	{// @endlock
//		var isOk = confirm("Are you sure to delete this?");
//			if(isOk){
//				waf.sources[getHtmlId("roleGroupSpaceCollection")].remove();
//			}
			$$(getHtmlId("DialogDeletRoleSpace")).show();
	};// @lock

	imageButtonDelete.click = function imageButtonDelete_click (event)// @startlock
	{// @endlock
//		var isOk = confirm("Are you sure to delete this?");
//			if(isOk){
//				waf.sources[getHtmlId("user")].remove();
//			}
		$$(getHtmlId("DialogDeleteUser")).show();
	};// @lock

	imageButtonDeletGr.click = function imageButtonDeletGr_click (event)// @startlock
	{// @endlock
//		var isOk = confirm("Are you sure to delete this?");
//			if(isOk){
//				waf.sources[getHtmlId("userGroupCollection")].remove();
//			}
			$$(getHtmlId('DialogdeletGroupUser')).show();
	};// @lock

	dataGridSpaces.onRowDblClick = function dataGridSpaces_onRowDblClick (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerLibrariesList")).show();
	};// @lock

	imageButtonSaveUpRol.click = function imageButtonSaveUpRol_click (event)// @startlock
	{// @endlock

//      debugger;
		var currentRolGroupSp = waf.sources[getHtmlId('roleGroupSpaceCollection')];
		var roles = {
			reading:$$(getHtmlId("checkboxRedUp")).getValue() ,
			contribution: $$(getHtmlId("checkboxContUp")).getValue(),
			collaboration:$$(getHtmlId("checkboxCollUp")).getValue()
		};
		currentRolGroupSp.updateRoleGroupSpace({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerUpGrpRol')).hide();
	           			waf.sources[getHtmlId("space")].serverRefresh();
	           			waf.sources[getHtmlId("roleGroupSpaceCollection")].serverRefresh();
	           			}
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	           	}
		,roles);
		
		
		
	};// @lock

	imageButtonCloseUp.click = function imageButtonCloseUp_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUpGrpRol")).hide();
	};// @lock

	imageButtonUpRol.click = function imageButtonUpRol_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUpGrpRol")).show();
		$$(getHtmlId("comboboxUpGr")).disable();
	};// @lock

	buttonSaveGR.click = function buttonSaveGR_click (event)// @startlock
	{// @endlock
		
		var spaceCurrent = WAF.sources[getHtmlId("space")].getCurrentElement().ID.value;
		var group = $$(getHtmlId("comboboxGroup")).getValue();
		var permissions = {
			reading:$$(getHtmlId("checkboxReading")).getValue() ,
			contribution: $$(getHtmlId("checkboxContribution")).getValue(),
			collaboration:$$(getHtmlId("checkboxCollaboration")).getValue()
		};
		
		ds.RoleGroupSpace.addRoleGroupSpace({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerAddGrpRol')).hide();
	           			waf.sources[getHtmlId("space")].serverRefresh();
	           			waf.sources[getHtmlId("roleGroupSpaceCollection")].serverRefresh();
//	           			$$(getHtmlId('userDataGrid')).setSelectedRows(getSelectedRows);
//	           			$$(getHtmlId('groupDataGrid')).refresh();
	           			$(getHtmlObj('errorDivGroupCombo')).html('');
	           			}
	           			
	           		else{
	           			
	           			var errors = event.result.message.split(',');
	           			$(getHtmlObj('errorDivGroupCombo')).html(errors[0]);
	           			}
	           		
	           		
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	           	}
		
		,spaceCurrent,group,permissions);
//		$$(getHtmlId("dataGridGrPer")).hide();
	
////	waf.sources[getHtmlId("roleGroupSpaceCollection")].collectioRefresh();
//		waf.sources[getHtmlId("roleGroupSpaceCollection")].serverRefresh();
////		waf.sources[getHtmlId("roleGroupSpaceCollection")].autoDispach();
//		$$(getHtmlId("dataGridGrPer")).show();
//		$$(getHtmlId("containerAddGrpRol")).hide();
		
	};// @lock

	buttonClearGR.click = function buttonClearGR_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("comboboxGroup")).setValue("");
		$$(getHtmlId("checkboxReading")).uncheck();
		$$(getHtmlId("checkboxCollaboration")).uncheck();
		$$(getHtmlId("checkboxContribution")).uncheck();
		$(getHtmlObj('errorDivGroupCombo')).html('');
	};// @lock

	imageButtonCloseAdR.click = function imageButtonCloseAdR_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddGrpRol")).hide();
	};// @lock

	imageButtonAddRol.click = function imageButtonAddRol_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddGrpRol")).show();
	};// @lock

	imageButtonDel.click = function imageButtonDel_click (event)// @startlock
	{// @endlock
//		var isOk = confirm("Are you sure to delete this?");
//			if(isOk){
//				waf.sources[getHtmlId("group6")].remove();
//			}
			
			$$(getHtmlId("DialogDeletGroup")).show();
	};// @lock

	imageButtonMinus.click = function imageButtonMinus_click (event)// @startlock
	{// @endlock
//		var isOk = confirm("Are you sure to delete this?");
//			if(isOk){
//				waf.sources[getHtmlId("userGroupCollection5")].remove();
//			}
			
			$$(getHtmlId('DialogDeletUserGr')).show();
			
	};// @lock

	dataGridListGroup.onRowClick = function dataGridListGroup_onRowClick (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUsers")).show();
		$$(getHtmlId("containerAddGroup")).hide();
		$$(getHtmlId("containerUpdateGrp")).hide();
	};// @lock

	imageButtonEnrg.click = function imageButtonEnrg_click (event)// @startlock
	{// @endlock
		var allUsers = WAF.sources[getHtmlId("user1")].getEntityCollection();
		var groupCurrent = WAF.sources[getHtmlId("group6")].getCurrentElement();
		var selectedEntity = waf.widgets[getHtmlId("dataGridUs")].getSelection();
		
		var users = [];
		allUsers.buildFromSelection(selectedEntity, {
	    onSuccess: function(ev) {
	    	
	    		ev.entityCollection.forEach(function(e) {
      		  users.push(e.entity.ID.value);
        
    });
	           ds.UserGroup.addUsersGroup({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerListUs')).hide();
	           			waf.sources[getHtmlId("group6")].serverRefresh();
//	           			$$(getHtmlId('userDataGrid')).setSelectedRows(getSelectedRows);
//	           			$$(getHtmlId('groupDataGrid')).refresh();
	           			$(getHtmlObj('errorDivSelectUser')).html('');
	           			}
	           			
	           		else{
	           			
	           			var errors = event.result.message.split(',');
	           			$(getHtmlObj('errorDivSelectUser')).html(errors[1]);
	           			}
	           		
	           		
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	           	}
	           ,users,groupCurrent.ID.value);
	    }
	});
	
//			$$('bodyComponent').loadComponent({path: '/Components/settings.waComponent'});

		
	};// @lock

	imageButtonPlus.click = function imageButtonPlus_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerListUs")).show();
	};// @lock

	imageButton4.click = function imageButton4_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerListUs")).hide();
	};// @lock

	imageButtonUp.click = function imageButtonUp_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUpdateGrp")).show()
		$$(getHtmlId("containerUsers")).show()
		$$(getHtmlId("containerAddGroup")).hide()
		
	};// @lock

	imageButtonEnr.click = function imageButtonEnr_click (event)// @startlock
	{// @endlock
		var nGroup = {
			name: $$(getHtmlId("textFieldNameGr")).getValue(),
			description: $$(getHtmlId("textFieldDesc")).getValue(),
			nbrUsers: $$(getHtmlId("textFieldNbr")).getValue()
		};
		ds.Group.addGroup({
			onSuccess :function(event){
				if(event.result == true){
	           			
	           			$$(getHtmlId('containerAddGroup')).hide();
//	           			waf.sources[getHtmlId("group6")].serverRefresh(); 
						waf.sources[getHtmlId("group6")].all();    			
	           			$(getHtmlObj('errorDivName')).html('');
	           			}
	           			
	           		else{
	           			
	           			var errors = event.result.message.split(',');
	           			$(getHtmlObj('errorDivName')).html(errors[0]);
	           			$(getHtmlObj('errorDivName')).html(errors[1]);
	           			}
			},
			onError:function(error){
				alert(error.result.message);
			}
		},nGroup);
									
	};// @lock

	imageButtonAd.click = function imageButtonAd_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerUsers")).show();
		$$(getHtmlId("containerUpdateGrp")).hide();
		$$(getHtmlId("containerAddGroup")).show();
	};// @lock

	imageButtonSaveSelect.click = function imageButtonSaveSelect_click (event)// @startlock
	{// @endlock

		var allGroups = WAF.sources[getHtmlId("group")].getEntityCollection();
		var userCurrent = WAF.sources[getHtmlId("user")].getCurrentElement();
		var selectedEntity = waf.widgets[getHtmlId("dataGridGrps")].getSelection();
		
		var groups = [];
		allGroups.buildFromSelection(selectedEntity, {
	    onSuccess: function(ev) {
	    		ev.entityCollection.forEach(function(e) {
      		  groups.push(e.entity.ID.value);
        
    });
    			
	           ds.UserGroup.addUserGroup({
	           	onSuccess:function(event ){
	           		if(event.result == true){
	           			
	           			$$(getHtmlId('containerAddGrps')).hide();
	           			waf.sources[getHtmlId("user")].serverRefresh();
//	           			$$(getHtmlId('userDataGrid')).setSelectedRows(getSelectedRows);
//	           			$$(getHtmlId('groupDataGrid')).refresh();
	           			$(getHtmlObj('errorDivGroups')).html('');
	           			}
	           			
	           		else{
	           			
	           			var errors = event.result.message.split(',');
	           			$(getHtmlObj('errorDivGroups')).html(errors[0]);
	           			}
	           		
	           		
	           		
	           		},
	           		
	           	onError:function(error){
	           		alert(error.result.message);
	           		}
	       }	,userCurrent.ID.value,groups);
	    }
	});
	
//				waf.sources[getHtmlId("userGroupCollection")].allEntities();
//				$$(getHtmlId("containerAddGrps")).hide();
	
			

		
	};// @lock

	icon8.click = function icon8_click (event)// @startlock
	{// @endlock
		//$$(getHtmlId("containerAddGrps")).hide();
	};// @lock

	imageButtonAddGr.click = function imageButtonAddGr_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddGrps")).show();
	};// @lock

	imageButtonClose.click = function imageButtonClose_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("containerAddGrps")).hide();
	};// @lock

	imageButtonAddUs.click = function imageButtonAddUs_click (event)// @startlock
	{// @endlock


		var nUser = {
			firstName: $$(getHtmlId("textFieldFname")).getValue(),
			lastName: $$(getHtmlId("textFieldLname")).getValue(),
			email: $$(getHtmlId("textFieldEmail")).getValue(),
			telNumber: $$(getHtmlId("textFieldTelNum")).getValue(),
			address: $$(getHtmlId("textFieldAddAddress")).getValue(),
			quotaUser: $$(getHtmlId("textFieldQuota")).getValue(),
			login: $$(getHtmlId("textFieldLogin")).getValue(),
			password: $$(getHtmlId("textFieldPass")).getValue(),	
			checked : $$(getHtmlId("checkBoxSendEmail")).getValue()
		};


			
		ds.User.addUser({
								onSuccess: function(event){
									if(event.result == true){
										$$(getHtmlId('addUserContainer')).hide();
//										debugger;
										waf.sources[getHtmlId("user")].all();
										$(getHtmlObj('errorDivFirst')).html('');
										$(getHtmlObj('errorDivLast')).html('');
										$(getHtmlObj('errorDivEmailU')).html('');
										$(getHtmlObj('errorDivLogin')).html('');
																
														
									} else {
										var errors = event.result.message.split(',');
										$(getHtmlObj('errorDivFirst')).html(errors[0]);
										$(getHtmlObj('errorDivLast')).html(errors[1]);
										$(getHtmlObj('errorDivEmailU')).html(errors[2]);
										$(getHtmlObj('errorDivLogin')).html(errors[3]);
										$(getHtmlObj('errorDivLogin')).html(errors[4]);
										
									}
								},
								onError: function(error) {
									//$$(getHtmlId('accountContainer')).hide();
									alert(error.result.message);
								}
							},nUser);
		//waf.sources[getHtmlId("user")].allEntities();

	};// @lock

	imageButtonUpdate.click = function imageButtonUpdate_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("updateUserContainer")).show();
		$$(getHtmlId("groupDataGrid")).show();
		$$(getHtmlId("documentDataGrid")).show();
		$$(getHtmlId("addUserContainer")).hide();
		$$(getHtmlId("imageButtonAddGr")).show();
		$$(getHtmlId("imageButtonDeletGr")).show();
		$$(getHtmlId("containerGridGroup")).show();
	};// @lock

	userDataGrid.onRowClick = function userDataGrid_onRowClick (event)// @startlock
	{// @endlock
		$$(getHtmlId("groupDataGrid")).show();
		$$(getHtmlId("documentDataGrid")).show();
		$$(getHtmlId("containerGridGroup")).show();
		$$(getHtmlId("imageButtonAddGr")).show();
		$$(getHtmlId("imageButtonDeletGr")).show();
		$$(getHtmlId("addUserContainer")).hide();
		$$(getHtmlId("updateUserContainer")).hide();
	};// @lock

	imageButtonAdd.click = function imageButtonAdd_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("groupDataGrid")).show();
		$$(getHtmlId("documentDataGrid")).show();
		$$(getHtmlId("updateUserContainer")).hide();
		$$(getHtmlId("imageButtonAddGr")).show();
		$$(getHtmlId("imageButtonDeletGr")).show();
		$$(getHtmlId("containerGridGroup")).show();
		$$(getHtmlId("addUserContainer")).show();
		

	};// @lock

	// @region eventManager// @startlock
	WAF.addListener(this.id + "_menuItem3", "click", menuItem3.click, "WAF");
	WAF.addListener(this.id + "_menuItem2", "click", menuItem2.click, "WAF");
	WAF.addListener(this.id + "_menuItem1", "click", menuItem1.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseDlgEm", "click", imageButtonCloseDlgEm.click, "WAF");
	WAF.addListener(this.id + "_buttonCancelEmail", "click", buttonCancelEmail.click, "WAF");
	WAF.addListener(this.id + "_buttonSendEmail", "click", buttonSendEmail.click, "WAF");
	WAF.addListener(this.id + "_imageButtonSaveUpRolDir", "click", imageButtonSaveUpRolDir.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloserolDir", "click", imageButtonCloserolDir.click, "WAF");
	WAF.addListener(this.id + "_buttonCancelRolDir", "click", buttonCancelRolDir.click, "WAF");
	WAF.addListener(this.id + "_buttonDeletRolDir", "click", buttonDeletRolDir.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseDeletDir", "click", imageButtonCloseDeletDir.click, "WAF");
	WAF.addListener(this.id + "_buttonClearaddRolDir", "click", buttonClearaddRolDir.click, "WAF");
	WAF.addListener(this.id + "_buttonSaveRolDir", "click", buttonSaveRolDir.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseAddRolDir", "click", imageButtonCloseAddRolDir.click, "WAF");
	WAF.addListener(this.id + "_imageButtonUpRolGroupDir", "click", imageButtonUpRolGroupDir.click, "WAF");
	WAF.addListener(this.id + "_imageButtonDelRolGrpDir", "click", imageButtonDelRolGrpDir.click, "WAF");
	WAF.addListener(this.id + "_imageButtonAddRolGrpDir", "click", imageButtonAddRolGrpDir.click, "WAF");
	WAF.addListener(this.id + "_imageButtonBackDirectory", "click", imageButtonBackDirectory.click, "WAF");
	WAF.addListener(this.id + "_imageButtonNext", "click", imageButtonNext.click, "WAF");
	WAF.addListener(this.id + "_dataGridLibrariesList", "onRowDblClick", dataGridLibrariesList.onRowDblClick, "WAF");
	WAF.addListener(this.id + "_imageButton9", "click", imageButton9.click, "WAF");
	WAF.addListener(this.id + "_button12", "click", button12.click, "WAF");
	WAF.addListener(this.id + "_button11", "click", button11.click, "WAF");
	WAF.addListener(this.id + "_imageButton8", "click", imageButton8.click, "WAF");
	WAF.addListener(this.id + "_button10", "click", button10.click, "WAF");
	WAF.addListener(this.id + "_button9", "click", button9.click, "WAF");
	WAF.addListener(this.id + "_imageButton7", "click", imageButton7.click, "WAF");
	WAF.addListener(this.id + "_button8", "click", button8.click, "WAF");
	WAF.addListener(this.id + "_button7", "click", button7.click, "WAF");
	WAF.addListener(this.id + "_button6", "click", button6.click, "WAF");
	WAF.addListener(this.id + "_button5", "click", button5.click, "WAF");
	WAF.addListener(this.id + "_imageButton6", "click", imageButton6.click, "WAF");
	WAF.addListener(this.id + "_button3", "click", button3.click, "WAF");
	WAF.addListener(this.id + "_button4", "click", button4.click, "WAF");
	WAF.addListener(this.id + "_imageButton5", "click", imageButton5.click, "WAF");
	WAF.addListener(this.id + "_buttonDelete", "click", buttonDelete.click, "WAF");
	WAF.addListener(this.id + "_buttonCancel", "click", buttonCancel.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseDialog", "click", imageButtonCloseDialog.click, "WAF");
	WAF.addListener(this.id + "_imageButton2", "click", imageButton2.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseLibraryUpdate", "click", imageButtonCloseLibraryUpdate.click, "WAF");
	WAF.addListener(this.id + "_buttonClearRolLibrary", "click", buttonClearRolLibrary.click, "WAF");
	WAF.addListener(this.id + "_buttonSaveLibraryRol", "click", buttonSaveLibraryRol.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseAddLibrary", "click", imageButtonCloseAddLibrary.click, "WAF");
	WAF.addListener(this.id + "_imageButtonAddLibrary", "click", imageButtonAddLibrary.click, "WAF");
	WAF.addListener(this.id + "_imageButtonDeletLibrary", "click", imageButtonDeletLibrary.click, "WAF");
	WAF.addListener(this.id + "_imageButtonEditLibrary", "click", imageButtonEditLibrary.click, "WAF");
	WAF.addListener(this.id + "_imageButtonBack", "click", imageButtonBack.click, "WAF");
	WAF.addListener(this.id + "_imageButtonSaveS", "click", imageButtonSaveS.click, "WAF");
	WAF.addListener(this.id + "_imageButtonClosEdSp", "click", imageButtonClosEdSp.click, "WAF");
	WAF.addListener(this.id + "_imageButtonSaveUpGrp", "click", imageButtonSaveUpGrp.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseUpGrp", "click", imageButtonCloseUpGrp.click, "WAF");
	WAF.addListener(this.id + "_imageButtonClseAddGrp", "click", imageButtonClseAddGrp.click, "WAF");
	WAF.addListener(this.id + "_imageButtonSaveUser", "click", imageButtonSaveUser.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseEdAcc", "click", imageButtonCloseEdAcc.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseUs", "click", imageButtonCloseUs.click, "WAF");
	WAF.addListener(this.id + "_imageButtonDelRol", "click", imageButtonDelRol.click, "WAF");
	WAF.addListener(this.id + "_imageButtonDelete", "click", imageButtonDelete.click, "WAF");
	WAF.addListener(this.id + "_imageButtonDeletGr", "click", imageButtonDeletGr.click, "WAF");
	WAF.addListener(this.id + "_dataGridSpaces", "onRowDblClick", dataGridSpaces.onRowDblClick, "WAF");
	WAF.addListener(this.id + "_imageButtonSaveUpRol", "click", imageButtonSaveUpRol.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseUp", "click", imageButtonCloseUp.click, "WAF");
	WAF.addListener(this.id + "_imageButtonUpRol", "click", imageButtonUpRol.click, "WAF");
	WAF.addListener(this.id + "_buttonSaveGR", "click", buttonSaveGR.click, "WAF");
	WAF.addListener(this.id + "_buttonClearGR", "click", buttonClearGR.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloseAdR", "click", imageButtonCloseAdR.click, "WAF");
	WAF.addListener(this.id + "_imageButtonAddRol", "click", imageButtonAddRol.click, "WAF");
	WAF.addListener(this.id + "_imageButtonDel", "click", imageButtonDel.click, "WAF");
	WAF.addListener(this.id + "_imageButtonMinus", "click", imageButtonMinus.click, "WAF");
	WAF.addListener(this.id + "_dataGridListGroup", "onRowClick", dataGridListGroup.onRowClick, "WAF");
	WAF.addListener(this.id + "_imageButtonEnrg", "click", imageButtonEnrg.click, "WAF");
	WAF.addListener(this.id + "_imageButtonPlus", "click", imageButtonPlus.click, "WAF");
	WAF.addListener(this.id + "_imageButton4", "click", imageButton4.click, "WAF");
	WAF.addListener(this.id + "_imageButtonUp", "click", imageButtonUp.click, "WAF");
	WAF.addListener(this.id + "_imageButtonEnr", "click", imageButtonEnr.click, "WAF");
	WAF.addListener(this.id + "_imageButtonAd", "click", imageButtonAd.click, "WAF");
	WAF.addListener(this.id + "_imageButtonSaveSelect", "click", imageButtonSaveSelect.click, "WAF");
	WAF.addListener(this.id + "_icon8", "click", icon8.click, "WAF");
	WAF.addListener(this.id + "_imageButtonAddGr", "click", imageButtonAddGr.click, "WAF");
	WAF.addListener(this.id + "_imageButtonClose", "click", imageButtonClose.click, "WAF");
	WAF.addListener(this.id + "_imageButtonAddUs", "click", imageButtonAddUs.click, "WAF");
	WAF.addListener(this.id + "_imageButtonUpdate", "click", imageButtonUpdate.click, "WAF");
	WAF.addListener(this.id + "_userDataGrid", "onRowClick", userDataGrid.onRowClick, "WAF");
	WAF.addListener(this.id + "_imageButtonAdd", "click", imageButtonAdd.click, "WAF");
	// @endregion// @endlock

	};// @lock


}// @startlock
return constructor;
})();// @endlock
