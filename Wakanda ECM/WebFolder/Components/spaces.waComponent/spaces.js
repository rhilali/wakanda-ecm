﻿
(function Component (id) {// @lock

// Add the code that needs to be shared between components here

function constructor (id) {

	// @region beginComponentDeclaration// @startlock
	var $comp = this;
	this.name = 'spaces';
	// @endregion// @endlock
	var actionsContainer = getHtmlObj('actionsContainer');
		
	this.load = function (data) {// @lock

	// @region namespaceDeclaration// @startlock
	var cancelSaveDocumentFromGoogleButton = {};	// @button
	var saveDocumentFromGoogleButton = {};	// @button
	var optionCreateRadioGroup = {};	// @radioGroup
	var closeChooseOptionCreateButton = {};	// @buttonImage
	var cancelChooseOptionCreateButton = {};	// @button
	var createDocumentButton = {};	// @button
	var okErrorVersionButton = {};	// @button
	var downloadVersionButton = {};	// @image
	var viewVersionButton = {};	// @image
	var skipMetaDataNewVersionButton = {};	// @button
	var closeMetaDataNewVersionButton = {};	// @buttonImage
	var validateMetaDataNewVersionButton = {};	// @button
	var saveRevertButton = {};	// @button
	var closeDeleteVersionButton = {};	// @buttonImage
	var deleteVersionButton = {};	// @image
	var cancelDeleteVersionButton = {};	// @button
	var confirmDeleteVersionButton = {};	// @button
	var closeRevertButton = {};	// @buttonImage
	var cancelRevertButton = {};	// @button
	var revertToVersionButton = {};	// @image
	var imageButton11 = {};	// @buttonImage
	var displayVersionsButton = {};	// @button
	var uploadNewVersionDocButton = {};	// @button
	var uploadNewVersionButton = {};	// @button
	var closeNewVersionButton = {};	// @buttonImage
	var saveNewVersionButton = {};	// @button
	var newVersionFileUpload = {};	// @fileUpload
	var okNotSavedDocsButton = {};	// @button
	var closeModifyDirectoryButton = {};	// @buttonImage
	var saveModifyDirectoryButton = {};	// @button
	var cancelModifyDirectoryButton = {};	// @button
	var modifyLibraryButton = {};	// @button
	var closeModifyLibraryButton = {};	// @buttonImage
	var saveModifyLibraryButton = {};	// @button
	var cancelModifyLibraryButton = {};	// @button
	var DeleteDocumentButton = {};	// @button
	var closeDeleteDocumentButton = {};	// @buttonImage
	var cancelDeleteDocumentButton = {};	// @button
	var confirmDeleteDocumentButton = {};	// @button
	var deleteDirectoryButton = {};	// @button
	var closeDeleteDirectoryButton = {};	// @buttonImage
	var cancelDeleteDirectoryButton = {};	// @button
	var confirmDeleteDirectoryButton = {};	// @button
	var deleteLibraryButton = {};	// @button
	var imageButton9 = {};	// @buttonImage
	var cancelDeleteLibraryButton = {};	// @button
	var confirmDeleteLibraryButton = {};	// @button
	var matrixLibraries = {};	// @matrix
	var cancelSaveNewVersionButton = {};	// @button
	var actionsDirectoryContainer = {};	// @container
	var actionsDirectoryContainer = {};	// @container
	var cancelEditMetaData = {};	// @button
	var imageButton10 = {};	// @buttonImage
	var saveMetaDataButton = {};	// @button
	var saveMetaDataButton = {};	// @button
	var editMetaDataButton = {};	// @button
	var actionsLibraryContainer = {};	// @container
	var iconSelectLibrary = {};	// @icon
	var skipButton = {};	// @button
	var queryTextField = {};	// @textField
	var closeMetaDataContainerButton = {};	// @buttonImage
	var imageButton8 = {};	// @buttonImage
	var newDocumentButton = {};	// @buttonImage
	var newDirectoryButton = {};	// @buttonImage
	var imageButton7 = {};	// @buttonImage
	var imageButton6 = {};	// @buttonImage
	var imageButton4 = {};	// @buttonImage
	var imageButton1 = {};	// @buttonImage
	var imageButton5 = {};	// @buttonImage
	var matrixSpaces = {};	// @matrix
	var buttonDownload = {};	// @button
	var buttonView = {};	// @button
	var iconActionDoc = {};	// @icon
	var containerActionDoc = {};	// @container
	var imageButton3 = {};	// @buttonImage
	var cancelSearchButton = {};	// @buttonImage
	var searchButton = {};	// @buttonImage
	var documentUpload = {};	// @fileUpload
	var validateMetaDataButton = {};	// @button
	var button2 = {};	// @button
	var saveDocumentButton = {};	// @button
	var cancelDocumentButton = {};	// @button
	var saveDirectoryButton = {};	// @button
	var cancelDirectoryButton = {};	// @button
	var documentContainer = {};	// @container
	var matrixDocuments = {};	// @matrix
	var librariesContainer = {};	// @container
	var backToLibraryButton = {};	// @buttonImage
	var SaveLibraryButton = {};	// @button
	var cancelCreateLibraryButton = {};	// @button
	var backButton = {};	// @buttonImage
	var addLibraryButton = {};	// @buttonImage
	var confirmDeleteButton = {};	// @button
	var cancelDeleteButton = {};	// @button
	var deleteButton = {};	// @button
	var modifySaveButton = {};	// @button
	var modifyCancelButton = {};	// @button
	var modifyButton = {};	// @button
	var save = {};	// @button
	var newSpaceButton = {};	// @buttonImage
	var cancel = {};	// @button
	var actionsContainer = {};	// @container
	var iconActions = {};	// @icon
	var spaceContainer = {};	// @container
	// @endregion// @endlock
// variables declaration
	var currentSpace;
	var currentLibrary;
	var currentDirectory = null;
	var directoryToDelete;
	var documentToDelete;
	var documentToUploadNewVersion;
	var documentToDisplayVersions;
	var versionToRevert;
	var versionToDelete;
	var pileTreeDirectories = [];
	var spacePermissions;
	var libraryPermissions;
	var directoryPermissions;
	var myFilesArray = [];
	var documentsToSave = [];
	var myNewVersionFile;
	var versionToSave;
	var fileNamesArray = [];
	
// initialisation des icons des actions

	var icons = getHtmlObj('matrixSpaces').find('.iconShow');
	$(icons).hide();
	var iconsLibrary = getHtmlObj('matrixLibraries').find('.iconSelectLibraryClass');
	$(iconsLibrary).hide();	
	var iconsDoc = getHtmlObj('matrixDocuments').find('.iconDoc');
	$(iconsDoc).hide();
//
	$$(getHtmlId('resultSearchTextField')).hide();

// init Widjets 
		$$(getHtmlId('newSpaceButton')).disable();

// init Permissions
		try{
			var isAdmin = dashboardNS.isAdmin();
		}
		catch(e){
			alert("Error: " + e.message);
		}
		if(isAdmin == true){
			$$(getHtmlId('newSpaceButton')).enable();
		}
		
	// eventHandlers// @lock

	cancelSaveDocumentFromGoogleButton.click = function cancelSaveDocumentFromGoogleButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('createDocumentGoogleDocsContainer')).hide();
	};// @lock

	saveDocumentFromGoogleButton.click = function saveDocumentFromGoogleButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('loadGoogleDocsImage')).show();
		$$(getHtmlId('createDocumentGoogleDocsContainer')).hide();
		var theParentDirectory = (currentDirectory == null)? null : currentDirectory;
		var documentParentData = {
				theLibrary: currentLibrary.ID,
				theParentDirectory: (currentDirectory == null)? null : currentDirectory.ID	
		}
		var responseJSON = docs.getGoogleDocs(responseGoogleDocs.id);
		var response = JSON.parse(responseJSON);
		var title = response.title;
		var mimeType = response.mimeType;
		var link ="";
		
		if(mimeType == "application/vnd.google-apps.document"){
			 link = response.exportLinks["application/vnd.openxmlformats-officedocument.wordprocessingml.document"];
		}
		if(mimeType == "application/vnd.google-apps.presentation"){
			 link = response.exportLinks["application/vnd.openxmlformats-officedocument.presentationml.presentation"];
		}
		if(mimeType == "application/vnd.google-apps.spreadsheet"){
			link = response.exportLinks["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"];
		}
		var uploadFromGoogleResult = docs.save(link,title,mimeType,responseGoogleDocs.id, documentParentData);	
		if(uploadFromGoogleResult.exists == true){
			alert("this file exists");
		}else{
			var documentData = {
				docs: [uploadFromGoogleResult.fileToSave],
				theLibrary: currentLibrary.ID,
				theParentDirectory: (currentDirectory == null)? null : currentDirectory.ID	
			}
			waf.ds.Document.addDocument({
				onSuccess: function(event){
						responseGoogleDocs = {};
						$$(getHtmlId('metaDataContainer')).show();
						metaDataArray = [];
						metaDataArray = event.result.metaData;	
						waf.sources.metaDataArray.sync();
						//image loading management 
						$$(getHtmlId('loadGoogleDocsImage')).hide();	
						if(event.result.error.length != 0){
							var message = "";
							for(var i=0;i<event.result.error.length;i++){						
								message += event.result.error[i] + "<br/>";
							}
							$$(getHtmlId('displayErrorsSaveDocumentsContainer')).show();
							$(getHtmlObj('NotSavedDocumentsErrorDiv')).html(message);		
						}			
				},
				onError: function(error) {
					alert(error.result.message);
					//image loading management 
					$$(getHtmlId('loadGoogleDocsImage')).hide();	
				}
			},documentData);
			waf.sources.metaDataArray.sync();
		}
		
	};// @lock

	optionCreateRadioGroup.change = function optionCreateRadioGroup_change (event)// @startlock
	{// @endlock
		var option = $$(getHtmlId('optionCreateRadioGroup')).getValue();
		if(option == "googleDocs"){
			$$(getHtmlId('documentTypeRadioGroup')).show();
		}else{
			$$(getHtmlId('documentTypeRadioGroup')).hide();
		}
	};// @lock

	closeChooseOptionCreateButton.click = function closeChooseOptionCreateButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("chooseOptionCreateContainer")).hide();
	};// @lock

	cancelChooseOptionCreateButton.click = function cancelChooseOptionCreateButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("chooseOptionCreateContainer")).hide();
		
	};// @lock

	createDocumentButton.click = function createDocumentButton_click (event)// @startlock
	{// @endlock
		var option = $$(getHtmlId('optionCreateRadioGroup')).getValue();
		if(option == "local"){
			$$(getHtmlId('chooseOptionCreateContainer')).hide();
			$$(getHtmlId('newDocumentContainer')).show();
			//image loading management 
			$$(getHtmlId('saveDocumentButton')).show();
			$$(getHtmlId('loadingImage')).hide();	
			$$(getHtmlId('loadingImage')).hide();
			//vider les eventuelles précedents erreurs
			$(getHtmlObj('uploadErrorDiv')).html("");
			$(getHtmlObj('uploaderrorsDiv')).html("");
		}else{
			$$(getHtmlId('chooseOptionCreateContainer')).hide();
			var documentType = $$(getHtmlId('documentTypeRadioGroup')).getValue();
			var CLIENT_ID = '183978444548-car17r6n9p575caa3oq6a3somj9id7ia.apps.googleusercontent.com';
   			var SCOPES = 'https://www.googleapis.com/auth/drive';
   			$$(getHtmlId('frame1')).hide();
   			$$(getHtmlId('loadGoogleDocsImage')).show();
			$$(getHtmlId('createDocumentGoogleDocsContainer')).show();
			if(documentType == "word"){
       				window.setTimeout(checkAuthForDocx, 1);
					function checkAuthForDocx(){
      					gapi.auth.authorize({'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': true}, handleAuthResultForDocx);
					}
  					function handleAuthResultForDocx(authResult){
      					if (authResult && !authResult.error) {
         					var token = authResult.access_token;
          					docs.setAccessToken(token);
          					var path = docs.createLocalFile("fileTest.txt","folderTest","test test test");
							if(docs.existsFile(path)){
								var responseJSON = docs.createGoogleDocs(path,"docx");
								var response = JSON.parse(responseJSON);
								responseGoogleDocs = response;
								sources.responseGoogleDocs.sync();
								$$(getHtmlId('frame1')).show();
   								$$(getHtmlId('loadGoogleDocsImage')).hide();
								$('#'+$$(getHtmlId('frame1')).id +' ' +'iframe').attr('src',response.alternateLink);
								
							}
        				}else{
							gapi.auth.authorize({'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': false}, handleAuthResultForDocx);
						}
					}
			}else{
				if(documentType == "excel"){ 
					window.setTimeout(checkAuthForXlsx, 1);
					function checkAuthForXlsx(){
      					gapi.auth.authorize({'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': true}, handleAuthResultForXlsx);
					}
  					function handleAuthResultForXlsx(authResult){
      					if (authResult && !authResult.error) {
         					var token = authResult.access_token;
          					docs.setAccessToken(token);
          					var path = docs.createLocalFile("fileTest.txt","folderTest","test test test");
							if(docs.existsFile(path)){
								var responseJSON = docs.createGoogleDocs(path,"xlsx");
								var response = JSON.parse(responseJSON);
								responseGoogleDocs = response;
								sources.responseGoogleDocs.sync();
								$('#'+$$(getHtmlId('frame1')).id +' ' +'iframe').attr('src',response.alternateLink);
								$$(getHtmlId('frame1')).show();
   								$$(getHtmlId('loadGoogleDocsImage')).hide();
							}
        				}else{
							gapi.auth.authorize({'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': false}, handleAuthResultForXlsx);
						}
					}
				}else{
					if(documentType == "ppt"){
					window.setTimeout(checkAuthForPptx, 1);
					function checkAuthForPptx(){
      					gapi.auth.authorize({'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': true}, handleAuthResultForPptx);
					}
  					function handleAuthResultForPptx(authResult){
      					if (authResult && !authResult.error) {
         					var token = authResult.access_token;
          					docs.setAccessToken(token);
          					var path = docs.createLocalFile("fileTest.txt","folderTest","test test test");
							if(docs.existsFile(path)){
								var responseJSON = docs.createGoogleDocs(path,"pptx");
								var response = JSON.parse(responseJSON);
								responseGoogleDocs = response;
								sources.responseGoogleDocs.sync();
								$('#'+$$(getHtmlId('frame1')).id +' ' +'iframe').attr('src',response.alternateLink);
								$$(getHtmlId('frame1')).show();
   								$$(getHtmlId('loadGoogleDocsImage')).hide();
							}
        				}else{
							gapi.auth.authorize({'client_id': CLIENT_ID, 'scope': SCOPES, 'immediate': false}, handleAuthResultForPptx);
						}
					}
					}
				}
				
			}
		}
	};// @lock

	okErrorVersionButton.click = function okErrorVersionButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('displayErrorNewVersionContainer')).hide();
	};// @lock

	downloadVersionButton.click = function downloadVersionButton_click (event)// @startlock
	{// @endlock
		var id = waf.sources.olderVersionsArray.getCurrentElement().ID;
		var url = 'http://localhost:8081/loadVersion?id='+id;
		window.open (url,id);
	};// @lock

	viewVersionButton.click = function viewVersionButton_click (event)// @startlock
	{// @endlock
		var id = waf.sources.olderVersionsArray.getCurrentElement().ID;
		var url = 'http://localhost:8081/readVersion?id='+id;
		window.open (url,id);
	};// @lock

	skipMetaDataNewVersionButton.click = function skipMetaDataNewVersionButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('metaDataNewVersionContainer')).hide();
		metaDataArray = [];
		waf.sources.metaDataArray.sync();
		reloadContents();
	};// @lock

	closeMetaDataNewVersionButton.click = function closeMetaDataNewVersionButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('metaDataNewVersionContainer')).hide();
		metaDataArray = [];
		waf.sources.metaDataArray.sync();
		reloadContents();
	};// @lock

	validateMetaDataNewVersionButton.click = function validateMetaDataNewVersionButton_click (event)// @startlock
	{// @endlock
		var currentMetaData = waf.sources.metaDataArray.getCurrentElement();
		try{
			var result = dashboardNS.updateMetaData(currentMetaData);
		}catch(e){
			alert("Error: " + e.message);
		}
		var rsp = eval("("+result.response+")");
		if(rsp.responseHeader.status != 0)
			alert(rsp.error.msg);
		$$(getHtmlId('metaDataNewVersionContainer')).hide();
		reloadContents();
		metaDataArray = [];
		waf.sources.metaDataArray.sync();
	};// @lock

	saveRevertButton.click = function saveRevertButton_click (event)// @startlock
	{// @endlock
		var versionData = {
			versionID: versionToRevert.ID,
			versionType: $$(getHtmlId('newVersionToRevertRadioGroup')).getValue(),
			comment: $$(getHtmlId('commentRevertVersion')).getValue()
		}
		waf.ds.Versions.revertToOlderVersion({
			onSuccess: function(event){
				$$(getHtmlId("revertToVersionContainer")).hide();
				waf.ds.Versions.getAllVersions({
					onSuccess: function(event){
						lastVersionObject = event.result.lastVersion;
						waf.sources.lastVersionObject.sync();
						olderVersionsArray = event.result.olderVersions;
						waf.sources.olderVersionsArray.sync();
					},
					onError: function(error){
			
					}
				}, documentToDisplayVersions.ID);
			},
			onError: function(error) {
				
			}
		
		}, versionData);
	};// @lock

	closeDeleteVersionButton.click = function closeDeleteVersionButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('cofirmDeleteVersionContainer')).hide();
	};// @lock

	deleteVersionButton.click = function deleteVersionButton_click (event)// @startlock
	{// @endlock
		versionToDelete = waf.sources.olderVersionsArray.getCurrentElement();
		$$(getHtmlId('cofirmDeleteVersionContainer')).show();
		$$(getHtmlId('versionInfo')).setValue(versionToDelete.version + "  of  " + versionToDelete.documentName);
	};// @lock

	cancelDeleteVersionButton.click = function cancelDeleteVersionButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("cofirmDeleteVersionContainer")).hide();
		
	};// @lock

	confirmDeleteVersionButton.click = function confirmDeleteVersionButton_click (event)// @startlock
	{// @endlock
		waf.ds.Versions.deleteVersion({
			onSuccess: function(event){
				$$(getHtmlId("cofirmDeleteVersionContainer")).hide();
				waf.ds.Versions.getAllVersions({
					onSuccess: function(event){
						lastVersionObject = event.result.lastVersion;
						waf.sources.lastVersionObject.sync();
						olderVersionsArray = event.result.olderVersions;
						waf.sources.olderVersionsArray.sync();
					},
					onError: function(error){
			
					}
				}, documentToDisplayVersions.ID);
			},
			onError: function(error) {
				
			}
		
		}, versionToDelete.ID);
		
	};// @lock

	closeRevertButton.click = function closeRevertButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('revertToVersionContainer')).hide();
	};// @lock

	cancelRevertButton.click = function cancelRevertButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('revertToVersionContainer')).hide();
	};// @lock

	revertToVersionButton.click = function revertToVersionButton_click (event)// @startlock
	{// @endlock
		versionToRevert = waf.sources.olderVersionsArray.getCurrentElement();
		$$(getHtmlId('revertToVersionContainer')).show();
		$$(getHtmlId('commentRevertVersion')).setValue("");
		$$(getHtmlId('documentNameToRevert')).setValue("(" + versionToRevert.documentName + ")");
		// gestion de la position
					var beforePosition = $$(getHtmlId('documentNameToRevert')).getPosition(); 
					var beforeWidth = $$(getHtmlId('documentNameToRevert')).getWidth(); 
					var leftAfter = (beforePosition.left) + beforeWidth;
					$$(getHtmlId('richText55')).setLeft(leftAfter+4);
					beforePosition = $$(getHtmlId('richText55')).getPosition();
					beforeWidth = $$(getHtmlId('richText55')).getWidth();
					$$(getHtmlId('versionToRevert')).setLeft(beforePosition.left + beforeWidth + 4);
		$$(getHtmlId('versionToRevert')).setValue(versionToRevert.version);
	};// @lock

	imageButton11.click = function imageButton11_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('displayVersionsContainer')).hide();
		reloadContents();
	};// @lock

	displayVersionsButton.click = function displayVersionsButton_click (event)// @startlock
	{// @endlock
		olderVersionsArray = [];
		documentToDisplayVersions = waf.sources.documentsDirectories.getCurrentElement();
		waf.ds.Versions.getAllVersions({
			onSuccess: function(event){
				lastVersionObject = event.result.lastVersion;
				waf.sources.lastVersionObject.sync();
				olderVersionsArray = event.result.olderVersions;
				waf.sources.olderVersionsArray.sync();
				$$(getHtmlId('displayVersionsContainer')).show();
				$$(getHtmlId('docNameTODisplayVersion')).setValue(documentToDisplayVersions.name);
				waf.sources.olderVersionsArray.sync();
				// gestion de la position
					var beforePosition = $$(getHtmlId('richText40')).getPosition(); 
					var beforeWidth = $$(getHtmlId('richText40')).getWidth(); 
					var leftAfter = (beforePosition.left) + beforeWidth;
					$$(getHtmlId('richText47')).setLeft(leftAfter+4);
					beforePosition = $$(getHtmlId('richText47')).getPosition();
					$$(getHtmlId('richText48')).setLeft(beforePosition.left + 25);
						// on matrix
//					var beforePosition2 = $$(getHtmlId('richText45')).getPosition();
//					var beforeWidth2 = $$(getHtmlId('richText45')).getWidth();
//					$$(getHtmlId('richText49')).setLeft((beforePosition2.left) + beforeWidth2 +4);
//					beforePosition2 = $$(getHtmlId('richText49')).getPosition();
//					$$(getHtmlId('richText50')).setLeft(beforePosition2.left + 25);
			},
			onError: function(error){
			
			}
		}, documentToDisplayVersions.ID);
		waf.sources.olderVersionsArray.sync();
	};// @lock

	uploadNewVersionDocButton.click = function uploadNewVersionDocButton_click (event)// @startlock
	{// @endlock
		myNewVersionFile = null;
		myNewVersionFile = $$(getHtmlId('newVersionFileUpload')).getFiles();
		$$(getHtmlId('uploadVersionLoadImage')).show();
	};// @lock

	uploadNewVersionButton.click = function uploadNewVersionButton_click (event)// @startlock
	{// @endlock
		documentToUploadNewVersion = waf.sources.documentsDirectories.getCurrentElement();
		$$(getHtmlId('newVersionContainer')).show();
		$$(getHtmlId('button4')).enable();
		$(getHtmlObj('errorDiv13')).html("");
		$$(getHtmlId('commentVersionTextField')).setValue("");
		$$(getHtmlId('docNameToNewVersion')).setValue(documentToUploadNewVersion.name);
	};// @lock

	closeNewVersionButton.click = function closeNewVersionButton_click (event)// @startlock
	{// @endlock
		if(versionToSave == null){
			$$(getHtmlId('newVersionContainer')).hide();
		}else{
     		try {
     			var result = dashboardNS.cancelUploadVersion(versionToSave);
			}catch(e){
				alert("Error: " + e.message);
			}
			$$(getHtmlId('newVersionContainer')).hide();
			versionToSave = null;
			alert(result.message);
		}
	};// @lock

	saveNewVersionButton.click = function saveNewVersionButton_click (event)// @startlock
	{// @endlock
		if(versionToSave == null){
			$(getHtmlObj('errorDiv13')).html(" You should upload one file !");
		}else{
			//image loading 
			$$(getHtmlId('saveNewVersionButton')).hide();
			$$(getHtmlId('saveNewVersionLoadImage')).show();
			//traitement
			var versionData = {
				document: documentToUploadNewVersion,
				versionToSave: versionToSave,
				versionType: $$(getHtmlId('versionRadioGroup')).getValue(),
				comment: $$(getHtmlId('commentVersionTextField')).getValue()
			}
			waf.ds.Versions.saveVersion({
				onSuccess: function(event){	
						metaDataArray = [];
						waf.sources.metaDataArray.sync();
						$$(getHtmlId('saveNewVersionButton')).show();
						$$(getHtmlId('saveNewVersionLoadImage')).hide();
						$$(getHtmlId('newVersionContainer')).hide();
						if(event.result.error.length != 0){
							var message = "";
							for(var i=0;i<event.result.error.length;i++){						
								message += event.result.error[i] + "<br/>";
							}
							$$(getHtmlId('displayErrorNewVersionContainer')).show();
							$(getHtmlObj('NotSavedVersionErrorDiv')).html(message);		
						}else{
							$$(getHtmlId('metaDataNewVersionContainer')).show();
							metaDataArray = event.result.metaData;	
							waf.sources.metaDataArray.sync();	
						}	
						versionToSave = null;		
				},
				onError: function(error) {
					$$(getHtmlId('newVersionContainer')).hide();
					alert(error.result.message);
					//image loading management 
					$$(getHtmlId('saveNewVersionButton')).show();
					$$(getHtmlId('saveNewVersionLoadImage')).hide();	
				}
			},versionData);
//			waf.sources.metaDataArray.sync();
		}
	};// @lock

	newVersionFileUpload.filesUploaded = function newVersionFileUpload_filesUploaded (event)// @startlock
	{// @endlock
		var fileName = myNewVersionFile[0].name;
		var theParentDirectory = (currentDirectory == null)? null : currentDirectory;
		var versionData = {
				name: fileName,
				theLibrary: currentLibrary.ID,
				theParentDirectory: (currentDirectory == null)? null : currentDirectory.ID	
		}
		waf.ds.Versions.uploadVersion({
			onSuccess: function(event){		
				versionToSave = event.result;
				myNewVersionFile = null;
				$$(getHtmlId('button4')).disable();
				$$(getHtmlId('uploadVersionLoadImage')).hide();
				$(getHtmlObj('errorDiv13')).html(" New version was uploaded");
			},
			onError : function(error){
				alert(error.result.message);
			}
		},versionData);
	};// @lock

	okNotSavedDocsButton.click = function okNotSavedDocsButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('displayErrorsSaveDocumentsContainer')).hide();
		$(getHtmlObj('NotSavedDocumentsErrorDiv')).html("");
		if(metaDataArray.length == 0){
			$$(getHtmlId('metaDataContainer')).hide();
		}	
	};// @lock

	closeModifyDirectoryButton.click = function closeModifyDirectoryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newDirectoryContainer')).hide();
		directoryObject.name = '';
		directoryObject.description = '';
		waf.sources.directoryObject.sync();
	};// @lock

	saveModifyDirectoryButton.click = function saveModifyDirectoryButton_click (event)// @startlock
	{// @endlock
		waf.sources.directoryObject.sync();
		var theParentDirectory = (currentDirectory == null)? null : currentDirectory;
		
		var directoryData = {
			name: waf.sources.directoryObject.name,
			description: waf.sources.directoryObject.description,
			theLibrary: currentLibrary.ID,
			theParentDirectory: (currentDirectory == null)? null : currentDirectory.ID	
		};
		
		waf.ds.Directory.addDirectory({
			onSuccess: function(event){
				if(event.result == null){
					$$(getHtmlId('newDirectoryContainer')).hide();
					reloadContents();
					directoryObject.name = "";
					directoryObject.description = "";
					waf.sources.directoryObject.sync(); 
				} else {	
					var errors = event.result.message.split(',');
					$(getHtmlObj('nameDirectoryErrorDiv')).html(errors[0]);
					$(getHtmlObj('descriptionDirectoryErrorDiv')).html(errors[1]);
				}				
			},
			onError: function(error) {
				$$(getHtmlId('newDirectoryContainer')).hide();
				alert(error.result.message);
			}
		},directoryData);
	};// @lock

	cancelModifyDirectoryButton.click = function cancelModifyDirectoryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newDirectoryContainer')).hide();
		directoryObject.name = '';
		directoryObject.description = '';
		waf.sources.directoryObject.sync();
	};// @lock

	modifyLibraryButton.click = function modifyLibraryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('modifyLibraryContainer')).show();
		//vider les eventuelles précedents erreurs
		$(getHtmlObj('errorDiv8')).html("");
		$(getHtmlObj('errorDiv9')).html("");
		//
		currentLibrary = waf.sources[getHtmlId('library')].getCurrentElement();
		$$(getHtmlId('nameLibraryToModify')).setValue(currentLibrary.name.value);
		libraryObject.name = currentLibrary.name.value;
		libraryObject.description = currentLibrary.description.value;
		waf.sources.libraryObject.sync();
	};// @lock

	closeModifyLibraryButton.click = function closeModifyLibraryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('modifyLibraryContainer')).hide();
		libraryObject.name = "";
		libraryObject.description = "";
		waf.sources.libraryObject.sync();
	};// @lock

	saveModifyLibraryButton.click = function saveModifyLibraryButton_click (event)// @startlock
	{// @endlock
		var libraryData = {
			name: waf.sources.libraryObject.name,
			description: waf.sources.libraryObject.description	
		};
		currentLibrary.updateLibrary({
			onSuccess: function(event){
				if(event.result == null){
					$$(getHtmlId('modifyLibraryContainer')).hide();
					libraryObject.name = "";
					libraryObject.description = "";
					waf.sources.libraryObject.sync();
					//actualiser
					$$(getHtmlId('matrixLibraries')).hide();
					var libraries = currentSpace.getLibraries(); 
					waf.sources[getHtmlId('library')].setEntityCollection(libraries); 
					$$(getHtmlId('matrixLibraries')).show();
				} else {
					var errors = event.result.message.split(',');
					$(getHtmlObj('errorDiv8')).html(errors[0]);
					$(getHtmlObj('errorDiv9')).html(errors[1]);
				}
			},
			onError: function(error) {
				$$(getHtmlId('modifyLibraryContainer')).hide();
				alert(error.result.message);
			}
		},libraryData);	
	};// @lock

	cancelModifyLibraryButton.click = function cancelModifyLibraryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('modifyLibraryContainer')).hide();
		libraryObject.name = "";
		libraryObject.description = "";
		waf.sources.libraryObject.sync();
	};// @lock

	DeleteDocumentButton.click = function DeleteDocumentButton_click (event)// @startlock
	{// @endlock
		documentToDelete = waf.sources.documentsDirectories.getCurrentElement();
		$$(getHtmlId("deleteDocumentContainer")).show();
		$$(getHtmlId("nameDocumentToDelete")).setValue(documentToDelete.name);
	};// @lock

	closeDeleteDocumentButton.click = function closeDeleteDocumentButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("deleteDocumentContainer")).hide();
	};// @lock

	cancelDeleteDocumentButton.click = function cancelDeleteDocumentButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("deleteDocumentContainer")).hide();
		
	};// @lock

	confirmDeleteDocumentButton.click = function confirmDeleteDocumentButton_click (event)// @startlock
	{// @endlock
		waf.ds.Document.deleteDocument({
			onSuccess: function(event){
				$$(getHtmlId("deleteDocumentContainer")).hide();
				reloadContents();
			},
			onError: function(error) {
				
			}
		
		}, documentToDelete.ID);
		
	};// @lock

	deleteDirectoryButton.click = function deleteDirectoryButton_click (event)// @startlock
	{// @endlock
		directoryToDelete = waf.sources.documentsDirectories.getCurrentElement();
		$$(getHtmlId("deleteDirectoryContainer")).show();
		$$(getHtmlId("nameDirectoryToDelete")).setValue(directoryToDelete.name);
	};// @lock

	closeDeleteDirectoryButton.click = function closeDeleteDirectoryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("deleteDirectoryContainer")).hide();
	};// @lock

	cancelDeleteDirectoryButton.click = function cancelDeleteDirectoryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("deleteDirectoryContainer")).hide();
		
	};// @lock

	confirmDeleteDirectoryButton.click = function confirmDeleteDirectoryButton_click (event)// @startlock
	{// @endlock
		waf.ds.Directory.deleteDirectory({
			onSuccess: function(event){
				$$(getHtmlId("deleteDirectoryContainer")).hide();
				reloadContents();
			},
			onError: function(error) {
				
			}
		
		}, directoryToDelete.ID);
		
	};// @lock

	deleteLibraryButton.click = function deleteLibraryButton_click (event)// @startlock
	{// @endlock
		currentLibrary = waf.sources[getHtmlId('library')];
		$$(getHtmlId("nameLibraryToDelete")).setValue(currentLibrary.name);
		$$(getHtmlId("confirmDeleteLibraryContainer")).show();
	};// @lock

	imageButton9.click = function imageButton9_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("confirmDeleteLibraryContainer")).hide();
	};// @lock

	cancelDeleteLibraryButton.click = function cancelDeleteLibraryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("confirmDeleteLibraryContainer")).hide();
		
	};// @lock

	confirmDeleteLibraryButton.click = function confirmDeleteLibraryButton_click (event)// @startlock
	{// @endlock
		var dl = currentLibrary.deleteLibrary();
		if(dl != null){
			alert(dl.message);
		}
		$$(getHtmlId("confirmDeleteLibraryContainer")).hide();
		//actualiser
		$$(getHtmlId('matrixLibraries')).hide();
		var libraries = currentSpace.getLibraries(); 
		waf.sources[getHtmlId('library')].setEntityCollection(libraries); 
		$$(getHtmlId('matrixLibraries')).show();
	};// @lock

	matrixLibraries.onChildrenDraw = function matrixLibraries_onChildrenDraw (event)// @startlock
	{// @endlock
		//selecteur de tous les icons
		var iconsLibrary = getHtmlObj("matrixLibraries").find(".iconSelectLibraryClass");
		//affichage de l'icon du premier element
		$(iconsLibrary[1]).show();
		//permissions
		var SelectModifyButton = getHtmlObj("matrixLibraries").find(".modifyButtonClass");
		var SelectDeleteButton = getHtmlObj("matrixLibraries").find(".deleteButtonClass");
		$$(SelectModifyButton[1].id).disable();
		$$(SelectDeleteButton[1].id).disable();
		if((spacePermissions.collaboration == true)){
				$$(SelectModifyButton[1].id).enable();
				$$(SelectDeleteButton[1].id).enable();
		}
	};// @lock

	cancelSaveNewVersionButton.click = function cancelSaveNewVersionButton_click (event)// @startlock
	{// @endlock
		if(versionToSave == null){
			$$(getHtmlId('newVersionContainer')).hide();
		}else{
     		try {
     			var result = dashboardNS.cancelUploadVersion(versionToSave);
			}catch(e){
				alert("Error: " + e.message);
			}
			$$(getHtmlId('newVersionContainer')).hide();
			versionToSave = null;
			alert(result.message);
		}
	};// @lock

	actionsDirectoryContainer.mouseover = function actionsDirectoryContainer_mouseover (event)// @startlock
	{// @endlock
		// traitement specifique si le contenu est 1 seul element !!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(waf.sources.documentsDirectories.length == 1){
			var theSelectIcon = getHtmlObj("matrixDocuments").find(".actionsDirectoryContainerClass");
			$(theSelectIcon[1]).show();
		}else{
			var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".actionsDirectoryContainerClass").attr('id');
			$$(SelectActionContainer).show();
		}
	};// @lock

	actionsDirectoryContainer.mouseout = function actionsDirectoryContainer_mouseout (event)// @startlock
	{// @endlock
		// traitement specifique si le contenu est 1 seul element !!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(waf.sources.documentsDirectories.length == 1){
			var theSelectIcon = getHtmlObj("matrixDocuments").find(".actionsDirectoryContainerClass");
			$(theSelectIcon[1]).hide();
		}else{
			var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".actionsDirectoryContainerClass").attr('id');
			$$(SelectActionContainer).hide();
		}
	};// @lock

	cancelEditMetaData.click = function cancelEditMetaData_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('editMetaDataContainer')).hide();
		metaDataArray = [];
		waf.sources.metaDataArray.sync();
	};// @lock

	imageButton10.click = function imageButton10_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('editMetaDataContainer')).hide();
		metaDataArray = [];
		waf.sources.metaDataArray.sync();
	};// @lock

	saveMetaDataButton.click = function saveMetaDataButton_click (event)// @startlock
	{// @endlock
		var currentMetaData = waf.sources.metaDataArray.getCurrentElement();
		try{
			var result = dashboardNS.updateMetaData(currentMetaData);
		}catch(e){
			alert("Error: " + e.message);
		}
		var rsp = eval("("+result.response+")");
		if(rsp.responseHeader.status != 0)
			alert(rsp.error.msg);
// save on dataStore
		var documentData = {
			ID: currentMetaData.idDoc
		}
		waf.ds.Document.updateDocument({
			onSuccess : function(event){
				$$(getHtmlId('editMetaDataContainer')).hide();
				reloadContents();
				metaDataArray = [];
				waf.sources.metaDataArray.sync();
			},
			onError : function(error){
				alert(error.result);
			}
		}, documentData);
	};// @lock

	editMetaDataButton.click = function editMetaDataButton_click (event)// @startlock
	{// @endlock
		var currentDocument = waf.sources.documentsDirectories.getCurrentElement();
		try{
			var metaData = dashboardNS.getMetaData({ID: currentDocument.ID, version: currentDocument.version});
		}catch(e){
			alert("Error: " + e.message);
		}
		var rsp = eval("("+metaData+")");
		metaDataArray = [];
		metaDataArray.push({
			idDoc: waf.sources.documentsDirectories.ID,
			path: waf.sources.documentsDirectories.path,
			name:currentDocument.name,
			title:rsp.response.docs[0].title,
			author:rsp.response.docs[0].author,
			version:rsp.response.docs[0].version,
			description:rsp.response.docs[0].description,
			publisher:rsp.response.docs[0].publisher
//			nbPages:(rsp.response.docs[0].xmptpg_npages == undefined)? rsp.response.docs[0].page_count : rsp.response.docs[0].xmptpg_npages,
//			size:rsp.response.docs[0].stream_size
		});		
		$$(getHtmlId('editMetaDataContainer')).show();
		waf.sources.metaDataArray.sync();
		console.log(metaDataArray);
	};// @lock

	actionsLibraryContainer.mouseover = function actionsLibraryContainer_mouseover (event)// @startlock
	{// @endlock
		var SelectActionsLibraryContainer = $(getHtmlObj("matrixLibraries").find(".waf-state-selected")).find(".actionsLibraryContainerClass").attr('id');
		$$(SelectActionsLibraryContainer).show();
	};// @lock

	actionsLibraryContainer.mouseout = function actionsLibraryContainer_mouseout (event)// @startlock
	{// @endlock
		var actionsLibraryContainer = getHtmlObj('matrixLibraries').find('.actionsLibraryContainerClass');
		$(actionsLibraryContainer).hide();
	};// @lock

	iconSelectLibrary.mouseover = function iconSelectLibrary_mouseover (event)// @startlock
	{// @endlock
		var SelectActionsLibraryContainer = $(getHtmlObj("matrixLibraries").find(".waf-state-selected")).find(".actionsLibraryContainerClass").attr('id');
		$$(SelectActionsLibraryContainer).show();
	};// @lock

	iconSelectLibrary.click = function iconSelectLibrary_click (event)// @startlock
	{// @endlock
		var SelectActionsLibraryContainer = $(getHtmlObj("matrixLibraries").find(".waf-state-selected")).find(".actionsLibraryContainerClass").attr('id');
		$$(SelectActionsLibraryContainer).show();
	};// @lock

	skipButton.click = function skipButton_click (event)// @startlock
	{// @endlock
		// actualiser la page :
		$$(getHtmlId('metaDataContainer')).hide();
		reloadContents();
		metaDataArray = [];	
		waf.sources.metaDataArray.sync();
	};// @lock

	queryTextField.keyup = function queryTextField_keyup (event)// @startlock
	{// @endlock
		if ( event.keyCode == 13 ){
	   			search();
		}
	};// @lock

	queryTextField.focus = function queryTextField_focus (event)// @startlock
	{// @endlock
		$$(getHtmlId('searchButton')).show();
	};// @lock

	closeMetaDataContainerButton.click = function closeMetaDataContainerButton_click (event)// @startlock
	{// @endlock
	// actualiser la page :
		$$(getHtmlId('metaDataContainer')).hide();
		reloadContents();
		metaDataArray = [];
		waf.sources.metaDataArray.sync();
	};// @lock

	imageButton8.click = function imageButton8_click (event)// @startlock
	{// @endlock
		if(documentsToSave.length == 0){
			$$(getHtmlId('newDocumentContainer')).hide();
		}else{
     		try {
     			var result = dashboardNS.cancelUpload(documentsToSave);
			}catch(e){
				alert("Error: " + e.message);
			}
			$$(getHtmlId('newDocumentContainer')).hide();
			documentsToSave = [];
			alert(result.message);
		}
	};// @lock

	newDocumentButton.click = function newDocumentButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('chooseOptionCreateContainer')).show();
	};// @lock

	newDirectoryButton.click = function newDirectoryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newDirectoryContainer')).show();
		waf.sources.directoryObject.sync();
	};// @lock

	imageButton7.click = function imageButton7_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newDirectoryContainer')).hide();
		directoryObject.name = '';
		directoryObject.description = '';
		waf.sources.directoryObject.sync();
	};// @lock

	imageButton6.click = function imageButton6_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newLibraryContainer')).hide();
		libraryObject.name = "";
		libraryObject.description = "";
		waf.sources.libraryObject.sync(); 
	};// @lock

	imageButton4.click = function imageButton4_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('modifySpaceContainer')).hide();
		spaceObject.name = "";
		spaceObject.description = "";
		spaceObject.quota = "";
		$$(getHtmlId('modifyManagerCombobox')).setValue(null);
		waf.sources.spaceObject.sync();
	};// @lock

	imageButton1.click = function imageButton1_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newSpaceContainer')).hide();
		spaceObject.name = "";
		spaceObject.description = "";
		spaceObject.quota = "";
		$$(getHtmlId('managerCombobox')).setValue(null);
		waf.sources.spaceObject.sync();
	};// @lock

	imageButton5.click = function imageButton5_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("confirmDeleteContainer")).hide();
	};// @lock

	matrixSpaces.onChildrenDraw = function matrixSpaces_onChildrenDraw (event)// @startlock
	{// @endlock
		//selecteur de tous les icons
		var icons = getHtmlObj("matrixSpaces").find(".iconShow");
		//affichage de l'icon du premier element
		$(icons[1]).show();
		//permissions
		var SelectModifyButton = getHtmlObj("matrixSpaces").find(".modifyButtonClass");
		var SelectDeleteButton = getHtmlObj("matrixSpaces").find(".deleteButtonClass");
		$$(SelectModifyButton[1].id).disable();
		$$(SelectDeleteButton[1].id).disable();
		currentSpace = waf.sources[getHtmlId('space')];
		if(currentSpace != null){
			try{
				var isManager = dashboardNS.isSpaceManager(currentSpace.ID);
			}catch(e){
				alert("Error: " + e.message);
			}
			if((isManager == true) || isAdmin == true){
				$$(SelectModifyButton[1].id).enable();
				$$(SelectDeleteButton[1].id).enable();
			}
		}
	};// @lock

	buttonDownload.click = function buttonDownload_click (event)// @startlock
	{// @endlock
		var id = waf.sources.documentsDirectories.getCurrentElement().ID;

			var url = waf.config.baseURL.replace("waLib/WAF",'loadDocument?id='+id);
			window.open (url,id);
	};// @lock

	buttonView.click = function buttonView_click (event)// @startlock
	{// @endlock
		var id = waf.sources.documentsDirectories.getCurrentElement().ID;

			var url = waf.config.baseURL.replace("waLib/WAF",'readDoc?id='+id);
			window.open (url,id);
	};// @lock

	iconActionDoc.mouseover = function iconActionDoc_mouseover (event)// @startlock
	{// @endlock
		// traitement specifique si le contenu est 1 seul element !!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(waf.sources.documentsDirectories.length == 1){
			if(waf.sources.documentsDirectories.getCurrentElement().type === 'document'){
				var theSelectIcon = getHtmlObj("matrixDocuments").find(".containerActionDocClass");
				$(theSelectIcon[1]).show();
			}
			if(waf.sources.documentsDirectories.getCurrentElement().type === 'directory'){
				var theSelectIcon = getHtmlObj("matrixDocuments").find(".actionsDirectoryContainerClass");
				$(theSelectIcon[1]).show();
			}
		}else{
			if(waf.sources.documentsDirectories.getCurrentElement().type === 'document'){
				var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".containerActionDocClass").attr('id');
				$$(SelectActionContainer).show();
			}
			if(waf.sources.documentsDirectories.getCurrentElement().type === 'directory'){
				var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".actionsDirectoryContainerClass").attr('id');
				$$(SelectActionContainer).show();
			}
		}
	};// @lock

	iconActionDoc.click = function iconActionDoc_click (event)// @startlock
	{// @endlock
		if(waf.sources.documentsDirectories.getCurrentElement().type === 'document'){
			var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".containerActionDocClass").attr('id');
			$$(SelectActionContainer).show();
		}
		if(waf.sources.documentsDirectories.getCurrentElement().type === 'directory'){
			var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".actionsDirectoryContainerClass").attr('id');
			$$(SelectActionContainer).show();
		}
		
	};// @lock

	containerActionDoc.mouseover = function containerActionDoc_mouseover (event)// @startlock
	{// @endlock
		// traitement specifique si le contenu est 1 seul element !!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(waf.sources.documentsDirectories.length == 1){
			var theSelectIcon = getHtmlObj("matrixDocuments").find(".containerActionDocClass");
			$(theSelectIcon[1]).show();
		}else{
			var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".containerActionDocClass").attr('id');
			$$(SelectActionContainer).show();
		}
	};// @lock

	containerActionDoc.mouseout = function containerActionDoc_mouseout (event)// @startlock
	{// @endlock
		// traitement specifique si le contenu est 1 seul element !!!!!!!!!!!!!!!!!!!!!!!!!!!
		if(waf.sources.documentsDirectories.length == 1){
				var theSelectIcon = getHtmlObj("matrixDocuments").find(".containerActionDocClass");
				$(theSelectIcon[1]).hide();
		}else{ 
			var SelectActionContainer = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".containerActionDocClass").attr('id');
			$$(SelectActionContainer).hide();
		}
	};// @lock

	imageButton3.click = function imageButton3_click (event)// @startlock
	{// @endlock
//		waf.ds.User.getCurrentUser();
//		console.log(waf.ds.User.getCurrentUser());
	};// @lock

	cancelSearchButton.click = function cancelSearchButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('cancelSearchButton')).hide();
		$$(getHtmlId('resultSearchTextField')).hide();
		$$(getHtmlId('queryTextField')).setValue("");
		reloadContents();
	};// @lock

////// functions 
function search() {
	var params = {
			searchBy: $$(getHtmlId('searchByCombobox')).getValue(),
			value: $$(getHtmlId('queryTextField')).getValue()
		}
		try{
			var result = dashboardNS.search(params);
		}
		catch(e){
			alert("Error: " + e.message);
		}
		var rsp = eval("("+result+")");
		var foundDocs = rsp.response.docs;
		var allDocuments = [];
		if(currentDirectory != null){
			waf.ds.Directory.getAllDocuments({
				onSuccess : function(event){
					allDocuments = event.result;
					var docsArray = [];
					for(var i=0;i<foundDocs.length;i++){
						for(var j=0;j<allDocuments.length;j++){
  							if (allDocuments[j].ID == foundDocs[i].id) {
     							docsArray.push(allDocuments.splice(j,1)[0]);
     						}
     					}
    			 	}
					for (var i= 0; i<docsArray.length; i++)
						docsArray[i].type = "document";
					documentsDirectories = docsArray;
					waf.sources.documentsDirectories.sync();
					$$(getHtmlId('searchButton')).hide();
					$$(getHtmlId('cancelSearchButton')).show();
					$$(getHtmlId('resultSearchTextField')).show();
						
				},
				onError : function(error){
					alert(error.result.message);
				}
			},currentDirectory.ID);
		}else{
			allDocuments = currentLibrary.getAllDocuments();
			var docsArray = [];
			for(var i=0;i<foundDocs.length;i++){
				for(var j=0;j<allDocuments.length;j++){
  					if (allDocuments[j].ID == foundDocs[i].id) {
     					docsArray.push(allDocuments.splice(j,1)[0]);
     				}
     			}
    		}
    		for (var i= 0; i<docsArray.length; i++)
				docsArray[i].type = "document";
			documentsDirectories = docsArray;
			waf.sources.documentsDirectories.sync();
			$$(getHtmlId('searchButton')).hide();
			$$(getHtmlId('cancelSearchButton')).show();
			$$(getHtmlId('resultSearchTextField')).show();
		}
}

function reloadContents(){
	while(waf.sources.documentsDirectories.length>0){
		waf.sources.documentsDirectories.removeCurrent();
	}	
	if(currentDirectory != null){
		waf.ds.Directory.getContents({
			onSuccess: function(event){
				var contentsArray = event.result;
				for(var i=0;i<contentsArray.length;i++){
					documentsDirectories.push(contentsArray[i]);
				}
				waf.sources.documentsDirectories.sync();
			},
			onError: function(error) {
				
			}		
		},currentDirectory.ID);
		
	}else{
		var contentsArray = currentLibrary.displayContents();
		for(var i=0;i<contentsArray.length;i++){
			documentsDirectories.push(contentsArray[i]);
		}
	}
	waf.sources.documentsDirectories.sync();
}



	searchButton.click = function searchButton_click (event)// @startlock
	{// @endlock
		search();	
	};// @lock

	documentUpload.filesUploaded = function documentUpload_filesUploaded (event)// @startlock
	{// @endlock
		fileNamesArray = [];
		for (var i = 0; i < myFilesArray.length; i++) {
   			fileNamesArray.push(myFilesArray[i].name);
		}
		var theParentDirectory = (currentDirectory == null)? null : currentDirectory;
		var documentData = {
				names: fileNamesArray,
				theLibrary: currentLibrary.ID,
				theParentDirectory: (currentDirectory == null)? null : currentDirectory.ID	
		}
		waf.ds.Document.verifyExistence({
			onSuccess: function(event){
				var message = event.result.filesToSave.length + " files was uploaded ";
				if(event.result.existedFiles.length !=0){
						message += ", " + event.result.existedFiles.length + " files was not uploaded because : <br/>" ;
						for(var i=0;i<event.result.length;i++){						
							message += "  The document ( " + event.result[i] + " ) exists" + "<br/>";
						}
						myFilesArray = [];
				}
				documentsToSave = event.result.filesToSave;
				$(getHtmlObj('uploaderrorsDiv')).html(message);
			},
			onError : function(error){
				alert(error.result.message);
			}
		},documentData);
	};// @lock

	validateMetaDataButton.click = function validateMetaDataButton_click (event)// @startlock
	{// @endlock
		//image loading management 
		$$(getHtmlId('validateMetaDataButton')).hide();
		$$(getHtmlId('validateLoading')).show();	
		//traitement
		var currentMetaData = waf.sources.metaDataArray.getCurrentElement();
		try{
			var result = dashboardNS.updateMetaData(currentMetaData);
		}catch(e){
			alert("Error: " + e.message);
		}
		var rsp = eval("("+result.response+")");
		if(rsp.responseHeader.status != 0)
			alert(rsp.error.msg);
			
  				$$(getHtmlId('matrix1')).hide();
  				//image loading management 
				$$(getHtmlId('validateMetaDataButton')).show();
				$$(getHtmlId('validateLoading')).hide();	
				//
				for (var i =0; i < metaDataArray.length; i++)
  					if (metaDataArray[i].name === currentMetaData.name) {
     					metaDataArray.splice(i,1);
     					break;
  					}
  				waf.sources.metaDataArray.sync();
				$$(getHtmlId('matrix1')).show();
				if(metaDataArray.length == 0){
					$$(getHtmlId('metaDataContainer')).hide();
					reloadContents();
				}	
	};// @lock

	button2.click = function button2_click (event)// @startlock
	{// @endlock
		myFilesArray = [];
		myFilesArray = $$(getHtmlId('documentUpload')).getFiles();
		
	};// @lock

	saveDocumentButton.click = function saveDocumentButton_click (event)// @startlock
	{// @endlock

		if(documentsToSave.length == 0){
			$(getHtmlObj('uploadErrorDiv')).html(" You should upload Some files !");
		}else{
			//image loading 
			$$(getHtmlId('saveDocumentButton')).hide();
			$$(getHtmlId('loadingImage')).show();
			//traitement
			var theParentDirectory = (currentDirectory == null)? null : currentDirectory;
			var documentData = {
				docs: documentsToSave,
				theLibrary: currentLibrary.ID,
				theParentDirectory: (currentDirectory == null)? null : currentDirectory.ID	
			}
		
			waf.ds.Document.addDocument({
				onSuccess: function(event){
						$$(getHtmlId('newDocumentContainer')).hide();
						documentsToSave = [];
						$$(getHtmlId('metaDataContainer')).show();
						metaDataArray = [];
//						waf.sources.metaDataArray.sync();
						metaDataArray = event.result.metaData;	
						waf.sources.metaDataArray.sync();
						//image loading management 
						$$(getHtmlId('saveDocumentButton')).show();
						$$(getHtmlId('loadingImage')).hide();	
						if(event.result.error.length != 0){
							var message = "";
							for(var i=0;i<event.result.error.length;i++){						
								message += event.result.error[i] + "<br/>";
							}
							$$(getHtmlId('displayErrorsSaveDocumentsContainer')).show();
							$(getHtmlObj('NotSavedDocumentsErrorDiv')).html(message);		
						}			
				},
				onError: function(error) {
					$$(getHtmlId('newDocumentContainer')).hide();
					alert(error.result.message);
					//image loading management 
					$$(getHtmlId('saveDocumentButton')).show();
					$$(getHtmlId('loadingImage')).hide();	
				}
			},documentData);
			waf.sources.metaDataArray.sync();
		}
		
	};// @lock

	cancelDocumentButton.click = function cancelDocumentButton_click (event)// @startlock
	{// @endlock
		if(documentsToSave.length == 0){
			$$(getHtmlId('newDocumentContainer')).hide();
		}else{
     		try {
     			var result = dashboardNS.cancelUpload(documentsToSave);
			}catch(e){
				alert("Error: " + e.message);
			}
			$$(getHtmlId('newDocumentContainer')).hide();
			documentsToSave = [];
			alert(result.message);
		}
	};// @lock

	saveDirectoryButton.click = function saveDirectoryButton_click (event)// @startlock
	{// @endlock
		waf.sources.directoryObject.sync();
		var theParentDirectory = (currentDirectory == null)? null : currentDirectory;
		
		var directoryData = {
			name: waf.sources.directoryObject.name,
			description: waf.sources.directoryObject.description,
			theLibrary: currentLibrary.ID,
			theParentDirectory: (currentDirectory == null)? null : currentDirectory.ID	
		};
		
		waf.ds.Directory.addDirectory({
			onSuccess: function(event){
				if(event.result == null){
					$$(getHtmlId('newDirectoryContainer')).hide();
					reloadContents();
					directoryObject.name = "";
					directoryObject.description = "";
					waf.sources.directoryObject.sync(); 
				} else {	
					var errors = event.result.message.split(',');
					$(getHtmlObj('nameDirectoryErrorDiv')).html(errors[0]);
					$(getHtmlObj('descriptionDirectoryErrorDiv')).html(errors[1]);
				}				
			},
			onError: function(error) {
				$$(getHtmlId('newDirectoryContainer')).hide();
				alert(error.result.message);
			}
		},directoryData);
	};// @lock

	cancelDirectoryButton.click = function cancelDirectoryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newDirectoryContainer')).hide();
		directoryObject.name = '';
		directoryObject.description = '';
		waf.sources.directoryObject.sync();
	};// @lock
	
	documentContainer.click = function documentContainer_click (event)// @startlock
	{// @endlock
		//initial 
		var actionContainersDoc = getHtmlObj('matrixDocuments').find('.containerActionDocClass');
		$(actionContainersDoc).hide();
		
		var actionsDirectoryContainer = getHtmlObj('matrixDocuments').find('.actionsDirectoryContainerClass');
		$(actionsDirectoryContainer).hide();
		
		var iconsDoc = getHtmlObj('matrixDocuments').find('.iconDoc');
		$(iconsDoc).hide();
		//le container selectionné
		if(waf.sources.documentsDirectories.length == 1){
			var theSelectIcon = getHtmlObj("matrixDocuments").find(".iconDoc");
			$(theSelectIcon[1]).show();
		}else{
			var theSelectIcon = $(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".iconDoc").attr('id');
			$$(theSelectIcon).show();
		}
	//permissions
		var SelectModifyDirectoryButton = $($(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".actionsDirectoryContainerClass")).find(".modifyDirectoryButtonClass").attr('id');
		var SelectDeleteDirectoryButton = $($(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".actionsDirectoryContainerClass")).find(".deleteDirectoryButtonClass").attr('id');
		
		var SelectUploadVersionDocumentButton = $($(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".containerActionDocClass")).find(".uploadVersionDocumentButtonClass").attr('id');
		var SelectEditDocumentButton = $($(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".containerActionDocClass")).find(".editDocumentButtonClass").attr('id');
		var SelectDeleteDocumentButton = $($(getHtmlObj("matrixDocuments").find(".waf-state-selected")).find(".containerActionDocClass")).find(".deleteDocumentButtonClass").attr('id');
		
		$$(SelectModifyDirectoryButton).disable();
		$$(SelectDeleteDirectoryButton).disable();
		
		$$(SelectUploadVersionDocumentButton).disable();
		$$(SelectEditDocumentButton).disable();
		$$(SelectDeleteDocumentButton).disable();
		
		if((libraryPermissions.collaboration == true)){
			$$(SelectModifyDirectoryButton).enable();
			$$(SelectDeleteDirectoryButton).enable();
			$$(SelectEditDocumentButton).enable();
			$$(SelectDeleteDocumentButton).enable();
		}
		if((libraryPermissions.contribution == true)){
			$$(SelectUploadVersionDocumentButton).enable();
		}
	};// @lock

	documentContainer.dblclick = function documentContainer_dblclick (event)// @startlock
	{// @endlock

		if(waf.sources.documentsDirectories.getCurrentElement().type == "directory"){
			currentDirectory = waf.sources.documentsDirectories.getCurrentElement();
			var id = currentDirectory.ID;
			pileTreeDirectories.push(currentDirectory);
			waf.ds.Directory.getContents({
				onSuccess: function(event){
				//permissions
					try{
						directoryPermissions = dashboardNS.checkDirectoryPermissions(currentLibrary.ID);
					}catch(e){
						alert("Error: " + e.message);
					}
					if((directoryPermissions.contribution == true) || (directoryPermissions.collaboration == true)){
						$$(getHtmlId('newDirectoryButton')).enable(); 
						$$(getHtmlId('newDocumentButton')).enable();
					}
				//display contents
					while(waf.sources.documentsDirectories.length>0){
						waf.sources.documentsDirectories.removeCurrent();
					}
					var contentsArray = event.result;
					for(var i=0;i<contentsArray.length;i++){
						documentsDirectories.push(contentsArray[i]);
					}
					waf.sources.documentsDirectories.sync();
					waf.sources.documentsDirectories.autoDispatch();
					// if la recherche est activee 
					$$(getHtmlId('cancelSearchButton')).hide();
					$$(getHtmlId('resultSearchTextField')).hide();
					$$(getHtmlId('queryTextField')).setValue("");
					// navigation bar manager
					var oldBar = $$(getHtmlId('navigationBar2')).getValue();
					$$(getHtmlId('navigationBar2')).setValue(oldBar + ' / ' + currentDirectory.name);
				},
				onError: function(error) {
				
				}
			},id);
		
		}else{
			var id = waf.sources.documentsDirectories.getCurrentElement().ID;
			var url = waf.config.baseURL.replace("waLib/WAF",'readDoc?id='+id);
			window.open (url,id);
		}	
	};// @lock

	matrixDocuments.onChildrenDraw = function matrixDocuments_onChildrenDraw (event)// @startlock
	{// @endlock
		var iconsDoc = getHtmlObj('matrixDocuments').find('.iconDoc');
		$(iconsDoc[1]).show();
		var elmt = $(event.htmlObject);
		elmt.find('.documentCheck').hide();
		elmt.find('.folderCheck').hide();
		elmt.find('.iconDoc').hide();
		
		if(event.source.type == "document"){
			elmt.find('.documentCheck').show();
			elmt.find('.versionClass').show();
			var SelectUploadVersionDocumentButton = getHtmlObj("matrixDocuments").find(".uploadVersionDocumentButtonClass");
			var SelectEditDocumentButton = getHtmlObj("matrixDocuments").find(".editDocumentButtonClass");
			var SelectDeleteDocumentButton = getHtmlObj("matrixDocuments").find(".deleteDocumentButtonClass");
			$$(SelectUploadVersionDocumentButton[1].id).disable();
			$$(SelectEditDocumentButton[1].id).disable();
			$$(SelectDeleteDocumentButton[1].id).disable();
			if((libraryPermissions.collaboration == true)){
				$$(SelectEditDocumentButton[1].id).enable();
				$$(SelectDeleteDocumentButton[1].id).enable();
			}
			if((libraryPermissions.contribution == true)){
				$$(SelectUploadVersionDocumentButton[1].id).enable();
			}
		}
		if(event.source.type == "directory"){
			elmt.find('.folderCheck').show();
			elmt.find('.versionClass').hide();
			//permissions
			var SelectModifyDirectoryButton = getHtmlObj("matrixDocuments").find(".modifyDirectoryButtonClass");
			var SelectDeleteDirectoryButton = getHtmlObj("matrixDocuments").find(".deleteDirectoryButtonClass");
			$$(SelectModifyDirectoryButton[1].id).disable();
			$$(SelectDeleteDirectoryButton[1].id).disable();
			if((libraryPermissions.collaboration == true)){
				$$(SelectModifyDirectoryButton[1].id).enable();
				$$(SelectDeleteDirectoryButton[1].id).enable();
			}
		}
	};// @lock
	

	librariesContainer.click = function librariesContainer_click (event)// @startlock
	{// @endlock
		var actionslibraryContainer = getHtmlObj('matrixLibraries').find('.actionsLibraryContainerClass');
		$(actionslibraryContainer).hide();
		
		var icons = getHtmlObj('matrixLibraries').find('.iconSelectLibraryClass');
		$(icons).hide();
		
		var theSelectIcon = $(getHtmlObj("matrixLibraries").find(".waf-state-selected")).find(".iconSelectLibraryClass").attr('id');
		$$(theSelectIcon).show();
		
		//permissions
		var SelectModifyButton = $($(getHtmlObj("matrixLibraries").find(".waf-state-selected")).find(".actionsLibraryContainerClass")).find(".modifyButtonClass").attr('id');
		var SelectDeleteButton = $($(getHtmlObj("matrixLibraries").find(".waf-state-selected")).find(".actionsLibraryContainerClass")).find(".deleteButtonClass").attr('id');
		$$(SelectModifyButton).disable();
		$$(SelectDeleteButton).disable();
//		console.log(SelectModifyButton);
		if((spacePermissions.collaboration == true)){
			$$(SelectModifyButton).enable();
			$$(SelectDeleteButton).enable();
		}	
	};// @lock

	librariesContainer.dblclick = function librariesContainer_dblclick (event)// @startlock
	{// @endlock
		currentLibrary = waf.sources[getHtmlId('library')];
		$$(getHtmlId('librariesBody')).hide(); 
		$$(getHtmlId('documentsBody')).show();
		//init 
		$$(getHtmlId('newDirectoryButton')).disable(); 
		$$(getHtmlId('newDocumentButton')).disable();
		//permissions
		try{
			libraryPermissions = dashboardNS.checkLibraryPermissions(currentLibrary.ID);
		}catch(e){
			alert("Error: " + e.message);
		}
		
		if((libraryPermissions.contribution == true) || (libraryPermissions.collaboration == true)){
			$$(getHtmlId('newDirectoryButton')).enable(); 
			$$(getHtmlId('newDocumentButton')).enable();
		}
		//display contents
		while(waf.sources.documentsDirectories.length>0){
			waf.sources.documentsDirectories.removeCurrent();
		}
		documentsDirectories = [];
		var contentsArray = currentLibrary.displayContents();
		for(var i=0;i<contentsArray.length;i++){
			documentsDirectories.push(contentsArray[i]);
		}
		waf.sources.documentsDirectories.sync();
		waf.sources.documentsDirectories.autoDispatch();
		 
		// navigation bar manager
		var oldBar = $$(getHtmlId('navigationBar')).getValue();
		$$(getHtmlId('navigationBar2')).setValue(oldBar + ' / ' + currentLibrary.name);		
	};// @lock

	backToLibraryButton.click = function backToLibraryButton_click (event)// @startlock
	{// @endlock
		if(pileTreeDirectories.length == 0){
			$$(getHtmlId('librariesBody')).show(); 
			$$(getHtmlId('documentsBody')).hide();
			currentDirectory = null;
			pileTreeDirectories =[];
			
		}else{
			if(pileTreeDirectories.length == 1){
				while(waf.sources.documentsDirectories.length>0){
					waf.sources.documentsDirectories.removeCurrent();
				}
				var contentsArray = currentLibrary.displayContents();
				for(var i=0;i<contentsArray.length;i++){
					documentsDirectories.push(contentsArray[i]);
				}
				waf.sources.documentsDirectories.sync();
				currentDirectory = null;
				pileTreeDirectories =[];
				
			}else{
				pileTreeDirectories.pop();
				currentDirectory = pileTreeDirectories[pileTreeDirectories.length - 1];
				var id = currentDirectory.ID;
				waf.ds.Directory.getContents({
					onSuccess: function(event){
						while(waf.sources.documentsDirectories.length>0){
							waf.sources.documentsDirectories.removeCurrent();
						}
						var contentsArray = event.result;
						for(var i=0;i<contentsArray.length;i++){
							documentsDirectories.push(contentsArray[i]);
						}
						waf.sources.documentsDirectories.sync();			
								
					},
					onError: function(error) {
				
					}
				},id);
			}
		}
		// navigation bar manager
		var oldBar = $$(getHtmlId('navigationBar2')).getValue();
		var oldBar2 = oldBar.split('/');
		oldBar2.pop();
		var newBar = oldBar2.join('/');
		$$(getHtmlId('navigationBar2')).setValue(newBar);
		// if la recherche est activee 
		$$(getHtmlId('cancelSearchButton')).hide();
		$$(getHtmlId('resultSearchTextField')).hide();
		$$(getHtmlId('queryTextField')).setValue("");
	};// @lock

	SaveLibraryButton.click = function SaveLibraryButton_click (event)// @startlock
	{// @endlock
		currentSpace = waf.sources[getHtmlId('space')];
		var libraryData = {
			name: waf.sources.libraryObject.name,
			description: waf.sources.libraryObject.description,
			theSpace: currentSpace.ID	
		};
		
		waf.ds.Library.addLibrary({
			onSuccess: function(event){
				if(event.result == null){
					$$(getHtmlId('newLibraryContainer')).hide();
					$$(getHtmlId('matrixLibraries')).hide();
					var libraries = currentSpace.getLibraries(); 
					waf.sources[getHtmlId('library')].setEntityCollection(libraries); 
					$$(getHtmlId('matrixLibraries')).show();
					libraryObject.name = "";
					libraryObject.description = "";
					waf.sources.libraryObject.sync(); 
				} else {	
					var errors = event.result.message.split(',');
					$(getHtmlObj('errorDiv4')).html(errors[0]);
					$(getHtmlObj('errorDiv10')).html(errors[1]);
				}				
			},
			onError: function(error) {
				$$(getHtmlId('newLibrayContainer')).hide();
				alert(error.result.message);
			}
		},libraryData);
	};// @lock

	cancelCreateLibraryButton.click = function cancelCreateLibraryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newLibraryContainer')).hide();
		libraryObject.name = "";
		libraryObject.description = "";
		waf.sources.libraryObject.sync(); 
	};// @lock

	backButton.click = function backButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('spacesBody')).show(); 
		$$(getHtmlId('librariesBody')).hide(); 
	};// @lock

	addLibraryButton.click = function addLibraryButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newLibraryContainer')).show();
		$$(getHtmlId('libraryName')).focus();
		waf.sources.libraryObject.sync();
	};// @lock

	confirmDeleteButton.click = function confirmDeleteButton_click (event)// @startlock
	{// @endlock
		var dl = currentSpace.deleteSpace();
		if(dl != null){
			alert(dl.message);
		}
		$$(getHtmlId("confirmDeleteContainer")).hide();
		$$('bodyComponent').loadComponent({path: '/Components/spaces.waComponent'});
	};// @lock

	cancelDeleteButton.click = function cancelDeleteButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("confirmDeleteContainer")).hide();
		
	};// @lock

	deleteButton.click = function deleteButton_click (event)// @startlock
	{// @endlock
		currentSpace = waf.sources[getHtmlId('space')];
		$$(getHtmlId("nameSpaceToDelete")).setValue(currentSpace.name);
		$$(getHtmlId("confirmDeleteContainer")).show();
	};// @lock
	

	modifySaveButton.click = function modifySaveButton_click (event)// @startlock
	{// @endlock
		var spaceData = {
			name: waf.sources.spaceObject.name,
			description: waf.sources.spaceObject.description,
//			manager: waf.sources.spaceObject.manager,
			manager: $$(getHtmlId('modifyManagerCombobox')).getValue(),
			quota: waf.sources.spaceObject.quota		
		};
		currentSpace.updateSpace({
			onSuccess: function(event){
				if(event.result == null){
					$$(getHtmlId('modifySpaceContainer')).hide();
					spaceObject.name = "";
					spaceObject.description = "";
					spaceObject.quota = "";
					$$(getHtmlId('modifyManagerCombobox')).setValue(null);
					waf.sources.spaceObject.sync();
					$$('bodyComponent').loadComponent({path: '/Components/spaces.waComponent'});
					
				} else {
					var errors = event.result.message.split(',');
					$(getHtmlObj('errorDiv2')).html(errors[0]);
					$(getHtmlObj('errorDiv5')).html(errors[1]);
					$(getHtmlObj('errorDiv6')).html(errors[2]);
					$(getHtmlObj('errorDiv7')).html(errors[3]);
				}
			},
			onError: function(error) {
				$$(getHtmlId('modifySpaceContainer')).hide();
				alert(error.result.message);
			}
		},spaceData);	
	};// @lock

	modifyCancelButton.click = function modifyCancelButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('modifySpaceContainer')).hide();
		spaceObject.name = "";
		spaceObject.description = "";
		spaceObject.quota = "";
		$$(getHtmlId('modifyManagerCombobox')).setValue(null);
		waf.sources.spaceObject.sync();
	};// @lock

	modifyButton.click = function modifyButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('modifySpaceContainer')).show();
		//vider les eventuelles précedents erreurs
		$(getHtmlObj('errorDiv2')).html("");
		$(getHtmlObj('errorDiv5')).html("");
		$(getHtmlObj('errorDiv6')).html("");
		$(getHtmlObj('errorDiv7')).html("");
		//
		currentSpace = waf.sources[getHtmlId('space')].getCurrentElement();
		$$(getHtmlId('nameSpaceToModify')).setValue(currentSpace.name.value);
		spaceObject.name = currentSpace.name.value;
		spaceObject.description = currentSpace.description.value;
		spaceObject.quota = currentSpace.quotaSpace.value;
		$$(getHtmlId('modifyManagerCombobox')).setValue(currentSpace.manager.value.ID);
		waf.sources.spaceObject.sync();
	};// @lock

	save.click = function save_click (event)// @startlock
	{// @endlock
		waf.sources.spaceObject.sync();
		var spaceData = {
			name: waf.sources.spaceObject.name,
			description: waf.sources.spaceObject.description,
//			manager: waf.sources.spaceObject.manager,
			manager: $$(getHtmlId('managerCombobox')).getValue(),
			quota: waf.sources.spaceObject.quota		
		};
		waf.ds.Space.addSpace({
			onSuccess: function(event){
				if(event.result == null){
					$$(getHtmlId('newSpaceContainer')).hide();
					spaceObject.name="";
					spaceObject.description="";
//					spaceObject.manager=null;
					$$(getHtmlId('managerCombobox')).setValue(null);
					spaceObject.quota="";
					waf.sources.spaceObject.sync();
					$$('bodyComponent').loadComponent({path: '/Components/spaces.waComponent'});
 					waf.sources[getHtmlId('space')].collectionRefresh();
				} else {	
					var errors = event.result.message.split(',');
					if(errors[4] != undefined) alert(errors[4]);
					$(getHtmlObj('errorDivName')).html(errors[0]);
					$(getHtmlObj('errorDivDescription')).html(errors[1]);
					$(getHtmlObj('errorDivManager')).html(errors[2]);
					$(getHtmlObj('errorDivQuota')).html(errors[3]);
				}				
			},
			onError: function(error) {
				$$(getHtmlId('newSpaceContainer')).hide();
				alert(error.result.message);
			}
		},spaceData);
	};// @lock

	newSpaceButton.click = function newSpaceButton_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newSpaceContainer')).show();
		//vider les eventuelles précedents erreurs
		$(getHtmlObj('errorDivName')).html("");
		$(getHtmlObj('errorDivManager')).html("");
		$(getHtmlObj('errorDivDescription')).html("");
		$(getHtmlObj('errorDivQuota')).html("");
		$$(getHtmlId('nameTextField')).focus();
		waf.sources.spaceObject.sync();
	};// @lock

	cancel.click = function cancel_click (event)// @startlock
	{// @endlock
		$$(getHtmlId('newSpaceContainer')).hide();
		spaceObject.name = "";
		spaceObject.description = "";
		spaceObject.quota = "";
		$$(getHtmlId('managerCombobox')).setValue(null);
		waf.sources.spaceObject.sync();
	};// @lock

	actionsContainer.mouseout = function actionsContainer_mouseout (event)// @startlock
	{// @endlock
		var actionContainers = getHtmlObj('matrixSpaces').find('.actionsContainerClass');
		$(actionContainers).hide();
	};// @lock

	actionsContainer.mouseover = function actionsContainer_mouseover (event)// @startlock
	{// @endlock
		var SelectActionContainer = $(getHtmlObj("matrixSpaces").find(".waf-state-selected")).find(".actionsContainerClass").attr('id');
		$$(SelectActionContainer).show();
	};// @lock

	iconActions.mouseover = function iconActions_mouseover (event)// @startlock
	{// @endlock
		var SelectActionContainer = $(getHtmlObj("matrixSpaces").find(".waf-state-selected")).find(".actionsContainerClass").attr('id');
		$$(SelectActionContainer).show();
	};// @lock

	iconActions.click = function iconActions_click (event)// @startlock
	{// @endlock
		var SelectActionContainer = $(getHtmlObj("matrixSpaces").find(".waf-state-selected")).find(".actionsContainerClass").attr('id');
		$$(SelectActionContainer).show();
	};// @lock
		
		
	spaceContainer.dblclick = function spaceContainer_dblclick (event)// @startlock
	{// @endlock
		currentSpace = waf.sources[getHtmlId('space')];
		//init
		$$(getHtmlId('addLibraryButton')).disable();
		//permissions
		try{
			spacePermissions = dashboardNS.checkSpacePermissions(currentSpace.ID);
		}catch(e){
			alert("Error: " + e.message);
		}
		if((spacePermissions.contribution == true) || (spacePermissions.collaboration == true)){
			$$(getHtmlId('addLibraryButton')).enable();
		}
		//traitement
		var libraries = currentSpace.getLibraries();
		$$(getHtmlId('spacesBody')).hide(); 
		$$(getHtmlId('librariesBody')).show(); 
		waf.sources[getHtmlId('library')].setEntityCollection(libraries); 
		// navigation bar manager
		var racine = $$(getHtmlId('navigationRacine')).getValue();
		$$(getHtmlId('navigationBar')).setValue(racine + currentSpace.name);		
	};// @lock

	spaceContainer.click = function spaceContainer_click (event)// @startlock
	{// @endlock
		var actionContainers = getHtmlObj('matrixSpaces').find('.actionsContainerClass');
		$(actionContainers).hide()
		
		var icons = getHtmlObj('matrixSpaces').find('.iconShow');
		$(icons).hide();
		
		var theSelectIcon = $(getHtmlObj("matrixSpaces").find(".waf-state-selected")).find(".iconShow").attr('id');
		$$(theSelectIcon).show();
		//permissions
		var SelectModifyButton = $($(getHtmlObj("matrixSpaces").find(".waf-state-selected")).find(".actionsContainerClass")).find(".modifyButtonClass").attr('id');
		var SelectDeleteButton = $($(getHtmlObj("matrixSpaces").find(".waf-state-selected")).find(".actionsContainerClass")).find(".deleteButtonClass").attr('id');
		$$(SelectModifyButton).disable();
		$$(SelectDeleteButton).disable();
//		console.log(SelectModifyButton);
		currentSpace = waf.sources[getHtmlId('space')];
		try{
			var isManager = dashboardNS.isSpaceManager(currentSpace.ID);
		}catch(e){
			alert("Error: " + e.message);
		}
		if((isManager == true) || isAdmin == true){
			$$(SelectModifyButton).enable();
			$$(SelectDeleteButton).enable();
		}	
	};// @lock
	

	// @region eventManager// @startlock
	WAF.addListener(this.id + "_cancelSaveDocumentFromGoogleButton", "click", cancelSaveDocumentFromGoogleButton.click, "WAF");
	WAF.addListener(this.id + "_saveDocumentFromGoogleButton", "click", saveDocumentFromGoogleButton.click, "WAF");
	WAF.addListener(this.id + "_optionCreateRadioGroup", "change", optionCreateRadioGroup.change, "WAF");
	WAF.addListener(this.id + "_closeChooseOptionCreateButton", "click", closeChooseOptionCreateButton.click, "WAF");
	WAF.addListener(this.id + "_cancelChooseOptionCreateButton", "click", cancelChooseOptionCreateButton.click, "WAF");
	WAF.addListener(this.id + "_createDocumentButton", "click", createDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_okErrorVersionButton", "click", okErrorVersionButton.click, "WAF");
	WAF.addListener(this.id + "_downloadVersionButton", "click", downloadVersionButton.click, "WAF");
	WAF.addListener(this.id + "_viewVersionButton", "click", viewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_skipMetaDataNewVersionButton", "click", skipMetaDataNewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_closeMetaDataNewVersionButton", "click", closeMetaDataNewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_validateMetaDataNewVersionButton", "click", validateMetaDataNewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_saveRevertButton", "click", saveRevertButton.click, "WAF");
	WAF.addListener(this.id + "_closeDeleteVersionButton", "click", closeDeleteVersionButton.click, "WAF");
	WAF.addListener(this.id + "_deleteVersionButton", "click", deleteVersionButton.click, "WAF");
	WAF.addListener(this.id + "_cancelDeleteVersionButton", "click", cancelDeleteVersionButton.click, "WAF");
	WAF.addListener(this.id + "_confirmDeleteVersionButton", "click", confirmDeleteVersionButton.click, "WAF");
	WAF.addListener(this.id + "_closeRevertButton", "click", closeRevertButton.click, "WAF");
	WAF.addListener(this.id + "_cancelRevertButton", "click", cancelRevertButton.click, "WAF");
	WAF.addListener(this.id + "_revertToVersionButton", "click", revertToVersionButton.click, "WAF");
	WAF.addListener(this.id + "_imageButton11", "click", imageButton11.click, "WAF");
	WAF.addListener(this.id + "_displayVersionsButton", "click", displayVersionsButton.click, "WAF");
	WAF.addListener(this.id + "_uploadNewVersionDocButton", "click", uploadNewVersionDocButton.click, "WAF");
	WAF.addListener(this.id + "_uploadNewVersionButton", "click", uploadNewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_closeNewVersionButton", "click", closeNewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_saveNewVersionButton", "click", saveNewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_newVersionFileUpload", "filesUploaded", newVersionFileUpload.filesUploaded, "WAF");
	WAF.addListener(this.id + "_okNotSavedDocsButton", "click", okNotSavedDocsButton.click, "WAF");
	WAF.addListener(this.id + "_closeModifyDirectoryButton", "click", closeModifyDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_saveModifyDirectoryButton", "click", saveModifyDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_cancelModifyDirectoryButton", "click", cancelModifyDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_modifyLibraryButton", "click", modifyLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_closeModifyLibraryButton", "click", closeModifyLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_saveModifyLibraryButton", "click", saveModifyLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_cancelModifyLibraryButton", "click", cancelModifyLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_DeleteDocumentButton", "click", DeleteDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_closeDeleteDocumentButton", "click", closeDeleteDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_cancelDeleteDocumentButton", "click", cancelDeleteDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_confirmDeleteDocumentButton", "click", confirmDeleteDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_deleteDirectoryButton", "click", deleteDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_closeDeleteDirectoryButton", "click", closeDeleteDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_cancelDeleteDirectoryButton", "click", cancelDeleteDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_confirmDeleteDirectoryButton", "click", confirmDeleteDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_deleteLibraryButton", "click", deleteLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_imageButton9", "click", imageButton9.click, "WAF");
	WAF.addListener(this.id + "_cancelDeleteLibraryButton", "click", cancelDeleteLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_confirmDeleteLibraryButton", "click", confirmDeleteLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_saveMetaDataButton", "click", saveMetaDataButton.click, "WAF");
	WAF.addListener(this.id + "_matrixLibraries", "onChildrenDraw", matrixLibraries.onChildrenDraw, "WAF");
	WAF.addListener(this.id + "_actionsDirectoryContainer", "mouseout", actionsDirectoryContainer.mouseout, "WAF");
	WAF.addListener(this.id + "_actionsDirectoryContainer", "mouseover", actionsDirectoryContainer.mouseover, "WAF");
	WAF.addListener(this.id + "_cancelSaveNewVersionButton", "click", cancelSaveNewVersionButton.click, "WAF");
	WAF.addListener(this.id + "_cancelEditMetaData", "click", cancelEditMetaData.click, "WAF");
	WAF.addListener(this.id + "_imageButton10", "click", imageButton10.click, "WAF");
	WAF.addListener(this.id + "_editMetaDataButton", "click", editMetaDataButton.click, "WAF");
	WAF.addListener(this.id + "_queryTextField", "keyup", queryTextField.keyup, "WAF");
	WAF.addListener(this.id + "_librariesContainer", "click", librariesContainer.click, "WAF");
	WAF.addListener(this.id + "_actionsLibraryContainer", "mouseover", actionsLibraryContainer.mouseover, "WAF");
	WAF.addListener(this.id + "_actionsLibraryContainer", "mouseout", actionsLibraryContainer.mouseout, "WAF");
	WAF.addListener(this.id + "_iconSelectLibrary", "mouseover", iconSelectLibrary.mouseover, "WAF");
	WAF.addListener(this.id + "_iconSelectLibrary", "click", iconSelectLibrary.click, "WAF");
	WAF.addListener(this.id + "_skipButton", "click", skipButton.click, "WAF");
	WAF.addListener(this.id + "_queryTextField", "focus", queryTextField.focus, "WAF");
	WAF.addListener(this.id + "_closeMetaDataContainerButton", "click", closeMetaDataContainerButton.click, "WAF");
	WAF.addListener(this.id + "_imageButton8", "click", imageButton8.click, "WAF");
	WAF.addListener(this.id + "_newDocumentButton", "click", newDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_newDirectoryButton", "click", newDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_imageButton7", "click", imageButton7.click, "WAF");
	WAF.addListener(this.id + "_imageButton6", "click", imageButton6.click, "WAF");
	WAF.addListener(this.id + "_imageButton4", "click", imageButton4.click, "WAF");
	WAF.addListener(this.id + "_imageButton1", "click", imageButton1.click, "WAF");
	WAF.addListener(this.id + "_imageButton5", "click", imageButton5.click, "WAF");
	WAF.addListener(this.id + "_matrixSpaces", "onChildrenDraw", matrixSpaces.onChildrenDraw, "WAF");
	WAF.addListener(this.id + "_buttonDownload", "click", buttonDownload.click, "WAF");
	WAF.addListener(this.id + "_buttonView", "click", buttonView.click, "WAF");
	WAF.addListener(this.id + "_iconActionDoc", "mouseover", iconActionDoc.mouseover, "WAF");
	WAF.addListener(this.id + "_iconActionDoc", "click", iconActionDoc.click, "WAF");
	WAF.addListener(this.id + "_containerActionDoc", "mouseover", containerActionDoc.mouseover, "WAF");
	WAF.addListener(this.id + "_containerActionDoc", "mouseout", containerActionDoc.mouseout, "WAF");
	WAF.addListener(this.id + "_imageButton3", "click", imageButton3.click, "WAF");
	WAF.addListener(this.id + "_documentContainer", "click", documentContainer.click, "WAF");
	WAF.addListener(this.id + "_cancelSearchButton", "click", cancelSearchButton.click, "WAF");
	WAF.addListener(this.id + "_searchButton", "click", searchButton.click, "WAF");
	WAF.addListener(this.id + "_documentUpload", "filesUploaded", documentUpload.filesUploaded, "WAF");
	WAF.addListener(this.id + "_validateMetaDataButton", "click", validateMetaDataButton.click, "WAF");
	WAF.addListener(this.id + "_button2", "click", button2.click, "WAF");
	WAF.addListener(this.id + "_saveDocumentButton", "click", saveDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_cancelDocumentButton", "click", cancelDocumentButton.click, "WAF");
	WAF.addListener(this.id + "_saveDirectoryButton", "click", saveDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_cancelDirectoryButton", "click", cancelDirectoryButton.click, "WAF");
	WAF.addListener(this.id + "_documentContainer", "dblclick", documentContainer.dblclick, "WAF");
	WAF.addListener(this.id + "_matrixDocuments", "onChildrenDraw", matrixDocuments.onChildrenDraw, "WAF");
	WAF.addListener(this.id + "_librariesContainer", "dblclick", librariesContainer.dblclick, "WAF");
	WAF.addListener(this.id + "_backToLibraryButton", "click", backToLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_SaveLibraryButton", "click", SaveLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_cancelCreateLibraryButton", "click", cancelCreateLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_backButton", "click", backButton.click, "WAF");
	WAF.addListener(this.id + "_addLibraryButton", "click", addLibraryButton.click, "WAF");
	WAF.addListener(this.id + "_spaceContainer", "dblclick", spaceContainer.dblclick, "WAF");
	WAF.addListener(this.id + "_confirmDeleteButton", "click", confirmDeleteButton.click, "WAF");
	WAF.addListener(this.id + "_cancelDeleteButton", "click", cancelDeleteButton.click, "WAF");
	WAF.addListener(this.id + "_deleteButton", "click", deleteButton.click, "WAF");
	WAF.addListener(this.id + "_modifySaveButton", "click", modifySaveButton.click, "WAF");
	WAF.addListener(this.id + "_modifyCancelButton", "click", modifyCancelButton.click, "WAF");
	WAF.addListener(this.id + "_modifyButton", "click", modifyButton.click, "WAF");
	WAF.addListener(this.id + "_save", "click", save.click, "WAF");
	WAF.addListener(this.id + "_newSpaceButton", "click", newSpaceButton.click, "WAF");
	WAF.addListener(this.id + "_cancel", "click", cancel.click, "WAF");
	WAF.addListener(this.id + "_actionsContainer", "mouseout", actionsContainer.mouseout, "WAF");
	WAF.addListener(this.id + "_actionsContainer", "mouseover", actionsContainer.mouseover, "WAF");
	WAF.addListener(this.id + "_iconActions", "mouseover", iconActions.mouseover, "WAF");
	WAF.addListener(this.id + "_iconActions", "click", iconActions.click, "WAF");
	WAF.addListener(this.id + "_spaceContainer", "click", spaceContainer.click, "WAF");
	// @endregion// @endlock

	};// @lock

}// @startlock
return constructor;
})();// @endlock
