﻿
(function Component (id) {// @lock

// Add the code that needs to be shared between components here

function constructor (id) {

	// @region beginComponentDeclaration// @startlock
	var $comp = this;
	this.name = 'overview';
	// @endregion// @endlock

	this.load = function (data) {// @lock
		
				

	// @region namespaceDeclaration// @startlock
	var docRichText = {};	// @richText
	var saveButton = {};	// @button
	var imageButtonCloAcc = {};	// @buttonImage
	var directoryEvent = {};	// @dataSource
	var spaceEvent = {};	// @dataSource
	var documentEvent = {};	// @dataSource
	var imageButton3 = {};	// @buttonImage
	var imageButton4 = {};	// @buttonImage
	var documentsMatrix = {};	// @matrix
	var directoriesMatrix = {};	// @matrix
	var spacesMatrix = {};	// @matrix
	// @endregion// @endlock

	// eventHandlers// @lock

	docRichText.click = function docRichText_click (event)// @startlock
	{// @endlock
		
			

	};// @lock

	docRichText.mouseover = function docRichText_mouseover (event)// @startlock
	{// @endlock
		$('#'+this.id).attr('title', $('#'+this.id).text());
	};// @lock

	saveButton.click = function saveButton_click (event)// @startlock
	{// @endlock
		
//		var elemt = $(event.htmlObject);
//		var obj= waf.sources[getHtmlId("user")].query("login = :1",waf.directory.currentUser().userName);
//		waf.sources[getHtmlId("userObj")].firstName = obj.firstName;
//		waf.sources[getHtmlId("userObj")].lastName = obj.lastName;
//		waf.sources[getHtmlId("userObj")].email = obj.email;
//		waf.sources[getHtmlId("userObj")].telNumber = obj.telNumber;
//		waf.sources[getHtmlId("userObj")].address = obj.address;
	var currentUser = waf.sources[getHtmlId('user1')];
	var userAcc = {
			firstName: $$(getHtmlId('textField8')).getValue(),
			lastName: $$(getHtmlId('textField9')).getValue(),
    		email: $$(getHtmlId('textField10')).getValue(),
			telNumber: $$(getHtmlId('textField11')).getValue(),
			address: $$(getHtmlId('textField12')).getValue()	
		};
		var oldPass =$$(getHtmlId('textFieldOldPass')).getValue();
		var newPass =$$(getHtmlId('textFieldNewPass')).getValue();
		var confirmPass = $$(getHtmlId('textFieldConfirm')).getValue();
		if(newPass != null && newPass != ""){
			if(newPass == confirmPass){
				waf.ds.User.checkPassword({
					onSuccess: function(data){
						var res = data.result;
						if(res){
							userAcc.password = $$(getHtmlId('textFieldConfirm')).getValue();
							
							currentUser.EditAccount({
								onSuccess: function(event){
									if(event.result == true){
										$$(getHtmlId('accountContainer')).hide();
										$(getHtmlObj('errorDivFName')).html('');
										$(getHtmlObj('errorDivLName')).html('');
										$(getHtmlObj('errorDivEmail')).html('');
										$(getHtmlObj('errorDivConfirmPass')).html("");
										$(getHtmlObj('errorDivOldPass')).html("");
														
									} else {
										var errors = event.result.message.split(',');
										$(getHtmlObj('errorDivFName')).html(errors[0]);
										$(getHtmlObj('errorDivLName')).html(errors[1]);
										$(getHtmlObj('errorDivEmail')).html(errors[2]);
									}
								},
								onError: function(error) {
									//$$(getHtmlId('accountContainer')).hide();
									alert(error.result.message);
								}
							},userAcc);
						}
						else {
							$(getHtmlObj('errorDivOldPass')).html("Error Password");
							return false;
						}	
					},
					onError: function(data){
						console.log(data);
						return;
					}	
				},oldPass)
		}
		else {
			$(getHtmlObj('errorDivConfirmPass')).html("Pasword And Confirmation are different");
			return;
			
		}
			
		}else{
			currentUser.EditAccount({
				onSuccess: function(event){
					if(event.result == true){
						$$(getHtmlId('accountContainer')).hide();
						$(getHtmlObj('errorDivFName')).html('');
						$(getHtmlObj('errorDivLName')).html('');
						$(getHtmlObj('errorDivEmail')).html('');
						$(getHtmlObj('errorDivConfirmPass')).html("");
						$(getHtmlObj('errorDivOldPass')).html("");
						
					} else {
						var errors = event.result.message.split(',');
						$(getHtmlObj('errorDivFName')).html(errors[0]);
						$(getHtmlObj('errorDivLName')).html(errors[1]);
						$(getHtmlObj('errorDivEmail')).html(errors[2]);
					}
				},
				onError: function(error) {
					//$$(getHtmlId('accountContainer')).hide();
					alert(error.result.message);
				}
			},userAcc);
		
	}	
	
	
	};// @lock

	imageButtonCloAcc.click = function imageButtonCloAcc_click (event)// @startlock
	{// @endlock
		$$(getHtmlId("accountContainer")).hide();
	};// @lock

	directoryEvent.onCurrentElementChange = function directoryEvent_onCurrentElementChange (event)// @startlock
	{// @endlock
		if (this.getCurrentElement()!==null) {
//			waf.sources[getHtmlId("directory")].orderBy("updatingDate desc");
		   // waf.sources[getHtmlId("document")].sync();
    }
	};// @lock

	spaceEvent.onCurrentElementChange = function spaceEvent_onCurrentElementChange (event)// @startlock
	{// @endlock
		if (this.getCurrentElement()!==null) {
//			waf.sources[getHtmlId("space")].orderBy("updatingDate desc");
		   // waf.sources[getHtmlId("document")].sync();
    }
	};// @lock

	documentEvent.onCurrentElementChange = function documentEvent_onCurrentElementChange (event)// @startlock
	{// @endlock
		if (this.getCurrentElement()!==null) {
//			debugger;
//			waf.sources[getHtmlId("document")].orderBy("updatingDate desc");
		   // waf.sources[getHtmlId("document")].sync();
    }
	};// @lock

	imageButton3.click = function imageButton3_click (event)// @startlock
	{// @endlock
		$$('bodyComponent').loadComponent({path: '/Components/overview.waComponent'});
	};// @lock

	imageButton4.click = function imageButton4_click (event)// @startlock
	{// @endlock
//		$$(getHtmlId("spacesContainer")).hide();
//		$$(getHtmlId("directoriesContainer")).hide();
//		$$(getHtmlId("documentsContainer")).hide();

		$$(getHtmlId("accountContainer")).show();
		var obj= waf.sources[getHtmlId("user1")].query("login = :1",waf.directory.currentUser().userName, {
				onSuccess1:function(){
					
					waf.sources[getHtmlId("user1")].sync();
				}
		
		});
		
	};// @lock
												
	documentsMatrix.onChildrenDraw = function documentsMatrix_onChildrenDraw (event)// @startlock
	{// @endlock
		var elemt = $(event.htmlObject);
//		var us = waf.sources[getHtmlId("document")].creator.ID;
//		console.log(us);
		if($.trim(waf.sources[getHtmlId("document")].creationDate) == $.trim(waf.sources[getHtmlId("document")].updatingDate)){
			
			elemt.find(".docMsg").html("Create a new document");
		}
		else {
			elemt.find(".docMsg").html(" Update document");

		}

		
	};// @lock

	directoriesMatrix.onChildrenDraw = function directoriesMatrix_onChildrenDraw (event)// @startlock
	{// @endlock
		var elem = $(event.htmlObject);
		var creatDate = waf.sources[getHtmlId("directory")].creationDate;
		var updDate = waf.sources[getHtmlId("directory")].updatingDate; 
		if($.trim(waf.sources[getHtmlId("directory")].creationDate) == $.trim(waf.sources[getHtmlId("directory")].updatingDate)){
			
			elem.find(".msgUpdatedDirectory").html("has been created in "+creatDate);
		}
		else {
			elem.find(".msgUpdatedDirectory").html("has been updated"+updDate);

		}
	};// @lock

	spacesMatrix.onChildrenDraw = function spacesMatrix_onChildrenDraw (event)// @startlock
	{// @endlock
		var element = $(event.htmlObject);
		var crDate = waf.sources[getHtmlId("space")].creationDate;
		var upDate = waf.sources[getHtmlId("space")].updatingDate;
		var nameSpace = waf.sources[getHtmlId("space")].name;
		if($.trim(waf.sources[getHtmlId("space")].creationDate) == $.trim(waf.sources[getHtmlId("space")].updatingDate)){

			element.find(".msgupdatedlabel").html("has been created In\n"+crDate);
			element.find(".msgDate").html("In ");
		}
		else {
			element.find(".msgupdatedlabel").html("has been updated"+upDate);
			element.find(".msgDate").html("In\n"+upDate);

		}
		
	};// @lock

	// @region eventManager// @startlock
	WAF.addListener(this.id + "_docRichText", "click", docRichText.click, "WAF");
	WAF.addListener(this.id + "_docRichText", "mouseover", docRichText.mouseover, "WAF");
	WAF.addListener(this.id + "_saveButton", "click", saveButton.click, "WAF");
	WAF.addListener(this.id + "_imageButtonCloAcc", "click", imageButtonCloAcc.click, "WAF");
	WAF.addListener(this.id + "_directory", "onCurrentElementChange", directoryEvent.onCurrentElementChange, "WAF");
	WAF.addListener(this.id + "_space", "onCurrentElementChange", spaceEvent.onCurrentElementChange, "WAF");
	WAF.addListener(this.id + "_document", "onCurrentElementChange", documentEvent.onCurrentElementChange, "WAF");
	WAF.addListener(this.id + "_imageButton3", "click", imageButton3.click, "WAF");
	WAF.addListener(this.id + "_imageButton4", "click", imageButton4.click, "WAF");
	WAF.addListener(this.id + "_documentsMatrix", "onChildrenDraw", documentsMatrix.onChildrenDraw, "WAF");
	WAF.addListener(this.id + "_directoriesMatrix", "onChildrenDraw", directoriesMatrix.onChildrenDraw, "WAF");
	WAF.addListener(this.id + "_spacesMatrix", "onChildrenDraw", spacesMatrix.onChildrenDraw, "WAF");
	// @endregion// @endlock

	};// @lock


}// @startlock
return constructor;
})();// @endlock
