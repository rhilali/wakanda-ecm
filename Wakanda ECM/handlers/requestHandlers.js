﻿
function readDocument(request, response){
    parameters = request.urlQuery.split("&");
	var document,
		path,
		docID;
    for (i = 0; i < parameters.length; i++) {
        var parameter = parameters[i].split("=");
        if (parameter[0]== "id") {
            docID = parameter[1];
        }else{
        	response.body ="Warning";
        	return;
    	}
	}
	document = ds.Document.find("ID =:1",docID);
	if(document == null){
		response.body="This Document Does not Exist Or You don't have permissions to read this document";
	}else{
	  		path = document.path;
        	var file = File(path);
        	if(document.type !== null){
			response.headers["CONTENT_TYPE"] = document.type;
        	}else{
        		response.headers["CONTENT_TYPE"] = 'application/octet-stream'; 
				response.headers['content-disposition'] = 'attachement; filename='+document.name; 
        	}
			response.body = file.toBuffer().toBlob();
	}
}

function loadDocument(request, response){
	
    parameters = request.urlQuery.split("&");
	var document,
		path,
		docID;
		
    for (i = 0; i < parameters.length; i++) {
        var parameter = parameters[i].split("=");
        if (parameter[0]== "id") {
            docID = parameter[1];
        }
        else {
        	response.body ="Warning";
        	return;
    }
}
			document = ds.Document.find("ID =:1",docID);
			if(document == null){
				
				response.body="This Document Does not Exist Or You don't have permissions to download this document";
			}
			else {	
            path = document.path;
            filename=document.name;
            var file = File(path);
			response.contentType = 'application/pdf';
   			response.headers['content-disposition'] = 'attachement; filename='+filename; 
   	 		response.body = file.toBuffer().toBlob();
   	 	}
	
}
function readVersion(request, response){
    parameters = request.urlQuery.split("&");
	var version,
		path,
		versionID;
    for (i = 0; i < parameters.length; i++) {
        var parameter = parameters[i].split("=");
        if (parameter[0]== "id") {
            versionID = parameter[1];
        }
        else {
        	response.body ="Warning";
        	return;
    	}
	}
	version = ds.Versions.find("ID =:1",versionID);
	if(version == null){	
		response.body="Document Not Exist";
	}
	else {
    	path = version.path;
        var file = File(path);
        if(version.document.type !== null){
        	response.headers["CONTENT_TYPE"] = version.document.type;
        }else{
        	response.headers["CONTENT_TYPE"] = 'application/octet-stream'; 
        }
   	 	response.body = file.toBuffer().toBlob();
   	 	}
}
function loadVersion(request, response){
    parameters = request.urlQuery.split("&");
	var version,
		path,
		versionID;
    for (i = 0; i < parameters.length; i++) {
        var parameter = parameters[i].split("=");
        if (parameter[0]== "id") {
            versionID = parameter[1];
        }else{
        	response.body ="Warning";
        	return;
    	}
	}
	version = ds.Versions.find("ID =:1",versionID);
	if(version == null){
		response.body="Document Not Exist";
	}else {	
    	path = version.path;
        filename = version.document.name;
        var file = File(path);
		response.contentType = 'application/pdf';
   		response.headers['content-disposition'] = 'attachement; filename=' + filename; 
   	 	response.body = file.toBuffer().toBlob();
	}
}
