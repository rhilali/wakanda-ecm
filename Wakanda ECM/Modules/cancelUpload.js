﻿
exports.cancelUpload = function cancelUpload(documentsToSave){
	var result = {message: " The uploaded documents was removed "};
	for(var i=0; i<documentsToSave.length; i++){
		var path = documentsToSave[i].path;
		try{
			File(path).remove();
		}catch(e){
			result = {message: e.messages[1]};
		}
	}
	return result;
};
exports.cancelUploadVersion = function cancelUploadVersion(versionToSave){
	var result = {message: " The uploaded version was removed "};
	var path = versionToSave.path;
	try{
		File(path).remove();
	}catch(e){
		result = {message: e.messages[1]};
	}
	return result;
};
