﻿var apiKey = '';
var clientID ='183978444548-car17r6n9p575caa3oq6a3somj9id7ia.apps.googleusercontent.com';
var scope ='https://www.googleapis.com/auth/drive';
var accessToken ='';


exports.setAccessToken = function setAccessToken(token){
accessToken = token;
}


exports.createLocalFile = function createLocalFile(nameFile,nameFolder,content){
	var path = ds.getModelFolder().path;
  	var newFolder = Folder (path+nameFolder);
  	var myFile = new File (path+nameFolder+"/"+nameFile);
  	if(!newFolder.exists){
	    newFolder.create();
		myFile.create();
		var stream = TextStream(myFile,"write");
		stream.write(content+"\n");
		stream.close();
} 
	else {
		
				if(!myFile.exists){
				myFile.create();
				var stream = TextStream(myFile,"write");
				stream.write(content+"\n");
				stream.close();
							
		}
		
}
	return myFile.path;

}


exports.existsFile =function existsFile(path){
	
	var myFile = new File (path);
		if(myFile.exists){
			
			return true;
		}
		else {
			return false;
		}
}


exports.deleteFile = function deleteFile(path){
	var myFile = new File (path);
		if(myFile.exists){
			myFile.remove();
		}
}


     
exports.createGoogleDocs = function createGoogleDocs(path,type) {
	var fileName = new File(path);
	var xhr = new XMLHttpRequest();
	var urlText = "https://www.googleapis.com/upload/drive/v2/files?uploadType=media";
	var response = {};
	
	xhr.onreadystatechange =function() { 
     var state = this.readyState;
     if (state !== 4) { 
     return;
       
     }
     response = xhr.response;
     return response;

     };
     
	xhr.open("POST",urlText);
	
	if(type == "docx"){
		xhr.setRequestHeader('Content-Type',"application/vnd.google-apps.document");
	}
	if(type == "pptx"){
		xhr.setRequestHeader('Content-Type',"application/vnd.google-apps.presentation");
	}
	if(type =="xlsx"){
		xhr.setRequestHeader('Content-Type',"application/vnd.google-apps.spreadsheet");
	}
	xhr.setRequestHeader('Authorization','Bearer '+accessToken);
	
	xhr.send("");																								
    return response;   
    };
    
    
    
  exports.getGoogleDocs = function getGoogledocs(fileID){
    	
    	var xhr = new XMLHttpRequest();
    	var urlText ="https://www.googleapis.com/drive/v2/files/"+fileID;
    	var response ={};
    	
    	xhr.onreadystatechange =function() { 
        var state = this.readyState;
        if (state !== 4) { 
           return;
       
        }
       response = xhr.response;
        return response;

       };
    	
    	xhr.open("GET",urlText);
    	xhr.setRequestHeader('Authorization','Bearer '+accessToken);
		xhr.send();	
																									
      return response; 
    	}
    	
    	
 exports.getContent = function getContent(urlText){
//   	debugger;
   	var xhr = new XMLHttpRequest();
   	var response ={};
   	xhr.responseType = "blob";
   	xhr.onreadystatechange =function() { 
        var state = this.readyState;
        if (state !== 4) { 
           return;
       
        }
         	
       response = xhr.response;
        return response;

       };
	xhr.open("GET",urlText);
	xhr.setRequestHeader('Authorization','Bearer '+accessToken);
    xhr.send();
    
    return response;
  }
       
  	
   	exports.deleteGoogleDocs = function deleteGoogleDocs(fileID){
   		
   		//debugger;
   		var xhr = new XMLHttpRequest();
   		var url ="https://www.googleapis.com/drive/v2/files/"+fileID;
   		
   		xhr.onreadystatechange =function() { 
        var state = this.readyState;
        if (state !== 4) { 
           return;
        }
        response = xhr.response;
        return response;
       };
       
       xhr.open("DELETE",url);
       xhr.setRequestHeader('Authorization','Bearer '+accessToken);
       xhr.send();
       
        return response;
   		}
   		
 exports.save = function save(links,fileName,mimeType,idFile, documentParentData){
 	var library = null; 
	var parentDirectory = null;
	var exists;
	if(mimeType == "application/vnd.google-apps.document"){	
		var ext = ".docx";
    }
    if(mimeType == "application/vnd.google-apps.spreadsheet"){	
		var ext = ".xlsx";
    }
    if(mimeType == "application/vnd.google-apps.presentation"){	
		var ext = ".pptx";
    }
	
	if(documentParentData.theParentDirectory == null){
		library = ds.Library.find("ID= :1",documentParentData.theLibrary);
		var documents = library.documents;
		exists = false;
		documents.forEach(
    		function(doc) {
      			if(doc.name == fileName + ext){
      				exists = true;
      			}
    		}
   		);
		if(!exists){
			var rep = require("googleDocs").getContent(links);
			var path = library.path + '/' + fileName + " " + "(1.0)" + ext;
    		rep.copyTo(path,true);
    		var response =require("googleDocs").deleteGoogleDocs(idFile);
		}
	}else{
		parentDirectory = ds.Directory.find("ID= :1",documentParentData.theParentDirectory);
		var documents = parentDirectory.documents;
		exists = false;
		documents.forEach(
    		function(doc) {
      			if(doc.name == fileName + ext){
      				exists = true;
      			}
    		}
   		);
		if(!exists){
			var rep = require("googleDocs").getContent(links);
			var path = parentDirectory.path + '/' + fileName + " " + "(1.0)" + ext;
    		rep.copyTo(path,true);
    		var response =require("googleDocs").deleteGoogleDocs(idFile);
		}
	}
	return {exists: exists, fileToSave: {name: fileName + ext, path: path}};
 }  			
