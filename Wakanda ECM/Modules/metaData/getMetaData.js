﻿/*	In order to make the helloWorld() function available client-side, you have to add a reference to the 'getMetaData' module in the GUI Designer.
	The helloWorld() function can be executed from your JS file as follows:
	alert(getMetaData.helloWorld());
	
	For more information, refer to http://doc.wakanda.org/Wakanda0.Beta/help/Title/en/page1516.html
*/

exports.getMetaData = function getMetaData(document) {
	var strURL = "http://localhost:8983/solr/select";
    var xhr = new XMLHttpRequest();
    xhr.open('POST', strURL, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var params = getStandardArgs().concat(setQuery(document));
    var strData = params.join('&');
    xhr.send(strData);
    return xhr.responseText;
}

function getStandardArgs() {
    var params = [
        'wt=json',
        'indent=on',
        'fl=id, author, title, stream_name, content_type, description, publisher, page_count, stream_size, xmptpg_npages, version'
        ];

    return params;
}
function setQuery(document) {
  var qstr = 'q=id:' + document.ID+" AND " + 'version:'+ document.version;
  return qstr;
}