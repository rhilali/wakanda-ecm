﻿/*	In order to make the helloWorld() function available client-side, you have to add a reference to the 'indexDocument' module in the GUI Designer.
	The helloWorld() function can be executed from your JS file as follows:
	alert(indexDocument.helloWorld());
	
	For more information, refer to http://doc.wakanda.org/Wakanda0.Beta/help/Title/en/page1516.html
*/

exports.indexDocument = function indexDocument (documentArgs) {
	var strURL = "http://localhost:8983/solr/update/extract";
    var xhr = new XMLHttpRequest();
    xhr.open('POST', strURL, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var params = getStandardArgs().concat(setDocumentArgs(documentArgs.id,documentArgs.path, documentArgs.version));
    var strData = params.join('&');
    xhr.send(strData);
    return {response: xhr.responseText};
	
};

function getStandardArgs() {
    var params = [
    	'literalsOverride =true',
    	'overwrite=false',
        'wt=json',
        'indent=on',
        'fmap.content=attr_content',
        'commit=true' 
        ];
    return params;
}

function setDocumentArgs(id,path,version) {
  var documentArgs = "stream.file=" + path + "&" + "literal.id=" + id + "&" +"literal.version=" +version;
  return documentArgs;
}
