﻿
exports.search = function search (params) {
	var strURL = "http://localhost:8983/solr/select";
    var xhr = new XMLHttpRequest();
    xhr.open('POST', strURL, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var params = getStandardArgs().concat(setQuery(params));
    var strData = params.join('&');
    xhr.send(strData);
    return xhr.responseText;
	
};

function getStandardArgs() {
    var params = [
        'wt=json',
        'indent=on',
        'fl=id'
        ];

    return params;
}
function setQuery(params) {
  var qstr = 'q=' + params.searchBy + ':' + '*' +escape(params.value)+ '*';
  return qstr;
}
