﻿
exports.updateMetaData = function updateMetaData(documentArgs) {
	//suppression de l'ancienne version Solr ne permet de modifier individual fields !!!!!!!!!
	require('metaData/updateMetaData').deleteVersionMetaData({idDoc:documentArgs.idDoc, version: documentArgs.version});
	//	reIndexer le doc
	var strURL = "http://localhost:8983/solr/update/extract";
    var xhr = new XMLHttpRequest();
    xhr.open('POST', strURL, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var params = getStandardArgs().concat(setDocumentArgs(documentArgs));
    var strData = params.join('&');
    xhr.send(strData);
    return {response: xhr.responseText};
};
exports.indexRevertVersion = function indexRevertVersion(documentArgs) {
	//	reIndexer le doc
	var strURL = "http://localhost:8983/solr/update/extract";
    var xhr = new XMLHttpRequest();
    xhr.open('POST', strURL, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    var params = getStandardArgs().concat(setDocumentArgs(documentArgs));
    var strData = params.join('&');
    xhr.send(strData);
    return {response: xhr.responseText};
};
exports.deleteMetaData = function deleteMetaData(idDocument){
	var strURL = "http://localhost:8983/solr/update";
	var strData = "stream.body=<delete><query>id:";
	strData += idDocument +"</query></delete>&commit=true";
	var xhr = new XMLHttpRequest();
    xhr.open('POST', strURL, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(strData);
    return {response: xhr.responseText};
};

exports.deleteVersionMetaData = function deleteVersionMetaData(versionData){
	var strURL = "http://localhost:8983/solr/update";
	var strData = "stream.body=<delete><query>id:";
	strData += versionData.idDoc + " AND version:" + versionData.version + "</query></delete>&commit=true";
	var xhr = new XMLHttpRequest();
    xhr.open('POST', strURL, true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(strData);
    return {response: xhr.responseText};
};
function getStandardArgs() {
    var params = [
    	'literalsOverride =true',
        'wt=json',
        'indent=on',
        'overwrite=false',
        'fmap.content=attr_content',
        'commit=true' 
        ];
    return params;
}

function setDocumentArgs(documentArgs) {
  var documentArgs = [
  		"stream.file=" + documentArgs.path,
  		"literal.id=" + documentArgs.idDoc,
  		"literal.version=" + documentArgs.version,
  		"literal.Author=" + documentArgs.author,
  		"literal.title=" + documentArgs.title,
//  		"literal.Content-Type=" + documentArgs.type,
  		"literal.description=" + documentArgs.description,
  		"literal.publisher=" + documentArgs.publisher,
  		"literal.Page-Count=" + documentArgs.nbPages,
  		
  		];
  documentArgs.join('&');
  return documentArgs;
}
