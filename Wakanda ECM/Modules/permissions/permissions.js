﻿
function getCurrentUserData(){
	var session = currentSession();
	var myStorage = session.storage;
	var myID = (myStorage.myID == undefined)? null : myStorage.myID;
	var theGroups = (myStorage.theGroups == undefined)? null : myStorage.theGroups;
	return {ID:myID, groups: theGroups};
}
exports.isAdmin = function isAdmin () {
	var currentUserData = getCurrentUserData();
	var result = false;
	if(currentUserData.ID != null){
		var admin = ds.Admin.find("ID = :1",currentUserData.ID);
		if(admin != null){
			result = true;
		}
	}
	return result;
};
exports.isSpaceManager = function isSpaceManager(idSpace) {
	var currentUserData = getCurrentUserData();
	var space = ds.Space.find("ID = :1",idSpace);
	var result = false;
	if(currentUserData.ID != null){
	if(space != null){
		if(space.manager.ID == currentUserData.ID){
			result = true;
		}
	}
	}
	return result;
};
exports.checkSpacePermissions = function checkSpacePermissions (idSpace) {
	var okContribution = false;
	var okCollaboration = false;
	var okReading = false;
	var currentUserData = getCurrentUserData();
	var permissions = require('permissions/permissions');
	var space = ds.Space.find("ID = :1",idSpace);
if(space != null){
	if((permissions.isAdmin() == true) || (permissions.isSpaceManager(space.ID))){
		okReading = true;
		okContribution = true;
		okCollaboration = true;
	}else{
		if(currentUserData.groups != null){
			for (var i=0; i<currentUserData.groups.length; i++){
				var perm = ds.RoleGroupSpace.find("group.ID = :1 AND space.ID = :2",currentUserData.groups[i],idSpace);
				if(perm != null){
					if(perm.reading == true){
						okReading = true;
					}
					if(perm.contribution == true){
						okContribution = true;
					}
					if(perm.collaboration == true){
						okCollaboration = true;
					}
				}
			}
		}
	}
}
	return {contribution: okContribution, collaboration: okCollaboration, reading: okReading};
	
};

exports.checkLibraryPermissions = function checkLibraryPermissions (idLibrary) {
	var okContribution = false;
	var okCollaboration = false;
	var okReading = false;
	var currentUserData = getCurrentUserData();
	var permissions = require('permissions/permissions');
	var library = ds.Library.find("ID = :1",idLibrary);
if(library != null){
	var parentSpace = (library.space == undefined)? -1 : library.space.ID;
	if((permissions.isAdmin() == true) || (permissions.isSpaceManager(parentSpace))){
		okReading = true;
		okContribution = true;
		okCollaboration = true;
	}else{
		if(currentUserData.groups != null){
			for (var i=0; i<currentUserData.groups.length; i++){
				var perm = ds.RoleGroupLibrary.find("group.ID = :1 AND library.ID = :2",currentUserData.groups[i],idLibrary);
				if(perm != null){
					if(perm.reading == true){
						okReading = true;
					}
					if(perm.contribution == true){
						okContribution = true;
					}
					if(perm.collaboration == true){
						okCollaboration = true;
					}
				}
			}
		}
	}
}
	return {contribution: okContribution, collaboration: okCollaboration, reading: okReading};
	
};

exports.checkDirectoryPermissions = function checkDirectoryPermissions (idDirectory) {
	var okContribution = false;
	var okCollaboration = false;
	var okReading = false;
	var currentUserData = getCurrentUserData();
	var permissions = require('permissions/permissions');
	var directory = ds.Directory.find("ID = :1",idDirectory);
if(directory != null){
	if((permissions.isAdmin() == true) || (permissions.isSpaceManager(directory.parentSpace))){
		okReading = true;
		okContribution = true;
		okCollaboration = true;
	}else{
		if(currentUserData.groups != null){
			for (var i=0; i<currentUserData.groups.length; i++){
				var perm = ds.RoleGroupDirectory.find("group.ID = :1 AND directory.ID = :2",currentUserData.groups[i],idDirectory);
				if(perm != null){
					if(perm.reading == true){
						okReading = true;
					}
					if(perm.contribution == true){
						okContribution = true;
					}
					if(perm.collaboration == true){
						okCollaboration = true;
					}
				}
			}
		}
	}
}
	return {contribution: okContribution, collaboration: okCollaboration, reading: okReading};
	
};
exports.checkDocumentPermissions = function checkDocumentPermissions (idDocument) {
	var okContribution = false;
	var okCollaboration = false;
	var okReading = false;
	var currentUserData = getCurrentUserData();
	var permissions = require('permissions/permissions');
	var document = ds.Document.find("ID = :1",idDocument);
if(document != null){
	if((permissions.isAdmin() == true) || (permissions.isSpaceManager(document.parentSpace))){
		okReading = true;
		okContribution = true;
		okCollaboration = true;
	}else{
		if(currentUserData.groups != null){
			for (var i=0; i<currentUserData.groups.length; i++){
				var perm = ds.AccessRuleGroupDocument.find("group.ID = :1 AND document.ID = :2",currentUserData.groups[i],idDocument);
				if(perm != null){
					if(perm.reading == true){
						okReading = true;
					}
					if(perm.contribution == true){
						okContribution = true;
					}
					if(perm.collaboration == true){
						okCollaboration = true;
					}
				}
			}
		}
	}
}
	return {contribution: okContribution, collaboration: okCollaboration, reading: okReading};
	
};