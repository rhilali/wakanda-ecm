﻿
function login(userName, password) {
    var user = ds.User.query('login === :1 ', userName);
    user = user.first();
    
    if (user != null && user.validatePassword(password)) {
    	var groupName = user.userGroupCollection.group.directoryID;
		var groups = user.userGroupCollection.group.ID;         
           
        return {
            ID: user.ID,
            name: user.login,
            fullName: user.firstName + " " + user.lastName,
            belongsTo: groupName,
            storage: {
            	myID: user.ID,
            	theGroups: groups,
                time: new Date(),
                access: "Guest access"
            }
        };
    }
    else {
//        return {
//            error: 1024,
//            errorMessage: "invalid login"
//        }
		return false;
    }

}
